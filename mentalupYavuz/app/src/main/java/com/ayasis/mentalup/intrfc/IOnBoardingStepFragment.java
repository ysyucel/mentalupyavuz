package com.ayasis.mentalup.intrfc;

/**
 * Created by AhmtBrK on 20/03/2017.
 */

public interface IOnBoardingStepFragment {
    void onNextButtonClicked();
}
