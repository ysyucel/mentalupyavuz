package com.ayasis.mentalup.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ayasis.mental.UnityPlayerActivity;
import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;

import static com.ayasis.mental.UnityPlayerActivity.userLanguageForUnity;
import static com.ayasis.mental.UnityPlayerActivity.userNameForUnity;

public class NativeCallUnity extends AppCompatActivity {
    Context context = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deneme);
        userNameForUnity = PreferencesHelper.getInstance(context).getUsername();
        userLanguageForUnity = PreferencesHelper.getInstance(context).getUserLang();
        Intent intent = new Intent(NativeCallUnity.this, UnityPlayerActivity.class);
        startActivity(intent);
    }
}
