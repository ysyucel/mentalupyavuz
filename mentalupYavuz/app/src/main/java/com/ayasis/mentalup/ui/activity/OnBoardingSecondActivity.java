package com.ayasis.mentalup.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by AhmtBrK on 20/03/2017.
 */

public class OnBoardingSecondActivity extends BaseActivity implements View.OnClickListener {

    Button launchButton;
    TextView onboardingsecondheader, firstTV, secondTV, thirdTV, fourthTV, fifthTV, sixthTV;
    LinearLayout onboardingsecondpagefirstlayer, onboardingsecondpagesecondlayer, onboardingsecondpagethirdlayer, onboardingsecondpagefourthlayer, onboardingsecondpagefifthlayer, onboardingsecondpagesixthlayer;
    ImageView firstImage, secondImage, thirdImage, fourthImage, fifthImage, sixthImage;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding_second);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("noparam"," " );
        mFirebaseAnalytics.logEvent("IKNOWPAGE_VISITED",bundle);

        //Fabric Event - I Know page Page visit
        Answers.getInstance().logCustom(new CustomEvent("Visit IKnow Page"));

        onboardingsecondheader = (TextView) findViewById(R.id.onboardingsecondheader);
        firstTV = (TextView) findViewById(R.id.firstTV);
        secondTV =(TextView) findViewById(R.id.secondTV);
        thirdTV= (TextView) findViewById(R.id.thirdTV);
        fourthTV= (TextView) findViewById(R.id.fourthTV);
        fifthTV= (TextView) findViewById(R.id.fifthTV);
        sixthTV= (TextView) findViewById(R.id.sixthTV);

        onboardingsecondpagefirstlayer = (LinearLayout)findViewById(R.id.onboardingsecondpagefirstlayer);
        onboardingsecondpagesecondlayer = (LinearLayout)findViewById(R.id.onboardingsecondpagesecondlayer);
        onboardingsecondpagethirdlayer = (LinearLayout)findViewById(R.id.onboardingsecondpagethirdlayer);
        onboardingsecondpagefourthlayer = (LinearLayout)findViewById(R.id.onboardingsecondpagefourthlayer);
        onboardingsecondpagefifthlayer = (LinearLayout)findViewById(R.id.onboardingsecondpagefifthlayer);
        onboardingsecondpagesixthlayer = (LinearLayout)findViewById(R.id.onboardingsecondpagesixthlayer);

        firstImage = (ImageView) findViewById(R.id.firstImage);
        secondImage = (ImageView) findViewById(R.id.secondimage);
        thirdImage = (ImageView) findViewById(R.id.thirdImage);
        fourthImage = (ImageView) findViewById(R.id.fourthImage);
        fifthImage = (ImageView) findViewById(R.id.fifthImage);
        sixthImage = (ImageView) findViewById(R.id.sixthImage);


        launchButton = (Button) findViewById(R.id.launchButton);
        launchButton.setOnClickListener(this);
        manageLayoutMargins();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.launchButton:
                Bundle bundle = new Bundle();
                bundle.putString("noparam"," " );
                mFirebaseAnalytics.logEvent("IKNOWPAGE_STARTBUTTON_CLICK",bundle);

                //Fabric Event - I Know Page Start Game Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click Start Game Button Via I Know Page"));

                Intent intent = new Intent(OnBoardingSecondActivity.this, NativeCallUnity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();

        if (isTablet == false) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth/ 12;


            double imageSize = screenHeigt /1336 * 100;

            firstImage.getLayoutParams().height =(int)imageSize;
            secondImage.getLayoutParams().height =(int)imageSize;
            thirdImage.getLayoutParams().height =(int)imageSize;
            fourthImage.getLayoutParams().height =(int)imageSize;
            fifthImage.getLayoutParams().height =(int)imageSize;
            sixthImage.getLayoutParams().height =(int)imageSize;

            firstImage.getLayoutParams().width =(int)imageSize;
            secondImage.getLayoutParams().width =(int)imageSize;
            thirdImage.getLayoutParams().width =(int)imageSize;
            fourthImage.getLayoutParams().width =(int)imageSize;
            fifthImage.getLayoutParams().width =(int)imageSize;
            sixthImage.getLayoutParams().width =(int)imageSize;


            double firstTopGap = screenHeigt / 1336.0 * 80;
            double secondTopGap = screenHeigt / 1336.0 * 50;
            double thirdTopGap = screenHeigt / 1336.0 * 35;
            double bottomGap = screenHeigt / 1336.0 * 53;
            double imageMarginRight = unitWidth/2;




            UiUtil.setMarginToView(firstTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(secondTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(thirdTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(fourthTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(fifthTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(sixthTV, (int)imageMarginRight, 0, 0, 0);

            onboardingsecondpagesecondlayer.setVisibility(View.GONE);
            UiUtil.setMarginToView(onboardingsecondheader, (int) unitWidth, (int) firstTopGap, (int) unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagefirstlayer, (int)(unitWidth), (int)secondTopGap, (int)(unitWidth), 0);
            UiUtil.setMarginToView(onboardingsecondpagesecondlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagethirdlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagefourthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagefifthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagesixthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(launchButton, (int)unitWidth, (int)0 , (int)unitWidth, (int)bottomGap);


            launchButton.getLayoutParams().height = (int)(unitWidth*1.9);

        } else {


            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth/ 12;


            double imageSize = screenHeigt /2048 * 150;

            firstImage.getLayoutParams().height =(int)imageSize;
            secondImage.getLayoutParams().height =(int)imageSize;
            thirdImage.getLayoutParams().height =(int)imageSize;
            fourthImage.getLayoutParams().height =(int)imageSize;
            fifthImage.getLayoutParams().height =(int)imageSize;
            sixthImage.getLayoutParams().height =(int)imageSize;

            firstImage.getLayoutParams().width =(int)imageSize;
            secondImage.getLayoutParams().width =(int)imageSize;
            thirdImage.getLayoutParams().width =(int)imageSize;
            fourthImage.getLayoutParams().width =(int)imageSize;
            fifthImage.getLayoutParams().width =(int)imageSize;
            sixthImage.getLayoutParams().width =(int)imageSize;


            double firstTopGap = screenHeigt / 2048 * 120;
            double secondTopGap = screenHeigt / 2048 * 100;
            double thirdTopGap = screenHeigt / 2048 * 75;
            double bottomGap = screenHeigt / 2048.0 * 131;
            double imageMarginRight = unitWidth/2;

            UiUtil.setMarginToView(firstTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(secondTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(thirdTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(fourthTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(fifthTV, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(sixthTV, (int)imageMarginRight, 0, 0, 0);

            UiUtil.setMarginToView(onboardingsecondheader, (int) unitWidth, (int) firstTopGap, (int) unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagefirstlayer, (int)unitWidth, (int)secondTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagesecondlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagethirdlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagefourthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagefifthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingsecondpagesixthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(launchButton, (int)(unitWidth *2), (int)0 , (int)(unitWidth *2), (int)bottomGap);
            launchButton.getLayoutParams().height = (int)(unitWidth * 1.392);

        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page
            Intent i = new Intent(OnBoardingSecondActivity.this, WebViewActivity.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}

