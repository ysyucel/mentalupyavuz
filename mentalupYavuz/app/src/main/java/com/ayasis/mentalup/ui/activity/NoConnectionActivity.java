package com.ayasis.mentalup.ui.activity;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;

import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;;

public class NoConnectionActivity extends BaseActivity  {


    ImageView bigPic;
    ImageView smallPic;
    TextView textView;
    Button retryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_connection);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        smallPic = (ImageView) findViewById(R.id.smallPic);
        bigPic = (ImageView) findViewById(R.id.bigPic);
        textView = (TextView) findViewById(R.id.textView);
        retryButton = (Button) findViewById(R.id.retry_Button);
        retryButton.findViewById(R.id.retry_Button);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                restartAPP();

            }
        });
        setmyConnectionView();

    }


    public void setmyConnectionView() {

        // Internet yoksa acilacak sayfanin ozelliklerine gore gelmesi
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x;
        int height = size.y;
        int widthforpic = smallPic.getDrawable().getIntrinsicWidth();
        int heightforpic = smallPic.getDrawable().getIntrinsicHeight();
        int widthforpic2 = bigPic.getDrawable().getIntrinsicWidth();
        int heightforpic2 = bigPic.getDrawable().getIntrinsicHeight();


        smallPic.setVisibility(View.INVISIBLE);
        bigPic.setVisibility(View.INVISIBLE);
        textView.setVisibility(View.VISIBLE);
        retryButton.setVisibility(View.VISIBLE);

        Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        textView.setTypeface(opensansRegular);
        retryButton.setTypeface(opensansRegular);

        if (PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel()) {

            textView.measure(0, 0);
            retryButton.measure(0, 0);
            int textheight = textView.getMeasuredHeight(); //get height
            int textwidth = textView.getMeasuredWidth();  //get width

            int buttonheiht = retryButton.getMeasuredHeight();
            int buttonwith = retryButton.getMeasuredWidth();

            bigPic.setVisibility(View.VISIBLE);
            bigPic.setX((width - widthforpic2) / 2);
            bigPic.setY((float) (height - heightforpic2) / 2);
            textView.setX((width - textwidth) / 2);
            textView.setY((float) ((height - heightforpic2) / 2) + heightforpic2 + 15);
            retryButton.setX((width - buttonwith) / 2);
            retryButton.setY(((height - heightforpic2 - buttonheiht) / 2) + heightforpic2 + buttonheiht + 45);
        } else {

            textView.measure(0, 0);//must call measure!
            retryButton.measure(0, 0);

            int textwidth = textView.getMeasuredWidth();  //get width
            int buttonwith = retryButton.getMeasuredWidth();
            int buttonheiht = retryButton.getMeasuredHeight();
            smallPic.setVisibility(View.VISIBLE);

            smallPic.setX((width - widthforpic) / 2);
            smallPic.setY((float) (height - heightforpic) / 2);
            textView.setX((width - textwidth) / 2);
            textView.setY((float) ((height - heightforpic) / 2) + heightforpic + 15);
            retryButton.setX((width - buttonwith) / 2);
            retryButton.setY((float) ((height - heightforpic - buttonheiht) / 2) + heightforpic + buttonheiht + 30);
        }
    }

    public void restartAPP() {

        Intent i = new Intent(NoConnectionActivity.this, SplashActivity.class);
        startActivity(i);
        finish();
    }


}