package com.ayasis.mentalup.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.intrfc.IOnBoardingFirstActivity;
import com.ayasis.mentalup.intrfc.IOnBoardingStepFragment;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by AhmtBrK on 17/03/2017.
 */

public class OnBoardingSecondStepFragment extends Fragment implements IOnBoardingStepFragment {

    public static OnBoardingSecondStepFragment newInstance() {

        Bundle args = new Bundle();

        OnBoardingSecondStepFragment fragment = new OnBoardingSecondStepFragment();
        fragment.setArguments(args);
        return fragment;
    }

    IOnBoardingFirstActivity activity;
    AppCompatCheckBox step1CB, step2CB, step3CB, step4CB, step5CB;
    TextView fragmentsecondHeader;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (IOnBoardingFirstActivity) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_onboarding_second_step, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        step1CB = (AppCompatCheckBox) view.findViewById(R.id.step1CB);
        step2CB = (AppCompatCheckBox) view.findViewById(R.id.step2CB);
        step3CB = (AppCompatCheckBox) view.findViewById(R.id.step3CB);
        step4CB = (AppCompatCheckBox) view.findViewById(R.id.step4CB);
        step5CB = (AppCompatCheckBox) view.findViewById(R.id.step5CB);
        fragmentsecondHeader = (TextView) view.findViewById(R.id.fragmentsecondHeader);
        manageLayoutMargins();
    }

    @Override
    public void onNextButtonClicked() {
        int cat1 = step1CB.isChecked() ? 1 : 0;
        int cat2 = step2CB.isChecked() ? 1 : 0;
        int cat3 = step3CB.isChecked() ? 1 : 0;
        int cat4 = step4CB.isChecked() ? 1 : 0;
        int cat5 = step5CB.isChecked() ? 1 : 0;

        activity.getOnBoardingStepContainer().setCat1(cat1);
        activity.getOnBoardingStepContainer().setCat2(cat2);
        activity.getOnBoardingStepContainer().setCat3(cat3);
        activity.getOnBoardingStepContainer().setCat4(cat4);
        activity.getOnBoardingStepContainer().setCat5(cat5);

        //Fabric Event - Onboarding Page Second Next Button Click
        Answers.getInstance().logCustom(new CustomEvent("Click Next2 Button Via Onboarding Page"));

        activity.updateProfileInfo();

    }
    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getActivity()).getDeviceModel();
        if (isTablet == false) {

            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            double firstTopGap = screenHeigt / 1336.0 * 80;
            double secondTopGap = screenHeigt / 1336.0 * 65;
            double thirdTopGap = screenHeigt / 1336.0 * 50;



            UiUtil.setMarginToView(fragmentsecondHeader, (int)(unitWidth), (int)firstTopGap, (int)(unitWidth), 0);
            UiUtil.setMarginToView(step1CB, (int)unitWidth, (int)secondTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(step2CB, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(step3CB, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(step4CB, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(step5CB, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);


        } else {


            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = (screenWidth) /12 ;

            double firstTopGap = screenHeigt / 2048 * 120;
            double secondTopGap = screenHeigt / 2048 * 100;
            double thirdTopGap = screenHeigt / 2048 * 75;

            UiUtil.setMarginToView(fragmentsecondHeader, (int)(unitWidth * 2), (int)firstTopGap, (int)(unitWidth * 2), 0);
            UiUtil.setMarginToView(step1CB, (int)(unitWidth * 2), (int)secondTopGap, (int)(unitWidth * 2), 0);
            UiUtil.setMarginToView(step2CB, (int)(unitWidth * 2), (int)thirdTopGap, (int)(unitWidth * 2), 0);
            UiUtil.setMarginToView(step3CB, (int)(unitWidth * 2), (int)thirdTopGap, (int)(unitWidth * 2), 0);
            UiUtil.setMarginToView(step4CB, (int)(unitWidth * 2), (int)thirdTopGap, (int)(unitWidth * 2), 0);
            UiUtil.setMarginToView(step5CB, (int)(unitWidth * 2), (int)thirdTopGap, (int)(unitWidth * 2), 0);

        }
    }
}
