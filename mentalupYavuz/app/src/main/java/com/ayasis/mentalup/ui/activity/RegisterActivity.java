package com.ayasis.mentalup.ui.activity;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.http.HttpClient;
import com.ayasis.mentalup.http.JsonObjectResponseHandler;
import com.ayasis.mentalup.util.Constants;
import com.ayasis.mentalup.util.DialogUtil;

import com.ayasis.mentalup.util.KeyboardUtils;

import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.SpecialCharChecker;
import com.ayasis.mentalup.util.UiUtil;
import com.ayasis.mentalup.util.Utils;
import com.ayasis.mentalup.util.serverErrorMessages;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by AhmtBrK on 13/03/2017.
 */

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    TextView  registerInfoTV, notMemberText, registerTVHeader;
    RelativeLayout rootLayout,headerLayout;
    LinearLayout contentLayout;
    ImageView headerLogoforRegister;

    EditText usernameED, passwordED;
    Button registerButton;
    Button loginButton;
    int tryCount= Constants.tryNumber;
    private FirebaseAnalytics mFirebaseAnalytics;
    Trace myTrace = FirebasePerformance.getInstance().newTrace("RegisterPage");


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_register);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Email ile kayıt olma aksiyonunu tıklayan tüm kullanıcılar için tetiklenir
        mFirebaseAnalytics.logEvent("REGISTEREMAIL_VISITED", null);

        //Fabric Event - Signup Page visit
        Answers.getInstance().logCustom(new CustomEvent("Visit Signup Page"));

        fillViews();

    }

    private void fillViews() {

        Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");
        Typeface opensanslight = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");

        headerLayout = (RelativeLayout) findViewById(R.id.headerLayout);
        notMemberText = (TextView) findViewById(R.id.notmembertext);
        notMemberText.setTypeface(opensansRegular);

        registerTVHeader = (TextView) findViewById(R.id.registerTVHeader);
        registerTVHeader.setTypeface(opensansSemibold);



        headerLogoforRegister = (ImageView) findViewById(R.id.headerLogoforRegister);



        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
        contentLayout = (LinearLayout) findViewById(R.id.contentLayout);
        registerInfoTV = (TextView) findViewById(R.id.registerInfoTV);
        registerInfoTV.setTypeface(opensansSemibold);

        usernameED = (EditText) findViewById(R.id.usernameED);
        usernameED.setTypeface(opensansRegular);

        passwordED = (EditText) findViewById(R.id.passwordED);
        passwordED.setTypeface(opensansRegular);


        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setTypeface(opensansSemibold);
        registerButton.setOnClickListener(this);
        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setTypeface(opensansRegular);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
                finish();

                //Sayfanın headerında yer alan login düğmesine tıklayan kullanıcılar için tetiklenir
                mFirebaseAnalytics.logEvent("REGISTEREMAIL_LOGINBUTTON_CLICK", null);

                //Fabric Event - Register Page Login Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click Login Button Via Register Page"));

            }
        });
        manageLayoutMargins();
        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener()
        {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible)
            {
                formfocused(isVisible);
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            manageLayoutMargins();
        } else {
            manageLayoutMargins();
        }
    }




    public  void formfocused(boolean halil){
        if(halil){
            headerLayout.setVisibility(View.GONE);

        }else{
            headerLayout.setVisibility(View.VISIBLE);

        }
    }



    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();

        if (isTablet == false) {

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            double logoWidth = (2.2 * unitWidth) ;
            double logoHeight = logoWidth/1.62;
            headerLogoforRegister.getLayoutParams().width=(int)logoWidth;
            headerLogoforRegister.getLayoutParams().height=(int)logoHeight;
            UiUtil.setMarginToView(headerLogoforRegister, (int)(unitWidth/2),(int)(unitWidth/2), 0, (int)(unitWidth/2));

            loginButton.getLayoutParams().width =(int)(logoWidth*1.25);
            loginButton.getLayoutParams().height=(int)(logoWidth/1.8);
            UiUtil.setMarginToView(loginButton, 0,(int)(unitWidth/2), (int)(unitWidth/2), (int)(unitWidth/2));
            UiUtil.setMarginToView(notMemberText, 0,0, (int)(unitWidth/2), 0);

            double firstTopGap = screenHeigt / 1336.0 * 113 ;
            UiUtil.setMarginToView(registerTVHeader, (int)unitWidth, (int)firstTopGap, (int)unitWidth, 0);

            double secondTopGap= screenHeigt / 1336.0 * 44;
            UiUtil.setMarginToView(registerInfoTV, (int)unitWidth, (int)secondTopGap, (int)unitWidth, 0);

            double thirdTopGap = screenHeigt / 1336.0 * 106;
            UiUtil.setMarginToView(usernameED, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            //double edHeight = screenHeigt / 1336.0 * 100;
            //usernameED.getLayoutParams().height = (int)(unitWidth*1.9*2)/3;

            double fourthTopGap = screenHeigt / 1336.0 * 68;
            UiUtil.setMarginToView(passwordED, (int)unitWidth, (int)fourthTopGap, (int)unitWidth, 0);

            double fifthTopGap = screenHeigt / 1336.0 * 74;
            UiUtil.setMarginToView(registerButton,(int)unitWidth, (int)fifthTopGap, (int)unitWidth, 0);

            registerButton.getLayoutParams().height = (int)(unitWidth*1.9);


        } else {


            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            double logoWidth = (2 * unitWidth) ;
            double logoHeight = logoWidth/1.62;
            headerLogoforRegister.getLayoutParams().width=(int)logoWidth;
            headerLogoforRegister.getLayoutParams().height=(int)logoHeight;
            UiUtil.setMarginToView(headerLogoforRegister, (int)(unitWidth/3),(int)(unitWidth/3), 0, (int)(unitWidth/3));

            loginButton.getLayoutParams().width =(int)(logoWidth*1.25);
            loginButton.getLayoutParams().height=(int)(logoWidth/1.8);
            UiUtil.setMarginToView(loginButton, 0,(int)(unitWidth/3), (int)(unitWidth/3), (int)(unitWidth/3));

            UiUtil.setMarginToView(notMemberText, 0,0, (int)(unitWidth/3), 0);

            double firstTopGap = screenHeigt / 2048 * 131 ;
            UiUtil.setMarginToView(registerTVHeader, (int)(2*unitWidth), (int)firstTopGap, (int)(2*unitWidth), 0);

            double secondTopGap= screenHeigt / 2048.0 * 72;
            UiUtil.setMarginToView(registerInfoTV, (int)(2*unitWidth), (int)secondTopGap, (int)(2*unitWidth), 0);

            double thirdTopGap = screenHeigt / 2048 * 130;
            UiUtil.setMarginToView(usernameED, (int)(2*unitWidth), (int)thirdTopGap, (int)(2*unitWidth), 0);


            double fourthTopGap = screenHeigt / 2048 * 94;
            UiUtil.setMarginToView(passwordED, (int)(2*unitWidth), (int)fourthTopGap, (int)(2*unitWidth), 0);

            double fifthTopGap = screenHeigt / 2048 * 121;
            UiUtil.setMarginToView(registerButton,(int)(2*unitWidth), (int)fifthTopGap, (int)(2*unitWidth), 0);

            registerButton.getLayoutParams().height = (int)(unitWidth*1.392);


        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.registerButton:

                //Sayfadaki kayıt ol düğmesine tıklayan kullanıcılar için tetiklenir
                mFirebaseAnalytics.logEvent("REGISTEREMAIL_REGBUTTON_CLICK", null);

                tryCount = Constants.tryNumber;
                String email = usernameED.getText().toString();
                String password = passwordED.getText().toString();


                //Fabric Event - Register Page Register Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click Register Button Via Register Page"));

                if (!Utils.isValidEmail(email)) {
                    DialogUtil.mentalUpErrorMsg(RegisterActivity.this, getString(R.string.localerrorcode2));


                    //Fabric Event - Register sayfası Hatalı Kayıt Sonrası
                    Answers.getInstance().logSignUp(new SignUpEvent()
                            .putMethod("RegisterWithEmail")
                            .putSuccess(false));

                } else if (SpecialCharChecker.isSpecialCharHas(email)) {
                    DialogUtil.mentalUpErrorMsg(RegisterActivity.this, getString(R.string.localerrorcode3));


                    //Fabric Event - Register sayfası Hatalı Kayıt Sonrası
                    Answers.getInstance().logSignUp(new SignUpEvent()
                            .putMethod("RegisterWithEmail")
                            .putSuccess(false));

                } else if (SpecialCharChecker.isPasswordMinLengthsEnough(password)) {
                    DialogUtil.mentalUpErrorMsg(RegisterActivity.this, getString(R.string.localerrorcode5));


                    //Fabric Event - Register sayfası Hatalı Kayıt Sonrası
                    Answers.getInstance().logSignUp(new SignUpEvent()
                            .putMethod("RegisterWithEmail")
                            .putSuccess(false));

                } else if (SpecialCharChecker.isSpecialCharHas(password)) {
                    DialogUtil.mentalUpErrorMsg(RegisterActivity.this, getString(R.string.localerrorcode4));


                    //Fabric Event - Register sayfası Hatalı Kayıt Sonrası
                    Answers.getInstance().logSignUp(new SignUpEvent()
                            .putMethod("RegisterWithEmail")
                            .putSuccess(false));

                } else {
                    doRegisterOnServer(email, password);
                }
                break;
        }
    }


    private void doRegisterOnServer(final String email, final String password ) {
        myTrace.start();


        if (tryCount != 0) {
            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("email", email);
                jsonParams.put("password", password);
                jsonParams.put("language", Utils.getLanguage());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            final SweetAlertDialog loadingProgress = DialogUtil.showProgressDialog(this, getString(R.string.alert_register_message), R.color.colorPrimary);
            HttpClient.directPostWithJsonEntity(RegisterActivity.this, Constants.S_REGISTER, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {

                @Override
                public void onSuccess(JSONObject response) {
                    myTrace.stop();
                    try {
                        String status = response.getString("status");
                        if (status.equals("SUCCESS")) {

                            tryCount = 0;

                            //email yolu ile kayıt olan kullanıcılar için tetiklenir.
                            Bundle param = new Bundle();
                            param.putString(FirebaseAnalytics.Param.SIGN_UP_METHOD, "email");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, param);

                            //Fabric Event - Register sayfası Başarılı Kayıt Sonrası
                            Answers.getInstance().logSignUp(new SignUpEvent()
                                    .putMethod("RegisterWithEmail")
                                    .putSuccess(true));

                            PreferencesHelper.getInstance(RegisterActivity.this).setUsercomeback(true);
                            PreferencesHelper.getInstance(RegisterActivity.this).setUsername(response.getString("userid"));
                            PreferencesHelper.getInstance(RegisterActivity.this).setEmail(email);
                            PreferencesHelper.getInstance(RegisterActivity.this).setPassword(password);
                            PreferencesHelper.getInstance(RegisterActivity.this).setUserLang(Utils.getLanguage());

                            //TODO:Start Asynchronous Service for Send Register Token
                            String notificationToken = FirebaseInstanceId.getInstance().getToken();
                            Utils.asekronNotificationToken(getApplicationContext(), notificationToken, response.getString("userid"));


                            navigateToOnBoard();
                        } else {
                            String error = response.getString("errorCode");
                            if (error.endsWith("3")||error.endsWith("4")||error.endsWith("8")||error.endsWith("9")||error.endsWith("10")) {
                                if (tryCount == 1) {


                                    //Fabric Event - Register sayfası Hatalı Kayıt Sonrası
                                    Answers.getInstance().logSignUp(new SignUpEvent()
                                            .putMethod("RegisterWithEmail")
                                            .putSuccess(false));
                                    serverErrorMessages.ShowErrorMessages(RegisterActivity.this, error);
                                } else {
                                    try {
                                        Thread.sleep(250);
                                    } catch (InterruptedException e2) {

                                    }
                                    tryCount = tryCount - 1;
                                    doRegisterOnServer(email, password);

                                }
                            }else{

                                Answers.getInstance().logSignUp(new SignUpEvent()
                                        .putMethod("RegisterWithEmail")
                                        .putSuccess(false));
                                serverErrorMessages.ShowErrorMessages(RegisterActivity.this, error);

                            }

                        }
                    } catch (JSONException e) {

                        if (tryCount == 1) {
                            DialogUtil.mentalUpErrorMsg(RegisterActivity.this, getString(R.string.localerrorcode6));
                        }else {
                            try {
                                Thread.sleep(250);
                            } catch (InterruptedException e2) {

                            }
                            tryCount = tryCount - 1;
                            doRegisterOnServer(email,password);
                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    myTrace.stop();

                    if (tryCount == 1) {
                        DialogUtil.mentalUpErrorMsg(RegisterActivity.this, getString(R.string.localerrorcode6));
                    }else {
                        try {
                            Thread.sleep(250);
                        } catch (InterruptedException e2) {

                        }
                        tryCount = tryCount - 1;
                        doRegisterOnServer(email,password);
                    }
                }

                @Override
                public void onFinish() {
                    myTrace.stop();
                    super.onFinish();
                    if (loadingProgress != null && loadingProgress.isShowing())
                        loadingProgress.dismiss();
                }
            });
        }
    }
    private void navigateToOnBoard() {
        Intent intent = new Intent(this, OnBoardingFirstActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page
            Intent i = new Intent(RegisterActivity.this, EntryPage.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }


}


