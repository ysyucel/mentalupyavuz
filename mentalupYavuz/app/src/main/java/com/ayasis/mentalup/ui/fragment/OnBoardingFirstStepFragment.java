package com.ayasis.mentalup.ui.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.intrfc.IOnBoardingFirstActivity;
import com.ayasis.mentalup.intrfc.IOnBoardingStepFragment;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.Calendar;

/**
 * Created by AhmtBrK on 17/03/2017.
 */

public class OnBoardingFirstStepFragment extends Fragment implements DatePickerDialog.OnDateSetListener, View.OnClickListener, IOnBoardingStepFragment {
    private FirebaseAnalytics mFirebaseAnalytics;

    public static OnBoardingFirstStepFragment newInstance() {

        Bundle args = new Bundle();

        OnBoardingFirstStepFragment fragment = new OnBoardingFirstStepFragment();
        fragment.setArguments(args);
        return fragment;
    }

    IOnBoardingFirstActivity activity;
    TextView birthDayTV, onboardingFirstmessageHeader, onboardingFirstmessagefun, nameTV, sexTV, mathTV, readwriteTV, birthTV;
    EditText nameSurnameED;
    AppCompatRadioButton radioMale, radioFemale;
    SwitchButton readWriteSwitch, arithmeticSwitch;
    RelativeLayout onboardingFirstmath, onboardingReadWrite;
    LinearLayout onboardingFirstName, onboardingFirstSex, onboardingFirstBirth, onboardingFirstmessageTV;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            activity = (IOnBoardingFirstActivity) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        View view = inflater.inflate(R.layout.fragment_onboarding_first_step, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        birthDayTV = (TextView) view.findViewById(R.id.birthDayTV);
        birthDayTV.setOnClickListener(this);
        nameSurnameED = (EditText) view.findViewById(R.id.nameSurnameED);
        nameSurnameED.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        radioMale = (AppCompatRadioButton) view.findViewById(R.id.radioMale);
        radioFemale = (AppCompatRadioButton) view.findViewById(R.id.radioFemale);
        readWriteSwitch = (SwitchButton) view.findViewById(R.id.readWriteSwitch);
        arithmeticSwitch = (SwitchButton) view.findViewById(R.id.arithmeticSwitch);
        onboardingFirstmath = (RelativeLayout) view.findViewById(R.id.onboardingFirstmath);
        onboardingReadWrite = (RelativeLayout) view.findViewById(R.id.onboardingReadWrite);

        onboardingFirstName = (LinearLayout) view.findViewById(R.id.onboardingFirstName);
        onboardingFirstSex = (LinearLayout) view.findViewById(R.id.onboardingFirstSex);
        onboardingFirstBirth = (LinearLayout) view.findViewById(R.id.onboardingFirstBirth);
        onboardingFirstmessageTV = (LinearLayout) view.findViewById(R.id.onboardingFirstmessageTV);
        birthTV = (TextView) view.findViewById(R.id.birthTV);

        nameTV = (TextView) view.findViewById(R.id.nameTV);
        sexTV = (TextView) view.findViewById(R.id.sexTV);
        readwriteTV = (TextView) view.findViewById(R.id.readwriteTV);
        mathTV = (TextView) view.findViewById(R.id.mathTV);

        onboardingFirstmessageHeader = (TextView) view.findViewById(R.id.onboardingFirstmessageHeader);
        onboardingFirstmessagefun = (TextView) view.findViewById(R.id.onboardingFirstmessagefun);
        manageLayoutMargins();
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.birthDayTV:
                showDatePickerDialog();
                break;
        }
    }


    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @SuppressWarnings("ResourceType")
    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR)-9;
        int month = calendar.get(Calendar.MONTH);

        DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar,this,year,month,01);

        calendar.add(Calendar.YEAR, -3);
        datepickerdialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        datepickerdialog.show();
        //datepickerdialog.getWindow().setBackgroundDrawable();



    }

    @Override
    public void onNextButtonClicked() {
        String nameSurname = nameSurnameED.getText().toString();
        int gender = radioMale.isChecked() ? 2 : 1;
        String birthday = birthDayTV.getText().toString();
        int readSkill = readWriteSwitch.isChecked() ? 1 : 0;
        int arithmeticSkill = arithmeticSwitch.isChecked() ? 1 : 0;

        activity.getOnBoardingStepContainer().setNameSurname(nameSurname);
        activity.getOnBoardingStepContainer().setGender(gender);
        activity.getOnBoardingStepContainer().setBirthDay(birthday);
        activity.getOnBoardingStepContainer().setReadSkill(readSkill);
        activity.getOnBoardingStepContainer().setMathSkill(arithmeticSkill);
        Bundle bundle = new Bundle();
        bundle.putString("noparam"," " );
        mFirebaseAnalytics.logEvent("ONBOARDINGPAGE_CONTBUTTON1_CLICK",bundle);

        //Fabric Event - Onboarding Page First Next Button Click
        Answers.getInstance().logCustom(new CustomEvent("Click Next1 Button Via Onboarding Page"));

    }

    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getActivity()).getDeviceModel();
        if (isTablet == false) {

            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            double firstTopGap = screenHeigt / 1336.0 * 80;
            double secondTopGap = screenHeigt / 1336.0 * 31;
            double thirdTopGap = screenHeigt / 1336.0 * 60;
            double fourthTopGap = screenHeigt / 1336.0 * 45;
            double fifthTopGap = screenHeigt / 1336.0 * 65;
            double sixthTopGap = screenHeigt / 1336.0 * 55;


            UiUtil.setMarginToView(onboardingFirstmessageHeader, (int)unitWidth, (int)firstTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingFirstmessagefun, (int)unitWidth, (int)secondTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingFirstName, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingFirstSex, (int)unitWidth, (int)fourthTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingFirstBirth, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingReadWrite, (int)unitWidth,(int)fifthTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(onboardingFirstmath, (int)unitWidth,(int)sixthTopGap, (int)unitWidth, 0);

        } else {


            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = (screenWidth) / 12;


            double firstTopGap = screenHeigt / 2048 * 120;
            double secondTopGap = screenHeigt / 2048.0 * 45;
            double thirdTopGap = screenHeigt / 2048 * 90;
            double fourthTopGap = screenHeigt / 2048.0 * 67;
            double fifthTopGap = screenHeigt / 2048* 96;
            double sixthTopGap = screenHeigt / 2048 * 82;
            double seventhTopGap = screenHeigt / 2048 * 120;


            UiUtil.setMarginToView(onboardingFirstmessageHeader, (int)(unitWidth*2), (int)firstTopGap, (int)(unitWidth*2), 0);
            UiUtil.setMarginToView(onboardingFirstmessagefun, (int)(unitWidth*2), (int)secondTopGap, (int)(unitWidth*2), 0);
            UiUtil.setMarginToView(onboardingFirstName, (int)(unitWidth*2), (int)seventhTopGap, (int)(unitWidth*2), 0);
            UiUtil.setMarginToView(onboardingFirstSex, (int)(unitWidth*2), (int)fourthTopGap, (int)(unitWidth*2), 0);
            UiUtil.setMarginToView(onboardingFirstBirth, (int)(unitWidth*2), (int)thirdTopGap, (int)(unitWidth*2), 0);
            UiUtil.setMarginToView(onboardingReadWrite, (int)(unitWidth*2),(int)fifthTopGap, (int)(unitWidth*2), 0);
            UiUtil.setMarginToView(onboardingFirstmath, (int)(unitWidth*2),(int)sixthTopGap, (int)(unitWidth*2), 0);

        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        if (currentYear - year >= 0) {
            monthOfYear += 1;
            if (monthOfYear < 10) {
                birthDayTV.setText(dayOfMonth + " / 0" + monthOfYear + " / " + year);
            } else {
                birthDayTV.setText(dayOfMonth + " / " + monthOfYear + " / " + year);
            }
        } else {

        }
    }
}
