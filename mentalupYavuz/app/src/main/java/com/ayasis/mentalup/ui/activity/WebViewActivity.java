package com.ayasis.mentalup.ui.activity;

import android.app.ActivityOptions;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.Constants;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.Utils;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import io.fabric.sdk.android.Fabric;


import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import static com.ayasis.mentalup.util.Utils.asekronLocaleSend;


public class WebViewActivity extends BaseActivity {

    private FirebaseAnalytics mFirebaseAnalytics;


    WebView webView;

    boolean clearHistory = false;
    private GoogleApiClient client;
    boolean firstime = true;

    IInAppBillingService mService;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Locale current = getResources().getConfiguration().locale;
        setContentView(R.layout.activity_webwiew);

        Intent serviceIntent =
                new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        //UI elementelerini bagliyoruz
        webView = (WebView) findViewById(R.id.webview);
        //
        webView.addJavascriptInterface(new CustomJavaScriptInterface(WebViewActivity.this), "settings");


        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            //final SweetAlertDialog loadingProgress = DialogUtil.showProgressDialog(WebViewActivity.this,"", R.color.colorPrimary);

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                //loadingProgress.show();

                if (url.endsWith("/")){
                    url = url.substring(0,url.length()-1);
                }




                if (url.equals(Constants.U_EXIT)) {
                    // Kullanici cikis yapmak istegini urlden anliyoruz. Tuttugumuz degerleri bosa aliyoruz ve

                    PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);

                    //PreferencesHelper.getInstance(getApplicationContext()).setUsername(null);
                    //PreferencesHelper.getInstance(getApplicationContext()).setPassword(null);
                    Intent intent = new Intent(WebViewActivity.this, EntryPage.class);
                    ActivityOptions options =
                            ActivityOptions.makeCustomAnimation(WebViewActivity.this, android.R.anim.slide_in_left, android.R.anim.fade_out);
                    WebViewActivity.this.startActivity(intent, options.toBundle());
                    finish();

                } else {
                    if (url.equals(Constants.U_SALE)) {
                        // Kullanici satin alma sayfasina gitmek istediginde Intent Acarak satis sayfasina yonlendiriyoruz.

                        Intent intent = new Intent(WebViewActivity.this, BuyPremiumFirstStepActivity.class);
                        startActivity(intent);
                        finish();

                        return true;

                    } else {
                        if (url.endsWith(Constants.U_BASE)) {
                            // SessionTimeout oldugunu URLden anliyoruz

                            recreate();

                        } else {
                            if (url.equals(Constants.U_INDEX)){

                                //Fabric Event - Daily Workout Page visit
                                Answers.getInstance().logCustom(new CustomEvent("Visit Daily Workout Page"));

                            }else {
                                if (url.equals(Constants.U_WEB_LOGIN)){
                                    PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);
                                    PreferencesHelper.getInstance(getApplicationContext()).setUsername(null);
                                    PreferencesHelper.getInstance(getApplicationContext()).setPassword(null);
                                    Intent intent = new Intent(WebViewActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

                            }
                        }
                    }
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {

                //if (loadingProgress != null && loadingProgress.isShowing())
                    //loadingProgress.dismiss();

                //Sayfa yuklenmesi tamamlandi, onu halledediyoruz.

                Log.d("URL", url);
                // eger notification sayfa yonlendirdiyse kontrol edip sayfaya atalim :)

                if (Constants.savedNotification){
                    if (url.equals(Constants.U_INDEX)){

                        loadURL(Constants.NotificationPageWebPage);
                        Constants.savedNotification = false;



                    }
                }


                // Kullanci index sayfasindaysa kullanici bilgilerini almak icin javascrit fonksiyonuna bakiyoruz.
                /*if (url.contains(Constants.U_INDEX)) {
                    webView.evaluateJavascript("getCouponNo()", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            String userJson = value.replace("\\", "");
                            userJson = userJson.substring(1,userJson.length()-1);
                            //Stringi Ayiriyoruz
                            try {
                                // Json Objeye atiyoruz, ayirip Preferences a ekliyoruz. Preferences helper util icinde
                                JSONObject userObject = new JSONObject(userJson);
                                String couponNo = userObject.getString("couponNo");
                                if(couponNo.equals(null) || couponNo.equals("null")){
                                    couponNo = "";

                                }
                                Constants.Coupon = couponNo;

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }*/

                super.onPageFinished(view, url);


            }
        });


        boolean usercomeback = PreferencesHelper.getInstance(getApplicationContext()).getUsercomeback();

        // Sayfa acildiginda kullanici durumuna bakip buna gore yonlendirme yapiyoruz.
        if (usercomeback) {
            // Sayfa Acildiginda kullanici onceden gelmisse kullanici adi ve sifresini cekiyoruz.
            String username = PreferencesHelper.getInstance(getApplicationContext()).getUsername();
            String password = PreferencesHelper.getInstance(getApplicationContext()).getPassword();
            String autologinUrl = Constants.U_LOGIN.replace("<USERNAME>", username).replace("<PASSWORD>", password);
            loadURL(autologinUrl);
        } else {
            //Kullanici onceden gelmemisse ana ekrana yonlendiriyoruz.
            //Intent i = new Intent(WebViewActivity.this, EntryPage.class);
            //startActivity(i);
            //finish();
        }


        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }




    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page

            // Kullanicinin geri tusuna basma durumu
            if (webView.canGoBack() == true) {
                //CONTROL COMES HERE IF THERE IS ANY PREVIOUS PAGE
                webView.goBack();
                return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("WebViewActivity Page") //
                .setUrl(Uri.parse("http://mentalup.net"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }


    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());

        webView.onResume();
        webView.reload();


        // Monitor launch times and interval from installation
        //RateThisApp.onStart(this);
        // If the criteria is satisfied, "Rate this app" dialog will be shown
        // Burada Internet durumu kontrol edlilmeli

    }



    @Override
    public void onStop() {
        super.onStop();
        webView.onPause();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        // Burada Internet durumu tekrar kontrol edilmeli


        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    public void loadURL(String url) {

        if (url.endsWith("/")){
            url = url.substring(0,url.length()-1);
        }

        webView.loadUrl(url);
    }

    @Override
    public void onPause(){
        super.onPause();
        webView.onPause();
        //Toast.makeText(WebViewActivity.this, "On Pause",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onResume(){
        super.onResume();
        webView.onResume();
        webView.reload();


    }

    public class CustomJavaScriptInterface {
        Context mContext;

        CustomJavaScriptInterface(Context c) {

            mContext = c;

        }
        @JavascriptInterface
        public void changeLanguage(String bn) {

            PreferencesHelper.getInstance(mContext).setUserLang(bn);
            Utils.setLanguage(getApplicationContext());


        }

        @JavascriptInterface
        public void shareDailySummary(final String message) {



            //Fabric Event - Rate Us click
            Answers.getInstance().logCustom(new CustomEvent("Click Share With Friends"));

            runOnUiThread(new Runnable() {

                public void run() {
                    shareFacebook(message);
                }
            });

        }

        @JavascriptInterface
        public void rateUs(String message){

            //Firebase Event - Rate Us Click
            Bundle bundle = new Bundle();
            bundle.putString("noparam"," " );
            mFirebaseAnalytics.logEvent("RATEPAGE_CLICK",bundle);

            //Fabric Event - Rate Us click
            Answers.getInstance().logCustom(new CustomEvent("Click Rate Us"));

            sendToMarket();

            Toast.makeText(mContext, getString(R.string.rateus_message), Toast.LENGTH_SHORT).show();
        }


    }

    public void share(){

        File imagePath = new File(WebViewActivity.this.getCacheDir(), "images");
        File newFile = new File(imagePath, "image.png");
        Uri contentUri = FileProvider.getUriForFile(WebViewActivity.this, "com.ayasis.mentalupTesting.fileprovider", newFile);

        if (contentUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, "MentalUP.net");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
            shareIntent.setDataAndType(contentUri, getContentResolver().getType(contentUri));
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
            startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        }



    }


    public void saveImage(){


        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                webView.enableSlowWholeDocumentDraw();
            }

            webView.measure(View.MeasureSpec.makeMeasureSpec(
                    View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            webView.layout(0, 0, webView.getMeasuredWidth(),
                    webView.getMeasuredHeight());
            webView.setDrawingCacheEnabled(true);
            webView.buildDrawingCache();
            Bitmap bm = Bitmap.createBitmap(webView.getMeasuredWidth(),
                    webView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);



            Canvas bigcanvas = new Canvas(bm);
            Paint paint = new Paint();
            int iHeight = bm.getHeight();
            bigcanvas.drawBitmap(bm, 0, iHeight, paint);
            webView.draw(bigcanvas);

            File cachePath = new File(WebViewActivity.this.getCacheDir(), "images");
            cachePath.mkdirs(); // don't forget to make the directory
            FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
            bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();

            //share();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void sendToMarket(){

        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }

    }


    public void shareFacebook(String message){

        Bundle param = new Bundle();
        param.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "SHARE_ACHIEVEMENT");
        param.putString(FirebaseAnalytics.Param.ITEM_ID, "AP_V1");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, param);


        String shareLink = "https://play.google.com/store/apps/details?id=com.ayasis.mentalup";
        String facebookQ=  getString(R.string.fb_share_dailysummary).replace("SCORE", message);


        if (Utils.getCountrybySim(WebViewActivity.this).equals("TR")){
            shareLink = "https://play.google.com/store/apps/details?id=com.ayasis.mentalup";
        }else{
            shareLink = "https://www.mentalup.net";
        }


        ShareLinkContent link = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(shareLink))
                .setQuote(facebookQ)
                .build();


        ShareDialog shareDialog = new ShareDialog(WebViewActivity.this);
        shareDialog.show(link, ShareDialog.Mode.AUTOMATIC);

    }

    ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
            //Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_SHORT).show();


            try {

                Bundle activeSubs = mService.getPurchases(3, "com.ayasis.mentalup", "subs", null);

                int response = activeSubs.getInt("RESPONSE_CODE");
                if (response == 0) {

                    ArrayList<String>  purchaseDataList = activeSubs.getStringArrayList("INAPP_PURCHASE_DATA_LIST");

                    for (int i = 0; i < purchaseDataList.size(); ++i) {
                        String purchaseData = purchaseDataList.get(i);
                        try {
                            JSONObject obj = new JSONObject(purchaseData);
                            Log.d("purchaseData", purchaseData);
                            String orderID = obj.getString("orderId");
                            String token = obj.getString("purchaseToken");
                            String storeid = obj.getString("productId");
                            int pstate = obj.getInt("purchaseState");

                            if (pstate == 0){
                                asekronLocaleSend(WebViewActivity.this, token, orderID, storeid);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }



            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    };


}










