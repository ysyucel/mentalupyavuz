package com.ayasis.mentalup.ui.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;

public class quitActivityUnity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quit_unity);
        PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);

        //PreferencesHelper.getInstance(getApplicationContext()).setUsername(null);
        //PreferencesHelper.getInstance(getApplicationContext()).setPassword(null);
        Intent intent = new Intent(quitActivityUnity.this, EntryPage.class);
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(quitActivityUnity.this, android.R.anim.slide_in_left, android.R.anim.fade_out);
        quitActivityUnity.this.startActivity(intent, options.toBundle());
        finish();
    }
}
