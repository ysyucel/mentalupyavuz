package com.ayasis.mentalup.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.ayasis.mental.UnityPlayerActivity.isPremiumUnity;

/**
 * Created by AhmtBrK on 26/02/2017.
 */

public class PreferencesHelper {

    private static PreferencesHelper instance;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    private static final String PROP_USERNAME = "username";
    private static final String PROP_PASSWORD = "password";
    private static final String PROP_USERCOME = "usercomeback";
    private static final String PROP_EMAIL = "e-mail";
    private static final String PROP_DEVICE = "deviceModel";
    private static final String PROP_LANG = "userlanguage";
    private static final String PROP_COUNTRY = "usercountry";
    private static final String PROP_NEWUSER = "isNewUser" ;
    private static final String PROP_UNITY_APP_STATE = "unityState";
    private static final String PROP_UNITYSTATE = "unitystate";
    private static final String PROP_USER_DATA_SEND = "userdatasend";
    private static final String PROP_USER_TOKEN = "usertoken";
    private static final String PROP_USER_ORDERID = "userorederid";
    private static final String PROP_SALE_USERNAME = "saleusername";
    private static final String PROP_IS_PREMIUM = "isPremium";



    private static final String PROP_APP_SIT = "appsit";
    private static final String PROP_APP_STATE = "appstate";
    private PreferencesHelper(Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mEditor = mPreferences.edit();
    }

    public static PreferencesHelper getInstance(Context context) {
        if (instance == null)
            instance = new PreferencesHelper(context);
        return instance;
    }


    public void setUnityAppState (String state){

        mEditor.putString(PROP_UNITY_APP_STATE, state);
        mEditor.commit();

    }
    public void setUsername(String username) {
        mEditor.putString(PROP_USERNAME, username);
        mEditor.commit();
    }
    public void setEmail(String email) {
        mEditor.putString(PROP_EMAIL, email);
        mEditor.commit();
    }
    public void setUnitystate(String unitystate) {
        mEditor.putString(PROP_UNITYSTATE, unitystate);
        mEditor.commit();
    }
    public void setUserLang(String ln){

        mEditor.putString(PROP_LANG, ln);
        mEditor.commit();

    }

    public void setUserCountry(String cn){

        mEditor.putString(PROP_COUNTRY, cn);
        mEditor.commit();

    }

    public void setPassword(String password) {
        mEditor.putString(PROP_PASSWORD, password);
        mEditor.commit();
    }


    public void setUsercomeback(Boolean x) {
        mEditor.putBoolean(PROP_USERCOME, x);
        mEditor.commit();
    }
    public void setUserPremium(Boolean x) {
        mEditor.putBoolean(PROP_IS_PREMIUM, x);
        isPremiumUnity=x.toString();
        mEditor.commit();
    }

    public void setIsNewUser(Boolean x) {
        mEditor.putBoolean(PROP_NEWUSER, x);
        mEditor.commit();
    }

    public void setDeviceModel(Boolean x) {
        mEditor.putBoolean(PROP_DEVICE, x);
        mEditor.commit();
    }

    public void setPurchaseSend(Boolean x){

        mEditor.putBoolean(PROP_USER_DATA_SEND, x);
        mEditor.commit();

    }

    public void setOrderToken(String token){

        mEditor.putString(PROP_USER_TOKEN, token);
        mEditor.commit();

    }

    public void setSaleUserName(String name){

        mEditor.putString(PROP_SALE_USERNAME, name);
        mEditor.commit();

    }


    public void setOrderId(String id){

        mEditor.putString(PROP_USER_ORDERID, id);
        mEditor.commit();

    }

    public void setAppState(String state){

        mEditor.putString(PROP_APP_STATE, state);
        mEditor.commit();
    }
    public void setAppSit(Boolean x) {

        mEditor.putBoolean(PROP_APP_SIT, x);
        mEditor.commit();

    }
    public String getOrderToken() {
        return mPreferences.getString(PROP_USER_TOKEN, "");
    }
    public String getSaleUserName() {
        return mPreferences.getString(PROP_SALE_USERNAME, "");
    }

    public String getOrderId() {
        return mPreferences.getString(PROP_USER_ORDERID, "");
    }

    public boolean getPurchaseSend() {
        return mPreferences.getBoolean(PROP_USER_DATA_SEND, true);
    }

    public String getEmail() {
        return mPreferences.getString(PROP_EMAIL, "");
    }

    public boolean getDeviceModel() {
        return mPreferences.getBoolean(PROP_DEVICE, false);
    }

    public String getUnityAppState(){return mPreferences.getString(PROP_UNITY_APP_STATE, "");}

    public boolean getUsercomeback() {
        return mPreferences.getBoolean(PROP_USERCOME, false);
    }

    public boolean getUserPremium() {
        return mPreferences.getBoolean(PROP_IS_PREMIUM, false);
    }

    public boolean getIsNewUser(){return mPreferences.getBoolean(PROP_NEWUSER, true);}

    public String getUsername() {
        return mPreferences.getString(PROP_USERNAME, "");
    }
    public String getUnitystate() {
        return mPreferences.getString(PROP_UNITYSTATE, "");
    }

    public String getUserLang() { return mPreferences.getString(PROP_LANG, "");}
    public boolean getAppSit() { return mPreferences.getBoolean(PROP_APP_SIT, false);}

    public String getUserCountry(){ return mPreferences.getString(PROP_COUNTRY, ""); }
    public String getPassword() {
        return mPreferences.getString(PROP_PASSWORD, "");
    }
}
