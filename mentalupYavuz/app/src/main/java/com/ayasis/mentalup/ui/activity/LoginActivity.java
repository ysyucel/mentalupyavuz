package com.ayasis.mentalup.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.http.HttpClient;
import com.ayasis.mentalup.http.JsonObjectResponseHandler;
import com.ayasis.mentalup.util.Constants;
import com.ayasis.mentalup.util.DialogUtil;
import com.ayasis.mentalup.util.KeyboardUtils;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.SpecialCharChecker;
import com.ayasis.mentalup.util.UiUtil;
import com.ayasis.mentalup.util.Utils;
import com.ayasis.mentalup.util.serverErrorMessages;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by AhmtBrK on 13/03/2017.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    TextView loginInfoTV, notMemberText, loginInfoHeader;
    RelativeLayout rootLayout, orLayout, headerLayout2;
    LinearLayout contentLayout ;
    EditText usernameED, passwordED;
    Button loginButton;
    Button registerButton, missPassword;
    ImageView headerLogoforLogin;
    int tryCount = Constants.tryNumber;
    private FirebaseAnalytics mFirebaseAnalytics;
    Trace myTrace = FirebasePerformance.getInstance().newTrace("LoginPage");


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_login);


        //Firebase Event: Uygulamanin ilk acildigininda olusan event
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Email ile giriş yapma aksiyonunu tıklayan tüm kullanıcılar için tetiklenir
        mFirebaseAnalytics.logEvent("LOGINEMAIL_VISITED", null);

        //Fabric Event - Login Page visit
        Answers.getInstance().logCustom(new CustomEvent("Visit Login Page"));


        fillViews();
        //checkAutoLogin();
    }



    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            manageLayoutMargins();
        } else {
            manageLayoutMargins();
        }
    }

    private void fillViews() {

        boolean isTablet = PreferencesHelper.getInstance(LoginActivity.this).getDeviceModel();

        Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");

        headerLayout2 = (RelativeLayout) findViewById(R.id.headerLayout2);


        loginInfoHeader = (TextView) findViewById(R.id.logininfoHeader);
        loginInfoHeader.setTypeface(opensansSemibold);

        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
        contentLayout = (LinearLayout) findViewById(R.id.contentLayout);

        loginInfoTV = (TextView) findViewById(R.id.loginInfoTV);
        loginInfoTV.setTypeface(opensansSemibold);


        usernameED = (EditText) findViewById(R.id.usernameED);
        usernameED.setTypeface(opensansRegular);


        passwordED = (EditText) findViewById(R.id.passwordED);
        passwordED.setTypeface(opensansRegular);


        if (!PreferencesHelper.getInstance(getApplicationContext()).getEmail().equals("") &&
                PreferencesHelper.getInstance(getApplicationContext()).getEmail() !=null ){
            usernameED.setText(PreferencesHelper.getInstance(getApplicationContext()).getEmail());

            if (!PreferencesHelper.getInstance(getApplicationContext()).getPassword().equals("") &&
                    PreferencesHelper.getInstance(getApplicationContext()).getPassword() !=null ){
                passwordED.setText(PreferencesHelper.getInstance(getApplicationContext()).getPassword());

            }

        }else {
            usernameED.setText(PreferencesHelper.getInstance(getApplicationContext()).getUsername());
            passwordED.setText(PreferencesHelper.getInstance(getApplicationContext()).getPassword());
        }


        headerLogoforLogin = (ImageView) findViewById(R.id.headerLogoforLogin);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setTypeface(opensansSemibold);

        missPassword = (Button) findViewById(R.id.missPassword);
        missPassword.setTypeface(opensansRegular);

        notMemberText = (TextView) findViewById(R.id.notmembertext);
        notMemberText.setTypeface(opensansRegular);


        loginButton.setTransformationMethod(null);

        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setTypeface(opensansRegular);

        missPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, MissPasswordActivity.class);
                startActivity(i);
                finish();

                //Sayfadaki şifremi unuttum linkine tıklayan kullanıcılar için tetiklenir
                mFirebaseAnalytics.logEvent("LOGINEMAIL_MISSPWD_CLICK", null);


            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
                finish();

                //Sayfanın headerında yer alan kayıt ol düğmesine tıklayan kullanıcılar için tetiklenir
                mFirebaseAnalytics.logEvent("LOGINEMAIL_REGISTERBUTTON_CLICK", null);

                //Fabric Event - Login Page Register Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click Register Button Via Login Page"));

            }
        });
        loginButton.setOnClickListener(this);

        manageLayoutMargins();
        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener()
        {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible)
            {
                formfocused(isVisible);
            }
        });

    }

    public  void formfocused(boolean halil){

        if(halil){

            headerLayout2.setVisibility(View.GONE);
            //userAgreementTV.setVisibility(View.GONE);
        }else{

            headerLayout2.setVisibility(View.VISIBLE);
            //userAgreementTV.setVisibility(View.VISIBLE);
        }



    }

    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(LoginActivity.this).getDeviceModel();
        if (isTablet == false) {

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;
            double logoWidth = (2.2 * unitWidth) ;
            double logoHeight = logoWidth/1.62;
            headerLogoforLogin.getLayoutParams().width=(int)logoWidth;
            headerLogoforLogin.getLayoutParams().height=(int)logoHeight;
            UiUtil.setMarginToView(headerLogoforLogin, (int)(unitWidth/2),(int)(unitWidth/2), 0, (int)(unitWidth/2));

            registerButton.getLayoutParams().width =(int)(logoWidth*1.25);
            registerButton.getLayoutParams().height=(int)(logoWidth/1.8);
            UiUtil.setMarginToView(registerButton, 0,(int)(unitWidth/2), (int)(unitWidth/2), (int)(unitWidth/2));
            UiUtil.setMarginToView(notMemberText, 0,0, (int)(unitWidth/2), 0);

            double firstTopGap = screenHeigt / 1336.0 * 113 ;
            UiUtil.setMarginToView(loginInfoHeader, (int)unitWidth, (int)firstTopGap, (int)unitWidth, 0);

            double secondTopGap= screenHeigt / 1336.0 * 44;
            UiUtil.setMarginToView(loginInfoTV, (int)unitWidth, (int)secondTopGap, (int)unitWidth, 0);

            double thirdTopGap = screenHeigt / 1336.0 * 106;
            UiUtil.setMarginToView(usernameED, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);

            double fourthTopGap = screenHeigt / 1336.0 * 68;
            UiUtil.setMarginToView(passwordED, (int)unitWidth, (int)fourthTopGap, (int)unitWidth, 0);

            double fifthTopGap = screenHeigt / 1336.0 * 74;
            UiUtil.setMarginToView(loginButton,(int)unitWidth, (int)fifthTopGap, (int)unitWidth, 0);

            loginButton.getLayoutParams().height = (int)(unitWidth*1.9);

            double sixthGap = screenHeigt /1336.0 * 45;
            UiUtil.setMarginToView(missPassword,(int)unitWidth , (int)sixthGap, (int)unitWidth, 0);

        } else {


            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            double logoWidth = (2 * unitWidth) ;
            double logoHeight = logoWidth/1.62;
            headerLogoforLogin.getLayoutParams().width=(int)logoWidth;
            headerLogoforLogin.getLayoutParams().height=(int)logoHeight;
            UiUtil.setMarginToView(headerLogoforLogin, (int)(unitWidth/3),(int)(unitWidth/3), 0, (int)(unitWidth/3));

            registerButton.getLayoutParams().width =(int)(logoWidth*1.25);
            registerButton.getLayoutParams().height=(int)(logoWidth/1.8);
            UiUtil.setMarginToView(registerButton, 0,(int)(unitWidth/3), (int)(unitWidth/3), (int)(unitWidth/3));
            UiUtil.setMarginToView(notMemberText, 0,0, (int)(unitWidth/3), 0);

            double firstTopGap = screenHeigt / 2048 * 131 ;
            UiUtil.setMarginToView(loginInfoHeader, (int)(2*unitWidth), (int)firstTopGap, (int)(2*unitWidth), 0);
            double secondTopGap= screenHeigt / 2048.0 * 72;
            UiUtil.setMarginToView(loginInfoTV, (int)(2*unitWidth), (int)secondTopGap, (int)(2*unitWidth), 0);
            double thirdTopGap = screenHeigt / 2048 * 130;
            UiUtil.setMarginToView(usernameED, (int)(2*unitWidth), (int)thirdTopGap, (int)(2*unitWidth), 0);

            double fourthTopGap = screenHeigt / 2048 * 94;
            UiUtil.setMarginToView(passwordED, (int)(2*unitWidth), (int)fourthTopGap, (int)(2*unitWidth), 0);

            double fifthTopGap = screenHeigt / 2048 * 121;
            UiUtil.setMarginToView(loginButton,(int)(2*unitWidth), (int)fifthTopGap, (int)(2*unitWidth), 0);

            loginButton.getLayoutParams().height = (int)(unitWidth*1.392);
            double sixthGap = screenHeigt /2048 * 66;
            UiUtil.setMarginToView(missPassword,(int)(2*unitWidth) , (int)sixthGap, (int)(2*unitWidth), 0);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                tryCount = Constants.tryNumber;
                String email = usernameED.getText().toString();
                String password = passwordED.getText().toString();


                //Sayfadaki giriş yap düğmesine tıklayan kullanıcılar için tetiklenir
                mFirebaseAnalytics.logEvent("LOGINEMAIL_LOGINBUTTON_CLICK", null);

                //Fabric Event - Login Page Login Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click Login Button Via Login Page"));

                if (SpecialCharChecker.checkLoginFields(email, password)) {
                    doLoginOnServer(email, password);

                }else{

                    //Fabric Event - Login sayfası hatalı login sonrası
                    Answers.getInstance().logLogin(new LoginEvent()
                            .putMethod("LoginWithEmail")
                            .putSuccess(false));

                    DialogUtil.mentalUpErrorMsg(LoginActivity.this, getString(R.string.localerrorcode1));
                }
                break;
        }
    }

    public void doLoginOnServer(final String email, final String password) {
        myTrace.start();

        if (tryCount != 0) {
            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("username", email);
                jsonParams.put("password", password);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            final SweetAlertDialog loadingProgress = DialogUtil.showProgressDialog(this, getString(R.string.alert_login_message), R.color.colorPrimary);
            HttpClient.directPostWithJsonEntity(LoginActivity.this, Constants.S_LOGIN, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {

                @Override
                public void onSuccess(JSONObject response) {
                    myTrace.stop();
                    try {
                        String status = response.getString("status");
                        String error = response.getString("errorCode");

                        if (status.equals("SUCCESS")) {
                            //email yolu ile giriş yapan kullanıcılar için tetiklenir.
                            Bundle param = new Bundle();
                            param.putString("LOGIN_METHOD", "email");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, param);

                            //Fabric Event - Login sayfası başarılı login sonrası
                            Answers.getInstance().logLogin(new LoginEvent()
                                    .putMethod("LoginWithEmail")
                                    .putSuccess(true));

                            PreferencesHelper.getInstance(getApplicationContext()).setUsername(response.getString("userid"));
                            PreferencesHelper.getInstance(getApplicationContext()).setEmail(email);
                            PreferencesHelper.getInstance(getApplicationContext()).setPassword(password);
                            PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(true);
                            PreferencesHelper.getInstance(getApplicationContext()).setUserLang(response.getString("language"));

                            //TODO:Start Asynchronous Service for Send Register Token
                            String notificationToken = FirebaseInstanceId.getInstance().getToken();
                            Utils.asekronNotificationToken(getApplicationContext(), notificationToken, response.getString("userid"));

                            navigateToWebView();
                        } else {
                            if (error.endsWith("3")||error.endsWith("4")||error.endsWith("8")||error.endsWith("9")||error.endsWith("10")) {
                                if (tryCount == 1) {

                                    serverErrorMessages.ShowErrorMessages(LoginActivity.this, error);

                                    //Fabric Event - Login sayfası hatalı login sonrası
                                    Answers.getInstance().logLogin(new LoginEvent()
                                            .putMethod("LoginWithEmail")
                                            .putSuccess(false));

                                } else {
                                    try {
                                        Thread.sleep(250);
                                    } catch (InterruptedException e2) {

                                    }

                                    tryCount = tryCount - 1;
                                    doLoginOnServer(email, password);

                                }
                            }else{

                                serverErrorMessages.ShowErrorMessages(LoginActivity.this, error);

                                //Fabric Event - Login sayfası hatalı login sonrası
                                Answers.getInstance().logLogin(new LoginEvent()
                                        .putMethod("LoginWithEmail")
                                        .putSuccess(false));

                            }
                        }
                    } catch (JSONException e) {


                        if (tryCount == 1) {
                            DialogUtil.mentalUpErrorMsg(LoginActivity.this, getString(R.string.localerrorcode6));
                        }else {
                            try {
                                Thread.sleep(250);
                            } catch (InterruptedException e2) {

                            }
                            tryCount = tryCount -1;
                            doLoginOnServer(email,password);

                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    myTrace.stop();
                    if (tryCount == 1) {
                        DialogUtil.mentalUpErrorMsg(LoginActivity.this, getString(R.string.localerrorcode6));
                    }else {
                        try {
                            Thread.sleep(250);
                        } catch (InterruptedException e2) {

                        }
                        tryCount = tryCount -1;
                        doLoginOnServer(email,password);

                    }
                }

                @Override
                public void onFinish() {
                    myTrace.stop();
                    super.onFinish();
                    if (loadingProgress != null && loadingProgress.isShowing())
                        loadingProgress.dismiss();
                }
            });
        }
    }

    private void navigateToWebView() {
        Intent intent = new Intent(this, NativeCallUnity.class);
        startActivity(intent);
        finish();
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page
            Intent i = new Intent(LoginActivity.this, EntryPage.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

}
