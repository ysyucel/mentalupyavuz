package com.ayasis.mentalup.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ayasis.mentalup.ui.fragment.OnBoardingFirstStepFragment;
import com.ayasis.mentalup.ui.fragment.OnBoardingSecondStepFragment;

/**
 * Created by AhmtBrK on 17/03/2017.
 */

public class OnBoardingPagerAdapter extends FragmentPagerAdapter {

    public OnBoardingPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return OnBoardingFirstStepFragment.newInstance();
            case 1:
                return OnBoardingSecondStepFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
