package com.ayasis.mentalup.ui.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.Utils;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import static com.ayasis.mental.UnityPlayerActivity.facebookMessageUnity;

public class unityFacebookShare extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unity_facebook_share);
        Bundle param = new Bundle();
        param.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "SHARE_ACHIEVEMENT");
        param.putString(FirebaseAnalytics.Param.ITEM_ID, "AP_V1");


        String shareLink = "https://play.google.com/store/apps/details?id=com.ayasis.mentalup";
        String facebookQ=  getString(R.string.fb_share_dailysummary).replace("SCORE", facebookMessageUnity);


        if (Utils.getCountrybySim(unityFacebookShare.this).equals("TR")){
            shareLink = "https://play.google.com/store/apps/details?id=com.ayasis.mentalup";
        }else{
            shareLink = "https://www.mentalup.net";
        }


        ShareLinkContent link = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(shareLink))
                .setQuote(facebookQ)
                .build();


        ShareDialog shareDialog = new ShareDialog(unityFacebookShare.this);
        shareDialog.show(link, ShareDialog.Mode.AUTOMATIC);
        finish();
    }


}
