package com.ayasis.mentalup.util;


/**
 * Created by AhmtBrK on 25/02/2017.
 */

public class Constants {

    public static String Coupon = " ";


    public static int tryNumber = 5;

    public static int langChanged = 0;

    public static String NotificationPageWebPage = "";

    public static boolean savedNotification = false;

    public static int navigationFromSplash = 0 ;




    public static final String S_LOGIN = "https://www.mentalup.net/MobileWebService/MobileService.svc/login";
    public static final String S_REGISTER = "https://www.mentalup.net/MobileWebService/MobileService.svc/newUserAndroid";
    public static final String S_UPDATE = "https://www.mentalup.net/MobileWebService/MobileService.svc/updateUserInfo";
    public static final String S_SALE = "https://www.mentalup.net/MobileWebService/MobileService.svc/getItemListAndroid";
    public static final String S_SALE_SAVE = "https://www.mentalup.net/MobileWebService/MobileService.svc/itemSaleAndroid";
    public static final String S_MISSING_SALE = "https://www.mentalup.net/MobileWebService/MobileService.svc/missingAndroidSale";
    public static final String S_NOTIFICATIONTOKEN = "https://www.mentalup.net/MobileWebService/MobileService.svc/updateAndroidNotificationToken";
    public static final String S_CONNECT = "https://www.mentalup.net/MobileWebService/MobileService.svc/connectAndroid";

    public static final String U_BASE = "https://www.mentalup.net";
    public static final String U_EXIT = "https://www.mentalup.net/Application/MentalUP/handler/game/exit";
    public static final String U_LOGIN = "https://www.mentalup.net/mobile/giris-yap?username=<USERNAME>&password=<PASSWORD>";
    public static final String U_SALE = "https://www.mentalup.net/Application/MentalUP/handler/game/sale-packet-selection";
    public static final String U_INDEX = "https://www.mentalup.net/Application/MentalUP/handler/game/index";
    public static final String U_WEB_LOGIN = "https://www.mentalup.net/mobile/giris-yap";




    //final String indexTest = "http://test.mentalup.net/Application/MentalUP/handler/game/index" ;
    //final String testSatinalma = "http://test.mentalup.net/Application





    // Test URL Values

    /*

    public static final String S_LOGIN = "http://test.mentalup.net/MobileWebService/MobileService.svc/login";
    public static final String S_REGISTER = "http://test.mentalup.net/MobileWebService/MobileService.svc/newUserAndroid";
    public static final String S_UPDATE = "http://test.mentalup.net/MobileWebService/MobileService.svc/updateUserInfo";
    public static final String S_SALE = "http://test.mentalup.net/MobileWebService/MobileService.svc/getItemListAndroid";
    public static final String S_SALE_SAVE = "http://test.mentalup.net/MobileWebService/MobileService.svc/itemSaleAndroid";
    public static final String S_MISSING_SALE = "http://test.mentalup.net/MobileWebService/MobileService.svc/missingAndroidSale";

    public static final String S_NOTIFICATIONTOKEN = "http://test.mentalup.net/MobileWebService/MobileService.svc/updateAndroidNotificationToken";

    public static final String S_CONNECT = "http://test.mentalup.net/MobileWebService/MobileService.svc/connectAndroid";


    public static final String U_BASE = "mentalup.net";
    public static final String U_EXIT = "http://test.mentalup.net/Application/MentalUP/handler/game/exit";
    public static final String U_LOGIN = "http://test.mentalup.net/mobile/giris-yap?username=<USERNAME>&password=<PASSWORD>";
    public static final String U_SALE = "http://test.mentalup.net/Application/MentalUP/handler/game/sale-packet-selection";
    public static final String U_INDEX = "http://test.mentalup.net/Application/MentalUP/handler/game/index";
    public static final String U_WEB_LOGIN = "http://test.mentalup.net/mobile/giris-yap";


    */



}
