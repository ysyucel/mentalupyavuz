package com.ayasis.mentalup.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sercesh on 24/03/2017.
 */

public class SpecialCharChecker {


    public static boolean checkLoginFields(String email, String password) {

        if (isMinLengthsEnough(email,password)){
            return false;
        }
        if (isSpecialCharHas(email)) {
            return false;
        }
        if (isSpecialCharHas(password)) {
            return false;
        }

        return true;
    }

    public static boolean isUserEmailEmpty(String value){
        if (value.length() < 1){
            return true;
        }
        else {

            return false;
        }

    }

    public static boolean isMinLengthsEnough(String email,String password){
        if ( password.length() < 6 ){
            return true;
        }else if (email.length() < 1){
            return true;
        }else {
            return false;
        }
    }

    public static boolean isPasswordMinLengthsEnough(String password){
        if (password.length() < 6 ){
            return true;
        }else{
            return false;
        }
    }

    public static boolean isSpecialCharHas(String value) {
        List<String> characters = new ArrayList<>();
        characters.add("\\");
        characters.add("<");
        characters.add(">");
        characters.add("!");
        characters.add("=");
        characters.add("\\");
        characters.add("%");
        characters.add(";");
        characters.add("(");
        characters.add(")");
        characters.add("$");
        characters.add("+");
        characters.add("^");
        characters.add("'");
        characters.add("(char) 26");
        characters.add("(char) 9");
        characters.add("(char) 13");
        characters.add("(char) 8");
        characters.add("(char) 0");
        characters.add("&");
            characters.add(", ");
            characters.add("/");
            characters.add("'\'");


        for (int i = 0; i < characters.size(); i++) {
            if (value.contains(characters.get(i))) {
                return true;
            }
        }
        return false;
    }
}
