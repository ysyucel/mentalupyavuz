package com.ayasis.mentalup.ui.activity;

import android.app.ActivityManager;

import android.content.Intent;
import android.content.pm.ActivityInfo;

import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.annotation.Nullable;

import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ImageView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.LinearLayout;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.Constants;

import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by AhmtBrK on 25/02/2017.
 */

public class BuyPremiumFirstStepActivity extends BaseActivity implements View.OnClickListener{

    private RelativeLayout headerLayoutBeforeSale;
    private ImageView headerLogoBeforeSale;
    private TextView beforeSaleHeaderTV;
    private LinearLayout beforeSalefirstlayer;
    private ImageView firstImageBeforeSale;
    private TextView firstTVBeforeSale;
    private LinearLayout beforeSalesecondlayer;
    private ImageView secondimageBeforeSale;
    private TextView secondTVBeforeSale;
    private LinearLayout beforeSalethirdlayer;
    private ImageView thirdImageBeforeSale;
    private TextView thirdTVBeforeSale;
    private LinearLayout beforeSalefourthlayer;
    private ImageView fourthImageBeforeSale;
    private TextView fourthTVBeforeSale;
    private LinearLayout beforeSalefifthlayer;
    private ImageView fifthImageBeforeSale;
    private TextView fifthTVBeforeSale;
    private Button headerBackButtonBeforeSale, launchButtonBeforeSale;
    private FirebaseAnalytics mFirebaseAnalytics;
    int langChanged = Constants.langChanged;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_premium_first_step);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");
        Typeface opensanslight = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("noparam"," " );
        mFirebaseAnalytics.logEvent("BUYINGINFOPAGE_VISITED",bundle);

        //Fabric Event - Pre Packet Selection Page visit

        Answers.getInstance().logCustom(new CustomEvent("Visit Pre Packet Selection Page"));


        headerLayoutBeforeSale = (RelativeLayout) findViewById(R.id.headerLayoutBeforeSale);
        headerLogoBeforeSale = (ImageView) findViewById(R.id.headerLogoBeforeSale);

        headerBackButtonBeforeSale = (Button) findViewById(R.id.headerBackButtonBeforeSale);
        headerBackButtonBeforeSale.setOnClickListener(this);

        beforeSaleHeaderTV = (TextView) findViewById(R.id.BeforeSaleHeaderTV);
        beforeSaleHeaderTV.setTypeface(opensansSemibold);

        beforeSalefirstlayer = (LinearLayout) findViewById(R.id.BeforeSalefirstlayer);
        firstImageBeforeSale = (ImageView) findViewById(R.id.firstImageBeforeSale);
        firstTVBeforeSale = (TextView) findViewById(R.id.firstTVBeforeSale);
        firstTVBeforeSale.setTypeface(opensansRegular);

        beforeSalesecondlayer = (LinearLayout) findViewById(R.id.BeforeSalesecondlayer);
        secondimageBeforeSale = (ImageView) findViewById(R.id.secondimageBeforeSale);
        secondTVBeforeSale = (TextView) findViewById(R.id.secondTVBeforeSale);
        secondTVBeforeSale.setTypeface(opensansRegular);

        beforeSalethirdlayer = (LinearLayout) findViewById(R.id.BeforeSalethirdlayer);
        thirdImageBeforeSale = (ImageView) findViewById(R.id.thirdImageBeforeSale);
        thirdTVBeforeSale = (TextView) findViewById(R.id.thirdTVBeforeSale);
        thirdTVBeforeSale.setTypeface(opensansRegular);

        beforeSalefourthlayer = (LinearLayout) findViewById(R.id.BeforeSalefourthlayer);
        fourthImageBeforeSale = (ImageView) findViewById(R.id.fourthImageBeforeSale);
        fourthTVBeforeSale = (TextView) findViewById(R.id.fourthTVBeforeSale);
        fourthTVBeforeSale.setTypeface(opensansRegular);

        beforeSalefifthlayer = (LinearLayout) findViewById(R.id.BeforeSalefifthlayer);
        fifthImageBeforeSale = (ImageView) findViewById(R.id.fifthImageBeforeSale);
        fifthTVBeforeSale = (TextView) findViewById(R.id.fifthTVBeforeSale);
        fifthTVBeforeSale.setTypeface(opensansRegular);

        launchButtonBeforeSale =(Button) findViewById(R.id.launchButtonBeforeSale);
        launchButtonBeforeSale.setTypeface(opensansSemibold);
        launchButtonBeforeSale.setOnClickListener(this);

        manageLayoutMargins();


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.headerBackButtonBeforeSale:
                ActivityManager.MemoryInfo memoryInfo = getAvailableMemory();

                if (!memoryInfo.lowMemory) {
                    // Do memory intensive work ...
                    //Intent i = new Intent(BuyPremiumFirstStepActivity.this, WebViewActivity.class);
                    //startActivity(i);
                    finish();
                }else {
                    System.gc();
                    //Intent i = new Intent(BuyPremiumFirstStepActivity.this, WebViewActivity.class);
                    //startActivity(i);
                    finish();
                }

                break;
            case R.id.launchButtonBeforeSale:
                Bundle bundle = new Bundle();
                bundle.putString("noparam"," " );
                mFirebaseAnalytics.logEvent("BUYINGINFOPAGE_BUTTON_CLICK",bundle);


                //Fabric Event - Pre Packet Selection Page Buy Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click Buy Button Via Pre Packet Selection Page"));
                Intent intent = new Intent(BuyPremiumFirstStepActivity.this, BuyPremiumSecondStepActivity.class);

                startActivity(intent);
                finish();
                break;
        }
    }
    private ActivityManager.MemoryInfo getAvailableMemory() {
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        return memoryInfo;
    }


    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();


        if (isTablet == false) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeight = size.y;
            double unitWidth = screenWidth/ 12;

            //Header Block
            double logoWidth = (unitWidth * 2.2);
            double logoHeight = logoWidth / 1.62;
            headerLogoBeforeSale.getLayoutParams().width = (int)logoWidth;
            headerLogoBeforeSale.getLayoutParams().height = (int)logoHeight;
            UiUtil.setMarginToView(headerLogoBeforeSale, 0,(int)(unitWidth / 3), 0, (int)(unitWidth / 3));

            headerBackButtonBeforeSale.getLayoutParams().width = (int)(unitWidth * 0.7);
            headerBackButtonBeforeSale.getLayoutParams().height = (int)(unitWidth * 0.7);
            UiUtil.setMarginToView(headerBackButtonBeforeSale, (int)(unitWidth / 2), 0, 0, 0);

            double imageSize = unitWidth * 1.5;

            firstImageBeforeSale.getLayoutParams().height =(int)imageSize;
            secondimageBeforeSale.getLayoutParams().height =(int)imageSize;
            thirdImageBeforeSale.getLayoutParams().height =(int)imageSize;
            fourthImageBeforeSale.getLayoutParams().height =(int)imageSize;
            fifthImageBeforeSale.getLayoutParams().height =(int)imageSize;

            firstImageBeforeSale.getLayoutParams().width =(int)imageSize;
            secondimageBeforeSale.getLayoutParams().width =(int)imageSize;
            thirdImageBeforeSale.getLayoutParams().width =(int)imageSize;
            fourthImageBeforeSale.getLayoutParams().width =(int)imageSize;
            fifthImageBeforeSale.getLayoutParams().width =(int)imageSize;

            double firstTopGap = screenHeight / 1336.0 * 80;
            double secondTopGap = screenHeight / 1336.0 * 66;
            double thirdTopGap = screenHeight / 1336.0 * 35;
            double bottomGap = screenHeight / 1336.0 * 53;
            double imageMarginRight = unitWidth/2;

            UiUtil.setMarginToView(firstTVBeforeSale, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(secondTVBeforeSale, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(thirdTVBeforeSale, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(fourthTVBeforeSale, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(fifthTVBeforeSale, (int)imageMarginRight, 0, 0, 0);

            UiUtil.setMarginToView(beforeSaleHeaderTV, (int) unitWidth, (int) firstTopGap, (int) unitWidth, 0);
            UiUtil.setMarginToView(beforeSalefirstlayer, (int)(unitWidth), (int)secondTopGap, (int)(unitWidth), 0);
            UiUtil.setMarginToView(beforeSalesecondlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(beforeSalethirdlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(beforeSalefourthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(beforeSalefifthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(launchButtonBeforeSale, (int)unitWidth, (int)0 , (int)unitWidth, (int)bottomGap);

            launchButtonBeforeSale.getLayoutParams().height = (int)(unitWidth * 1.9);

        } else {

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeight = size.y;
            double unitWidth = screenWidth / 12;

            //Header Block
            double logoWidth = (unitWidth * 2.0);
            double logoHeight = logoWidth / 1.62;
            headerLogoBeforeSale.getLayoutParams().width = (int)logoWidth;
            headerLogoBeforeSale.getLayoutParams().height = (int)logoHeight;
            UiUtil.setMarginToView(headerLogoBeforeSale, 0,(int)(unitWidth / 3), 0, (int)(unitWidth / 3));

            headerBackButtonBeforeSale.getLayoutParams().width = (int)(unitWidth * 0.5);
            headerBackButtonBeforeSale.getLayoutParams().height = (int)(unitWidth * 0.5);
            UiUtil.setMarginToView(headerBackButtonBeforeSale, (int)(unitWidth / 2), 0, 0, 0);

            double imageSize = unitWidth * 1.5;

            firstImageBeforeSale.getLayoutParams().height =(int)imageSize;
            secondimageBeforeSale.getLayoutParams().height =(int)imageSize;
            thirdImageBeforeSale.getLayoutParams().height =(int)imageSize;
            fourthImageBeforeSale.getLayoutParams().height =(int)imageSize;
            fifthImageBeforeSale.getLayoutParams().height =(int)imageSize;

            firstImageBeforeSale.getLayoutParams().width =(int)imageSize;
            secondimageBeforeSale.getLayoutParams().width =(int)imageSize;
            thirdImageBeforeSale.getLayoutParams().width =(int)imageSize;
            fourthImageBeforeSale.getLayoutParams().width =(int)imageSize;
            fifthImageBeforeSale.getLayoutParams().width =(int)imageSize;

            double firstTopGap = screenHeight / 2048 * 120;
            double secondTopGap = screenHeight / 2048 * 100;
            double thirdTopGap = screenHeight / 2048 * 75;
            double bottomGap = screenHeight / 2048.0 * 131;
            double imageMarginRight = unitWidth/2;

            UiUtil.setMarginToView(firstTVBeforeSale, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(secondTVBeforeSale, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(thirdTVBeforeSale, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(fourthTVBeforeSale, (int)imageMarginRight, 0, 0, 0);
            UiUtil.setMarginToView(fifthTVBeforeSale, (int)imageMarginRight, 0, 0, 0);

            UiUtil.setMarginToView(beforeSaleHeaderTV, (int) unitWidth, (int) firstTopGap, (int) unitWidth, 0);
            UiUtil.setMarginToView(beforeSalefirstlayer, (int)(unitWidth), (int)secondTopGap, (int)(unitWidth), 0);
            UiUtil.setMarginToView(beforeSalesecondlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(beforeSalethirdlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(beforeSalefourthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(beforeSalefifthlayer, (int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);
            UiUtil.setMarginToView(launchButtonBeforeSale, (int)(unitWidth * 2), (int)0 , (int)(unitWidth * 2), (int)bottomGap);

            launchButtonBeforeSale.getLayoutParams().height = (int)(unitWidth * 1.392);

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page
            //Intent i = new Intent(BuyPremiumFirstStepActivity.this, WebViewActivity.class);
            //startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
