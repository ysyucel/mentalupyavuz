package com.ayasis.mentalup.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;

public class paymentPolicy extends BaseActivity {
    Button useragrementButton;
    TextView userAgreementtitleTV, useragrementfullTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Constants.navigationFromSplash = 2;
        setContentView(R.layout.activity_payment_policy);
        Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Fabric Event - User Agreement Page visit
        //Answers.getInstance().logCustom(new CustomEvent("Visit User Agreement Page"));

        useragrementButton = (Button)findViewById(R.id.paymentpCloseButton);
        userAgreementtitleTV = (TextView) findViewById(R.id.paymentPolicytitleTV);
        useragrementfullTV = (TextView) findViewById(R.id.paymentpfullTV);
        useragrementfullTV.setText(Html.fromHtml(getString(R.string.legalPaymentP)));

        userAgreementtitleTV.setTypeface(opensansSemibold);
        useragrementButton.setTypeface(opensansSemibold);
        useragrementfullTV.setTypeface(opensansRegular);

        useragrementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(paymentPolicy.this, BuyPremiumSecondStepActivity.class);
                startActivity(i);
                finish();
            }
        });
        manageLayoutMargins();

    }



    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();

        if (isTablet == false) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            UiUtil.setMarginToView(useragrementfullTV, (int)unitWidth, (int)(unitWidth/2) , (int)unitWidth,0 );
            UiUtil.setMarginToView(useragrementButton, (int)unitWidth, (int)(unitWidth/2) , (int)unitWidth, (int)(unitWidth/2));

            userAgreementtitleTV.getLayoutParams().height =(int)(unitWidth*1.9);
            useragrementButton.getLayoutParams().height = (int)(unitWidth*1.9);


        } else {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            UiUtil.setMarginToView(useragrementfullTV, (int)unitWidth, (int)(unitWidth/2) , (int)unitWidth,0 );
            UiUtil.setMarginToView(useragrementButton, (int)unitWidth*2, (int)(unitWidth/2) , (int)unitWidth*2, (int)(unitWidth/2));

            userAgreementtitleTV.getLayoutParams().height =(int)(unitWidth*1.392);
            useragrementButton.getLayoutParams().height = (int)(unitWidth*1.392);
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page
            Intent i = new Intent(paymentPolicy.this, BuyPremiumSecondStepActivity.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
