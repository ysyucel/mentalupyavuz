package com.ayasis.mentalup.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.http.HttpClient;
import com.ayasis.mentalup.http.JsonObjectResponseHandler;
import com.ayasis.mentalup.intrfc.IOnBoardingStepFragment;
import com.ayasis.mentalup.ui.adapter.OnBoardingPagerAdapter;
import com.ayasis.mentalup.intrfc.IOnBoardingFirstActivity;
import com.ayasis.mentalup.util.Constants;
import com.ayasis.mentalup.util.DialogUtil;
import com.ayasis.mentalup.util.KeyboardUtils;
import com.ayasis.mentalup.util.NetworkStateReceiver;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;
import com.ayasis.mentalup.util.Utils;
import com.ayasis.mentalup.util.serverErrorMessages;
import com.ayasis.mentalup.vo.OnBoardingStepContainer;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by AhmtBrK on 17/03/2017.
 */

public class OnBoardingFirstActivity extends BaseActivity implements View.OnClickListener, IOnBoardingFirstActivity {

    private ViewPager onBoardingPager;
    private View firstStrokeView, secondStrokeView;
    private Button continueLaunchButton;
    private LinearLayout firstTabLayout, secondTabLayout, onBoardingLayout;
    private TextView onBoardingnumberTV1, onBoardingnumberTV2, onBoardingTextTV1,onBoardingTextTV2;
    private OnBoardingStepContainer onBoardingStepContainer = new OnBoardingStepContainer();
    private FirebaseAnalytics mFirebaseAnalytics;
    int tryCount = Constants.tryNumber;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding_first);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("noparam"," " );
        mFirebaseAnalytics.logEvent("ONBOARDINGPAGE_VISITED",bundle);

        //Fabric Event - Onboarding Page visit
        Answers.getInstance().logCustom(new CustomEvent("Visit Onboarding Page"));


        initViews();
        initOnBoardingPager();
    }

    private void initViews() {
        onBoardingPager = (ViewPager) findViewById(R.id.onBoardingPager);
        firstStrokeView = findViewById(R.id.firstStrokeView);
        secondStrokeView = findViewById(R.id.secondStrokeView);

        firstTabLayout = (LinearLayout) findViewById(R.id.firstTabLayout);
        secondTabLayout = (LinearLayout) findViewById(R.id.secondTabLayout);
        onBoardingnumberTV1 =(TextView)findViewById(R.id.onBoardingnumberTV1);
        onBoardingnumberTV2 =(TextView)findViewById(R.id.onBoardingnumberTV2);

        onBoardingTextTV1 = (TextView) findViewById(R.id.onBoardingTextTV1);
        onBoardingTextTV2 = (TextView) findViewById(R.id.onBoardingTextTV2);
        onBoardingLayout = (LinearLayout) findViewById(R.id.onBoardingLayout);
        continueLaunchButton = (Button) findViewById(R.id.continueLaunchButton);
        continueLaunchButton.setOnClickListener(this);
        firstTabLayout.setOnClickListener(this);
        secondTabLayout.setOnClickListener(this);
        manageLayoutMargins();
    }



    private void initOnBoardingPager() {
        OnBoardingPagerAdapter onBoardingPagerAdapter = new OnBoardingPagerAdapter(getSupportFragmentManager());
        onBoardingPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // nothing to do
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    firstStrokeView.setVisibility(View.VISIBLE);
                    secondStrokeView.setVisibility(View.GONE);
                    continueLaunchButton.setText(getString(R.string.onboarding1_button_text));


                } else if (position == 1) {
                    firstStrokeView.setVisibility(View.GONE);
                    secondStrokeView.setVisibility(View.VISIBLE);
                    continueLaunchButton.setText(getString(R.string.onboarding1_button_text));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // nothing to do
            }
        });

        KeyboardUtils.addKeyboardToggleListener(this, new KeyboardUtils.SoftKeyboardToggleListener()
        {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible)
            {
                formfocused(isVisible);
            }
        });

        onBoardingPager.setAdapter(onBoardingPagerAdapter);
        onBoardingPager.setOffscreenPageLimit(onBoardingPagerAdapter.getCount());
        firstStrokeView.setVisibility(View.VISIBLE);
        secondStrokeView.setVisibility(View.GONE);
        continueLaunchButton.setText(getString(R.string.onboarding1_button_text));
    }

    public  void formfocused(boolean halil){

        if(halil){

            continueLaunchButton.setVisibility(View.GONE);

        }else{

            continueLaunchButton.setVisibility(View.VISIBLE);

        }



    }

    @Override
    public void onClick(View view) {
        int currentItem = onBoardingPager.getCurrentItem();
        switch (view.getId()) {
            case R.id.continueLaunchButton:
                IOnBoardingStepFragment onBoardingStepFragment = getCurrentFragment();
                onBoardingStepFragment.onNextButtonClicked();
                if (currentItem < 1)
                    onBoardingPager.setCurrentItem(currentItem + 1);
                else if (currentItem == 1) {
                    // finish
                }
                break;
            case R.id.firstTabLayout:
                onBoardingPager.setCurrentItem(0);
                break;
            case R.id.secondTabLayout:
                onBoardingPager.setCurrentItem(1);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        int currentItem = onBoardingPager.getCurrentItem();
        if (currentItem > 0) {
            onBoardingPager.setCurrentItem(currentItem - 1);
        } else if (currentItem == 0) {
            // finish
        }
    }

    private IOnBoardingStepFragment getCurrentFragment() {
        try {
            return (IOnBoardingStepFragment) onBoardingPager.getAdapter().instantiateItem(onBoardingPager, onBoardingPager.getCurrentItem());
        } catch (ClassCastException e) {
            return null;
        }
    }

    @Override
    public OnBoardingStepContainer getOnBoardingStepContainer() {
        return onBoardingStepContainer;
    }

    @Override
    public void updateProfileInfo() {

        if (tryCount  != 0) {
            JSONObject jsonParams = new JSONObject();

            try {
                jsonParams.put("username", PreferencesHelper.getInstance(getApplicationContext()).getUsername());
                jsonParams.put("name", Utils.cleanSpecialCharValue(onBoardingStepContainer.getNameSurname()));
                jsonParams.put("birthDate", onBoardingStepContainer.getBirthDay());
                jsonParams.put("gender", onBoardingStepContainer.getGender());
                jsonParams.put("readSkill", onBoardingStepContainer.getReadSkill());
                jsonParams.put("mathSkill", onBoardingStepContainer.getMathSkill());
                jsonParams.put("cat1", onBoardingStepContainer.getCat1());
                jsonParams.put("cat2", onBoardingStepContainer.getCat2());
                jsonParams.put("cat3", onBoardingStepContainer.getCat3());
                jsonParams.put("cat4", onBoardingStepContainer.getCat4());
                jsonParams.put("cat5", onBoardingStepContainer.getCat5());

            } catch (JSONException e) {
                e.printStackTrace();
                DialogUtil.mentalUpErrorMsg(OnBoardingFirstActivity.this, getString(R.string.localerrorcode6));
            }

            final SweetAlertDialog loadingProgress = DialogUtil.showProgressDialog(this, getString(R.string.alert_onboarding_message), R.color.colorPrimary);
            HttpClient.directPostWithJsonEntity(OnBoardingFirstActivity.this, Constants.S_UPDATE, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {


                @Override
                public void onSuccess(JSONObject response) {
                    try {
                        Bundle bundle = new Bundle();
                        String status = response.getString("status");
                        if (status.equals("SUCCESS")) {
                            tryCount = 0;

                            bundle.putString("OnBoardingResult", "Success");
                            mFirebaseAnalytics.logEvent("ONBOARDINGPAGE_CONTBUTTON2_CLICK", bundle);
                            Intent intent = new Intent(OnBoardingFirstActivity.this, OnBoardingSecondActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            String error = response.getString("errorCode");
                            if (error.endsWith("3")||error.endsWith("4")||error.endsWith("8")||error.endsWith("9")||error.endsWith("10")) {
                                if (tryCount == 1) {
                                    bundle.putString("OnBoardingResult", "ServerError");
                                    mFirebaseAnalytics.logEvent("ONBOARDINGPAGE_CONTBUTTON2_CLICK", bundle);

                                    serverErrorMessages.ShowErrorMessages(OnBoardingFirstActivity.this, error);
                                } else {
                                    try {
                                        Thread.sleep(250);
                                    } catch (InterruptedException e2) {

                                    }
                                    tryCount = tryCount - 1;
                                    updateProfileInfo();
                                }
                            }else{
                                bundle.putString("OnBoardingResult", "ServerError");
                                mFirebaseAnalytics.logEvent("ONBOARDINGPAGE_CONTBUTTON2_CLICK", bundle);

                                serverErrorMessages.ShowErrorMessages(OnBoardingFirstActivity.this, error);


                            }
                        }
                    } catch (JSONException e) {
                        if (tryCount == 1) {
                            DialogUtil.mentalUpErrorMsg(OnBoardingFirstActivity.this, getString(R.string.localerrorcode6));
                        }else{
                            try {
                                Thread.sleep(250);
                            } catch (InterruptedException e2) {

                            }
                            tryCount = tryCount -1;
                            updateProfileInfo();
                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    error.printStackTrace();
                    if (tryCount == 1) {
                        DialogUtil.mentalUpErrorMsg(OnBoardingFirstActivity.this, getString(R.string.localerrorcode6));
                    }else{
                        try {
                            Thread.sleep(250);
                        } catch (InterruptedException e2) {

                        }
                        tryCount = tryCount -1;
                        updateProfileInfo();
                    }

                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    if (loadingProgress != null && loadingProgress.isShowing())
                        loadingProgress.dismiss();
                }
            });
        }
    }

    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();

        if (isTablet == false) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;


            UiUtil.setMarginToView(onBoardingLayout, 0, (int)(unitWidth/2) , 0, 0);
            onBoardingnumberTV1.getLayoutParams().height = (int)unitWidth;
            onBoardingnumberTV1.getLayoutParams().width = (int)unitWidth;
            onBoardingnumberTV2.getLayoutParams().height = (int)unitWidth;
            onBoardingnumberTV2.getLayoutParams().width = (int)unitWidth;

            UiUtil.setMarginToView(onBoardingTextTV1, 0, (int)(unitWidth/4) , 0, 0);
            UiUtil.setMarginToView(onBoardingTextTV2, 0, (int)(unitWidth/4) , 0, 0);

            UiUtil.setMarginToView(firstStrokeView, 0, (int)(unitWidth/4) , 0, 0);
            UiUtil.setMarginToView(secondStrokeView, 0, (int)(unitWidth/4) , 0, 0);

            double firstBottomGap = screenHeigt/1334 * 53;
            UiUtil.setMarginToView(continueLaunchButton, (int)unitWidth, (int)0 , (int)unitWidth, (int)firstBottomGap);
            continueLaunchButton.getLayoutParams().height = (int)(unitWidth*1.9);

        } else {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            UiUtil.setMarginToView(onBoardingLayout, 0, (int)(unitWidth / 4) , 0, 0);
            onBoardingnumberTV1.getLayoutParams().height = (int)(unitWidth * 0.75);
            onBoardingnumberTV1.getLayoutParams().width = (int)(unitWidth * 0.75);
            onBoardingnumberTV2.getLayoutParams().height = (int)(unitWidth * 0.75);
            onBoardingnumberTV2.getLayoutParams().width = (int)(unitWidth * 0.75);

            UiUtil.setMarginToView(onBoardingTextTV1, 0, (int)(unitWidth / 6) , 0, 0);
            UiUtil.setMarginToView(onBoardingTextTV2, 0, (int)(unitWidth / 6) , 0, 0);

            UiUtil.setMarginToView(firstStrokeView, 0, (int)(unitWidth / 6) , 0, 0);
            UiUtil.setMarginToView(secondStrokeView, 0,(int)(unitWidth / 6) , 0, 0);

            double firstBottomGap = screenHeigt / 2048 * 131;
            UiUtil.setMarginToView(continueLaunchButton, (int)(unitWidth * 2), 0 , (int)(unitWidth * 2), (int)firstBottomGap);
            continueLaunchButton.getLayoutParams().height = (int)(unitWidth * 1.392);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OnBoardingFirstActivity.this.stopService(new Intent(OnBoardingFirstActivity.this,
                NetworkStateReceiver.class));

    }

}
