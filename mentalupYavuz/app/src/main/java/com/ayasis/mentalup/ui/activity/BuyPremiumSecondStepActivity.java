package com.ayasis.mentalup.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;

import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.ayasis.mentalup.R;
import com.ayasis.mentalup.http.HttpClient;
import com.ayasis.mentalup.http.JsonObjectResponseHandler;
import com.ayasis.mentalup.iab.IabHelper;
import com.ayasis.mentalup.iab.IabResult;
import com.ayasis.mentalup.iab.IabUtil;
import com.ayasis.mentalup.iab.Inventory;
import com.ayasis.mentalup.iab.Purchase;
import com.ayasis.mentalup.util.Constants;
import com.ayasis.mentalup.util.DialogUtil;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;
import com.ayasis.mentalup.util.Utils;
import com.crashlytics.android.answers.AddToCartEvent;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.PurchaseEvent;
import com.crashlytics.android.answers.StartCheckoutEvent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;


import cz.msebera.android.httpclient.entity.StringEntity;
import io.fabric.sdk.android.Fabric;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.loopj.android.http.AsyncHttpClient.log;


/**
 * Created by AhmtBrK on 25/02/2017.
 */

public class BuyPremiumSecondStepActivity extends BaseActivity implements View.OnClickListener {

    String buyingItem;
    private static final int PURCHASE_REQUEST_CODE = 1001;
    private static final String TAG = BuyPremiumSecondStepActivity.class.getClass().getSimpleName();
    IabHelper mIabHelper;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RelativeLayout headerLayout;
    private ImageView headerLogo;
    private Button headerExitButton, headerBackButton;
    private TextView headerText;

    private RelativeLayout firstBuyItemRootLayout;
    private Button firstBuyItemButton;
    private LinearLayout firstBuyItem;
    private TextView firstbuyItemFakePrice;
    private TextView firstbuyItemRealPrice;
    private TextView firstBuyItemPackageName;
    private Button firstBuyItemCoupon;
    private String purcheseIdforfirst, purcheseIdforsecond, purcheseIdforthird;

    private RelativeLayout secondBuyItemRootLayout;
    private Button secondBuyItemButton;
    private LinearLayout secondBuyItem;
    private TextView secondbuyItemFakePrice;
    private TextView secondbuyItemRealPrice;
    private TextView secondBuyItemPackageName;
    private Button secondBuyItemCoupon;

    private RelativeLayout thirdBuyItemRootLayout;
    private Button thirdBuyItemButton;
    private LinearLayout thirdBuyItem;
    private TextView thirdbuyItemFakePrice;
    private TextView thirdbuyItemRealPrice;
    private TextView thirdBuyItemPackageName;
    private Button thirdBuyItemCoupon;
    String firstDuration, secondDuration, thirdDuration;

    IInAppBillingService mService;

    private TextView footerTextTV;

    int tryNumberforSale = Constants.tryNumber;
    int tryNumberforPackpages = Constants.tryNumber;
    String orderNumberForUnsucess;

    Trace myTrace = FirebasePerformance.getInstance().newTrace("Ask Packpages");
    Trace saleTrace = FirebasePerformance.getInstance().newTrace("SaleTrace");


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_buy_premium_second_step);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("noparam"," " );
        mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_VISITED",bundle);




        //Fabric Event - Packet Selection Page visit
        Answers.getInstance().logCustom(new CustomEvent("Visit Packet Selection Page"));

        Fabric.with(this);
        //Optimizely.startOptimizely(getString(R.string.com_optimizely_api_key), getApplication());

        Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");
        Typeface opensanslight = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf");

        //Header Items
        headerLayout = (RelativeLayout) findViewById(R.id.headerLayout);
        headerLogo = (ImageView) findViewById(R.id.headerLogo);

        headerExitButton = (Button) findViewById(R.id.headerExitButton);
        headerExitButton.setOnClickListener(this);
        headerExitButton.setVisibility(View.GONE);

        headerBackButton = (Button) findViewById(R.id.headerBackButton);
        headerBackButton.setOnClickListener(this);

        headerText = (TextView) findViewById(R.id.headerText);
        headerText.setTypeface(opensansSemibold);


        //First Package Items
        firstBuyItemRootLayout = (RelativeLayout) findViewById(R.id.firstBuyItemRootLayout);
        firstBuyItemRootLayout.setOnClickListener(this);

        firstBuyItem = (LinearLayout) findViewById(R.id.firstBuyItem);

        firstBuyItemButton = (Button)  findViewById(R.id.firstBuyItemButton);
        firstBuyItemButton.setOnClickListener(this);
        firstBuyItemButton.setTypeface(opensansRegular);

        firstbuyItemFakePrice = (TextView) findViewById(R.id.firstbuyItemFakePrice);
        firstbuyItemFakePrice.setTypeface(opensansSemibold);

        firstbuyItemRealPrice = (TextView) findViewById(R.id.firstbuyItemRealPrice);
        firstbuyItemRealPrice.setTypeface(opensansSemibold);

        firstBuyItemPackageName = (TextView) findViewById(R.id.firstBuyItemPackageName);
        firstBuyItemPackageName.setTypeface(opensansRegular);

        firstBuyItemCoupon = (Button) findViewById(R.id.firstBuyItemCoupon);
        firstBuyItemCoupon.setOnClickListener(this);
        firstBuyItemCoupon.setTypeface(opensansRegular);




        //Second Package Items
        secondBuyItemRootLayout = (RelativeLayout) findViewById(R.id.secondBuyItemRootLayout);
        secondBuyItemRootLayout.setOnClickListener(this);

        secondBuyItem = (LinearLayout) findViewById(R.id.secondBuyItem);

        secondBuyItemButton = (Button) findViewById(R.id.secondBuyItemButton);
        secondBuyItemButton.setOnClickListener(this);
        secondBuyItemButton.setTypeface(opensansRegular);

        secondbuyItemFakePrice = (TextView) findViewById(R.id.secondbuyItemFakePrice);
        secondbuyItemFakePrice.setTypeface(opensansSemibold);

        secondbuyItemRealPrice = (TextView) findViewById(R.id.secondbuyItemRealPrice);
        secondbuyItemRealPrice.setTypeface(opensansSemibold);

        secondBuyItemPackageName = (TextView) findViewById(R.id.secondBuyItemPackageName);
        secondBuyItemPackageName.setTypeface(opensansRegular);

        secondBuyItemCoupon = (Button) findViewById(R.id.secondBuyItemCoupon);
        secondBuyItemCoupon.setOnClickListener(this);
        secondBuyItemCoupon.setTypeface(opensansRegular);



        //Third Package Items
        thirdBuyItemRootLayout = (RelativeLayout) findViewById(R.id.thirdBuyItemRootLayout);
        thirdBuyItemRootLayout.setOnClickListener(this);

        thirdBuyItem = (LinearLayout) findViewById(R.id.thirdBuyItem);

        thirdBuyItemButton = (Button) findViewById(R.id.thirdBuyItemButton);
        thirdBuyItemButton.setOnClickListener(this);
        thirdBuyItemButton.setTypeface(opensansRegular);

        thirdbuyItemFakePrice = (TextView) findViewById(R.id.thirdbuyItemFakePrice);
        thirdbuyItemFakePrice.setTypeface(opensansSemibold);

        thirdbuyItemRealPrice = (TextView) findViewById(R.id.thirdbuyItemRealPrice);
        thirdbuyItemRealPrice.setTypeface(opensansSemibold);

        thirdBuyItemPackageName = (TextView) findViewById(R.id.thirdBuyItemPackageName);
        thirdBuyItemPackageName.setTypeface(opensansRegular);

        thirdBuyItemCoupon = (Button) findViewById(R.id.thirdBuyItemCoupon);
        thirdBuyItemCoupon.setOnClickListener(this);
        thirdBuyItemCoupon.setTypeface(opensansRegular);



        //footer Items
        footerTextTV = (TextView) findViewById(R.id.footerTextTV);
        footerTextTV.setTypeface(opensanslight);
        footerTextTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Sözleşmeyi inceleyen kullanıcılar için tetiklenir.
                mFirebaseAnalytics.logEvent("ENTRYSCRREN_CLICKEDAGGREEMENT", null);

                //Fabric Event - Welcome Page User Agreement Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click User Aggrement Button Via Welcome Page"));

                Intent i = new Intent(BuyPremiumSecondStepActivity.this, paymentPolicy.class);
                startActivity(i);
                finish();



            }
        });

        manageInAppBilling();

        manageLayoutMargins();

        /*try {
            if (mIabHelper != null) {

                Bundle mm = mIabHelper.getService().getPurchases(3, getApplicationContext().getPackageName(), "subs", null);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }*/

        doAskItemsOnServer(PreferencesHelper.getInstance(BuyPremiumSecondStepActivity.this).getUsername());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            manageLayoutMargins();
        } else {
            manageLayoutMargins();
        }
    }


    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();
        if (isTablet == false) {

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeight = size.y;
            double unitWidth = screenWidth / 12;

            //unit margin
            double unitMargin1 = unitWidth * 0.32;
            double unitMargin2 = unitWidth * 0.32;

            //buyItem button sizes
            double buyItemButtonWidth = unitWidth * 3;
            double buyItemButtonHeight = buyItemButtonWidth * 0.38;

            //discount flag sizes
            double buyItemDiscountFlagWidth = unitWidth * 3.62;
            double buyItemDiscountFlagHeight = buyItemDiscountFlagWidth * 0.305;

            // packpage heights
            //double buyItemHeight = unitWidth * 3.04;

            //Header Block
            double logoWidth = (unitWidth * 2.2);
            double logoHeight = logoWidth / 1.62;
            headerLogo.getLayoutParams().width = (int)logoWidth;
            headerLogo.getLayoutParams().height = (int)logoHeight;
            UiUtil.setMarginToView(headerLogo, 0,(int)(unitWidth / 3), 0, (int)(unitWidth / 3));

            headerExitButton.getLayoutParams().width = (int)(unitWidth * 0.7);
            headerExitButton.getLayoutParams().height = (int)(unitWidth * 0.7);
            UiUtil.setMarginToView(headerExitButton, 0, 0, (int)(unitWidth / 2), 0);

            headerBackButton.getLayoutParams().width = (int)(unitWidth * 0.7);
            headerBackButton.getLayoutParams().height = (int)(unitWidth * 0.7);
            UiUtil.setMarginToView(headerBackButton, (int)(unitWidth / 2), 0, 0, 0);

            //Value Proposition
            double firstGap = (screenHeight * 80) / 1336;
            UiUtil.setMarginToView(headerText, (int)(unitWidth/2), (int)firstGap, (int)(unitWidth/2), 0);


            //Subscription Packet 1
            double secondGap = (66 * screenHeight) / 1336;
            firstBuyItemRootLayout.getLayoutParams().height = (int)(buyItemButtonHeight * 3);
            UiUtil.setMarginToView(firstBuyItemRootLayout, (int)unitWidth, (int)secondGap, (int)unitWidth, 0);

            firstBuyItemButton.getLayoutParams().width = (int)buyItemButtonWidth;
            firstBuyItemButton.getLayoutParams().height =(int)buyItemButtonHeight;
            UiUtil.setMarginToView(firstBuyItemButton, 0, (int)unitMargin1, (int)unitMargin1, 0);

            UiUtil.setMarginToView(firstbuyItemFakePrice, 0, (int)unitMargin1, (int)unitMargin1, 0);
            firstbuyItemFakePrice.setPaintFlags(firstbuyItemFakePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            UiUtil.setMarginToView(firstbuyItemRealPrice, 0, 0, (int)unitMargin1, (int)unitMargin1);

            UiUtil.setMarginToView(firstBuyItemPackageName, (int)unitMargin2, (int)unitMargin2, 0, 0);

            firstBuyItemCoupon.getLayoutParams().width =(int)buyItemDiscountFlagWidth;
            //firstBuyItemCoupon.setText("%15 İndirim");
            //firstBuyItemCoupon.getLayoutParams().height = (int)buyItemDiscountFlagHeight;
            UiUtil.setMarginToView(firstBuyItemCoupon, (int)unitMargin2, 0, 0, 0);


            //Subscription Packet 2
            double thirdGap = (48 * screenHeight) / 1336;
            secondBuyItemRootLayout.getLayoutParams().height = (int)(buyItemButtonHeight * 3);
            UiUtil.setMarginToView(secondBuyItemRootLayout, (int)unitWidth, (int)thirdGap, (int)unitWidth, 0);

            secondBuyItemButton.getLayoutParams().width = (int)buyItemButtonWidth;
            secondBuyItemButton.getLayoutParams().height =(int)buyItemButtonHeight;
            UiUtil.setMarginToView(secondBuyItemButton, 0, (int)unitMargin1, (int)unitMargin1, 0);

            UiUtil.setMarginToView(secondbuyItemFakePrice, 0, (int)unitMargin1, (int)unitMargin1, 0);
            secondbuyItemFakePrice.setPaintFlags(secondbuyItemFakePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            UiUtil.setMarginToView(secondbuyItemRealPrice, 0, 0, (int)unitMargin1, (int)unitMargin1);

            UiUtil.setMarginToView(secondBuyItemPackageName, (int)unitMargin2, (int)unitMargin2, 0, 0);

            secondBuyItemCoupon.getLayoutParams().width =(int)buyItemDiscountFlagWidth;
            //secondBuyItemCoupon.setText("%30 İndirim");
            //secondBuyItemCoupon.getLayoutParams().height = (int)buyItemDiscountFlagHeight;
            UiUtil.setMarginToView(secondBuyItemCoupon, (int)unitMargin2, 0, 0, 0);


            //Subscription Packet 3
            thirdBuyItemRootLayout.getLayoutParams().height = (int)(buyItemButtonHeight * 3);
            UiUtil.setMarginToView(thirdBuyItemRootLayout, (int)unitWidth, (int)thirdGap, (int)unitWidth, 0);

            thirdBuyItemButton.getLayoutParams().width = (int)buyItemButtonWidth;
            thirdBuyItemButton.getLayoutParams().height =(int)buyItemButtonHeight;
            UiUtil.setMarginToView(thirdBuyItemButton, 0, (int)unitMargin1, (int)unitMargin1, 0);

            UiUtil.setMarginToView(thirdbuyItemFakePrice, 0, (int)unitMargin1, (int)unitMargin1, 0);
            thirdbuyItemFakePrice.setPaintFlags(thirdbuyItemFakePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            UiUtil.setMarginToView(thirdbuyItemRealPrice, 0, 0, (int)unitMargin1, (int)unitMargin1);

            UiUtil.setMarginToView(thirdBuyItemPackageName, (int)unitMargin2, (int)unitMargin2, 0, 0);

            thirdBuyItemCoupon.getLayoutParams().width =(int)buyItemDiscountFlagWidth+80;

            //thirdBuyItemCoupon.getLayoutParams().height = (int)buyItemDiscountFlagHeight;
            UiUtil.setMarginToView(thirdBuyItemCoupon, (int)unitMargin2/2,(int)unitMargin2 , 0, 0);

            UiUtil.setMarginToView(footerTextTV, (int)(unitWidth), (int)(thirdGap / 2), (int)(unitWidth), (int)(thirdGap / 2));


        } else {

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeight = size.y;
            double unitWidth = screenWidth / 12;

            //unit margin
            double unitMargin1 = unitWidth * 0.32;
            double unitMargin2 = unitWidth * 0.32;

            //buyItem button sizes
            double buyItemButtonWidth = unitWidth * 2.5;
            double buyItemButtonHeight = buyItemButtonWidth * 0.38;

            //discount flag sizes
            double buyItemDiscountFlagWidth = unitWidth * 3;
            double buyItemDiscountFlagHeight = buyItemDiscountFlagWidth * 0.305;

            // packpage heights
            //double buyItemHeight = unitWidth * 3.04;

            //Header Block
            double logoWidth = (unitWidth * 2);
            double logoHeight = logoWidth / 1.62;
            headerLogo.getLayoutParams().width = (int)logoWidth;
            headerLogo.getLayoutParams().height = (int)logoHeight;
            UiUtil.setMarginToView(headerLogo, 0,(int)(unitWidth / 3), 0, (int)(unitWidth / 3));

            headerExitButton.getLayoutParams().width = (int)(unitWidth * 0.5);
            headerExitButton.getLayoutParams().height = (int)(unitWidth * 0.5);
            UiUtil.setMarginToView(headerExitButton, 0, 0, (int)(unitWidth / 2), 0);

            headerBackButton.getLayoutParams().width = (int)(unitWidth * 0.5);
            headerBackButton.getLayoutParams().height = (int)(unitWidth * 0.5);
            UiUtil.setMarginToView(headerBackButton, (int)(unitWidth / 2), 0, 0, 0);

            //Value Proposition
            double firstGap = (screenHeight * 120) / 2048;
            UiUtil.setMarginToView(headerText, (int)(unitWidth), (int)firstGap, (int)(unitWidth), 0);


            //Subscription Packet 1
            double secondGap = (100 * screenHeight) / 2048;
            firstBuyItemRootLayout.getLayoutParams().height = (int)(buyItemButtonHeight * 3.15);
            UiUtil.setMarginToView(firstBuyItemRootLayout, (int)unitWidth, (int)secondGap, (int)unitWidth, 0);

            firstBuyItemButton.getLayoutParams().width = (int)buyItemButtonWidth;
            firstBuyItemButton.getLayoutParams().height =(int)buyItemButtonHeight;
            UiUtil.setMarginToView(firstBuyItemButton, 0, (int)unitMargin1, (int)unitMargin1, 0);

            UiUtil.setMarginToView(firstbuyItemFakePrice, 0, (int)unitMargin1, (int)unitMargin1, 0);
            firstbuyItemFakePrice.setPaintFlags(firstbuyItemFakePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            UiUtil.setMarginToView(firstbuyItemRealPrice, 0, 0, (int)unitMargin1, (int)unitMargin1);

            UiUtil.setMarginToView(firstBuyItemPackageName, (int)unitMargin2, (int)unitMargin2, 0, 0);

            firstBuyItemCoupon.getLayoutParams().width =(int)buyItemDiscountFlagWidth;
            //firstBuyItemCoupon.getLayoutParams().height = (int)buyItemDiscountFlagHeight;
            UiUtil.setMarginToView(firstBuyItemCoupon, (int)unitMargin2, (int)(1.5*unitMargin2), 0, 0);


            //Subscription Packet 2
            double thirdGap = (73 * screenHeight) / 2048;
            secondBuyItemRootLayout.getLayoutParams().height = (int)(buyItemButtonHeight * 3.15);
            UiUtil.setMarginToView(secondBuyItemRootLayout, (int)unitWidth, (int)thirdGap, (int)unitWidth, 0);

            secondBuyItemButton.getLayoutParams().width = (int)buyItemButtonWidth;
            secondBuyItemButton.getLayoutParams().height =(int)buyItemButtonHeight;
            UiUtil.setMarginToView(secondBuyItemButton, 0, (int)unitMargin1, (int)unitMargin1, 0);

            UiUtil.setMarginToView(secondbuyItemFakePrice, 0, (int)unitMargin1, (int)unitMargin1, 0);
            secondbuyItemFakePrice.setPaintFlags(secondbuyItemFakePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            UiUtil.setMarginToView(secondbuyItemRealPrice, 0, 0, (int)unitMargin1, (int)unitMargin1);

            UiUtil.setMarginToView(secondBuyItemPackageName, (int)unitMargin2, (int)unitMargin2, 0, 0);

            secondBuyItemCoupon.getLayoutParams().width =(int)buyItemDiscountFlagWidth;
           // secondBuyItemCoupon.getLayoutParams().height = (int)buyItemDiscountFlagHeight;
            UiUtil.setMarginToView(secondBuyItemCoupon, (int)unitMargin2, (int)(1.5*unitMargin2), 0, 0);


            //Subscription Packet 3
            thirdBuyItemRootLayout.getLayoutParams().height = (int)(buyItemButtonHeight * 3.15);
            UiUtil.setMarginToView(thirdBuyItemRootLayout, (int)unitWidth, (int)thirdGap, (int)unitWidth, 0);

            thirdBuyItemButton.getLayoutParams().width = (int)buyItemButtonWidth;
            thirdBuyItemButton.getLayoutParams().height =(int)buyItemButtonHeight;
            UiUtil.setMarginToView(thirdBuyItemButton, 0, (int)unitMargin1, (int)unitMargin1, 0);

            UiUtil.setMarginToView(thirdbuyItemFakePrice, 0, (int)unitMargin1, (int)unitMargin1, 0);
            thirdbuyItemFakePrice.setPaintFlags(thirdbuyItemFakePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            UiUtil.setMarginToView(thirdbuyItemRealPrice, 0, 0, (int)unitMargin1, (int)unitMargin1);

            UiUtil.setMarginToView(thirdBuyItemPackageName, (int)unitMargin2, (int)unitMargin2, 0, 0);

            thirdBuyItemCoupon.getLayoutParams().width =(int)buyItemDiscountFlagWidth+100;
            //thirdBuyItemCoupon.getLayoutParams().height = (int)buyItemDiscountFlagHeight;
            UiUtil.setMarginToView(thirdBuyItemCoupon, (int)unitMargin2, (int)(unitMargin2), 0, 0);

            UiUtil.setMarginToView(footerTextTV, (int)(unitWidth), (int)(thirdGap), (int)(unitWidth), (int)(thirdGap));
            //footerTextTV.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            //footerTextTV.setGravity(Gravity.CENTER);


        }
    }


    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putString("noparam"," " );
        switch (view.getId()) {


            case R.id.firstBuyItemRootLayout:
                buyingItem = purcheseIdforfirst;
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK1_CLICK",bundle);
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                    .putItemName(firstDuration + " Aylık")
                    .putItemId(purcheseIdforfirst));


                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforfirst , PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
            case R.id.secondBuyItemRootLayout:

                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK2_CLICK",bundle);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                buyingItem = purcheseIdforsecond;
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                        .putItemName(secondDuration +  " Aylık")
                        .putItemId(purcheseIdforsecond));

                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforsecond , PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
            case R.id.thirdBuyItemRootLayout:

                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK3_CLICK",bundle);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                buyingItem = purcheseIdforthird;
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                        .putItemName(thirdDuration + " Aylık")
                        .putItemId(purcheseIdforthird));

                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforthird, PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
            case R.id.headerBackButton:
                Intent intent = new Intent(BuyPremiumSecondStepActivity.this, BuyPremiumFirstStepActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.headerExitButton:
                Intent i = new Intent(BuyPremiumSecondStepActivity.this, WebViewActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.firstBuyItemButton:

                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK1_CLICK",bundle);

                buyingItem = purcheseIdforfirst;
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                        .putItemName(firstDuration + " Aylık")
                        .putItemId(purcheseIdforfirst));

                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforfirst , PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
            case R.id.secondBuyItemButton:

                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK2_CLICK",bundle);

                buyingItem =purcheseIdforsecond;
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                        .putItemName(secondDuration + " Aylık")
                        .putItemId(purcheseIdforsecond));

                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforsecond , PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
            case R.id.thirdBuyItemButton:
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK3_CLICK",bundle);

                buyingItem =purcheseIdforthird;
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                        .putItemName(thirdDuration + " Aylık")
                        .putItemId(purcheseIdforthird));

                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforthird , PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
            case R.id.firstBuyItemCoupon:
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                buyingItem= purcheseIdforfirst;

                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK1_CLICK",bundle);
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                        .putItemName(firstDuration + " Aylık")
                        .putItemId(purcheseIdforfirst));

                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforfirst , PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
            case R.id.secondBuyItemCoupon:
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK2_CLICK",bundle);
                buyingItem = purcheseIdforsecond;
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                        .putItemName(secondDuration + " Aylık")
                        .putItemId(purcheseIdforsecond));

                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforsecond , PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
            case R.id.thirdBuyItemCoupon:
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                buyingItem = purcheseIdforthird;

                mFirebaseAnalytics.logEvent("PACKETSELECTIONPAGE_PACK3_CLICK",bundle);
                //Fabric Event - Add To Chart
                Answers.getInstance().logAddToCart(new AddToCartEvent()
                        .putItemName(thirdDuration + " Aylık")
                        .putItemId(purcheseIdforthird));

                if (mIabHelper != null) {
                    mIabHelper.launchSubscriptionPurchaseFlow(BuyPremiumSecondStepActivity.this, purcheseIdforthird , PURCHASE_REQUEST_CODE, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
                }
                break;
        }
    }

    public IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase)
        {
            Bundle bundle = new Bundle();

            if (result.isSuccess()){
                saleTrace.start();
                bundle.putString("PRODUCT_ID", buyingItem);
                mFirebaseAnalytics.logEvent("INAPPPURCHASE_SUCCESS", bundle);
                // add flag for unity
                PreferencesHelper.getInstance(getApplicationContext()).setUserPremium(true);
                sendPurchaseInfoToServer(purchase);


            } else{
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                bundle.putString("noparam"," " );
                mFirebaseAnalytics.logEvent("INAPPPURCHASE_FAILED",bundle);
                return;
            }


        }
    };



    private void sendPurchaseInfoToServer(final Purchase purchase) {
        Double price = 0.0;
        String currencyType = "";

        ArrayList<String> skuList = new ArrayList<String>();
        skuList.add(purchase.getSku());

        //Get Price and Currency From Store-------------------------------
        Bundle querySku = new Bundle();
        querySku.putStringArrayList("ITEM_ID_LIST", skuList);


        try {


            Inventory inv = mIabHelper.queryInventory(true, skuList);
            String priceee = inv.getSkuDetails(purchase.getSku()).getPrice();


            Bundle skuDetails = mIabHelper.getService().getSkuDetails(3, getPackageName(), "subs", querySku);
            int response = skuDetails.getInt("RESPONSE_CODE");
            if (response == 0) {
                ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
                String responseJson = responseList.get(0);
                JSONObject responseObject = new JSONObject(responseJson);
                currencyType = responseObject.getString("price_currency_code");
                price = responseObject.getDouble("price_amount_micros");
            } else {
                //Storedan tutari ve para birimini cekemedik, default atiyoruz.
                price = 0.0;
                currencyType = "";
            }
        } catch (Exception e) {
            //Storedan tutari ve para birimini cekemedik, default atiyoruz.
            price = 0.0;
            currencyType = "";
            e.printStackTrace();
        }
        //-------------------------------------------------------------------

        String username = PreferencesHelper.getInstance(getApplicationContext()).getUsername();
        String storeid = skuList.get(0);
        String duration = "0";

        if (storeid.equals(purcheseIdforfirst)) {
            duration = firstDuration;
        } else if (storeid.equals(purcheseIdforsecond)) {
            duration = secondDuration;
        } else {
            duration = thirdDuration;
        }

        String inAppId = purchase.getToken();
        String orderId = purchase.getOrderId();
        if (inAppId == null || inAppId.trim().equals("")) {
            inAppId = "noinappid";
        }

        if (orderId == null || orderId.trim().equals("")) {
            orderId = "noorderid";
        }

        PreferencesHelper.getInstance(getApplicationContext()).setOrderId(orderId);
        PreferencesHelper.getInstance(getApplicationContext()).setOrderToken(inAppId);
        PreferencesHelper.getInstance(getApplicationContext()).setSaleUserName(username);

        JSONObject jsonPara = new JSONObject();
        try {
            jsonPara.put("username", username);
            jsonPara.put("storeid", storeid);
            jsonPara.put("currency", currencyType);
            jsonPara.put("duration", duration);
            jsonPara.put("price", price);
            jsonPara.put("inapppurchaseid", inAppId);
            jsonPara.put("orderid", orderId);
            jsonPara.put("couponNo", Constants.Coupon);
        } catch (JSONException e) {
            Answers.getInstance().logCustom(new CustomEvent("json does not be created"));
        }




        try {

            BigDecimal money = new BigDecimal(price / 1000000);

            //Fabric Event - Start Checkout
            Answers.getInstance().logStartCheckout(new StartCheckoutEvent()
                    .putTotalPrice(money)
                    .putCurrency(Currency.getInstance(currencyType))
                    .putItemCount(1));

            //Fabric Event - Success Purchase
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemPrice(money)
                    .putCurrency(Currency.getInstance(currencyType))
                    .putItemName(duration + " Aylık")
                    .putItemId(storeid)
                    .putSuccess(true));
        } catch (Exception e) {
            //Couldnt write fabric event
        }

        writeValuestoServer(Utils.jsontoStringEntity(jsonPara));

    }


    public void writeValuestoServer(final StringEntity jsonparams){


        if (tryNumberforSale != 0) {

            final SweetAlertDialog loadingProgress = DialogUtil.showProgressDialog(this, getString(R.string.alert_saleregister_message), R.color.colorPrimary);

            HttpClient.directPostWithJsonEntity(BuyPremiumSecondStepActivity.this, Constants.S_SALE_SAVE, jsonparams, new JsonObjectResponseHandler() {

                @Override
                public void onSuccess(JSONObject response) {
                    saleTrace.stop();

                    try {
                        String status = response.getString("status");
                        String error = response.getString("errorCode");

                        if (status.equals("SUCCESS")) {
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            tryNumberforSale = 0;
                            PreferencesHelper.getInstance(getApplicationContext()).setPurchaseSend(true);
                            navigateToWebView();

                        } else {
                            if (tryNumberforSale == 1){
                                //Answers.getInstance().logCustom(new CustomEvent("Purchase_Send_Fail1")
                                        //.putCustomAttribute(PreferencesHelper.getInstance(getApplicationContext()).getUsername(), orderNumberForUnsucess));

                                PreferencesHelper.getInstance(getApplicationContext()).setPurchaseSend(false);
                                DialogUtil.mentalUpErrorMsg(BuyPremiumSecondStepActivity.this, getString(R.string.localerrorcode7));
                            }else{

                                try {
                                    Thread.sleep(1000);
                                    PreferencesHelper.getInstance(getApplicationContext()).setPurchaseSend(false);
                                } catch (InterruptedException e2) {

                                }

                            }
                            tryNumberforSale = tryNumberforSale - 1;
                            writeValuestoServer(jsonparams);

                        }
                    } catch (JSONException e) {

                        if(tryNumberforSale==1){
                            PreferencesHelper.getInstance(getApplicationContext()).setPurchaseSend(false);
                            DialogUtil.mentalUpErrorMsg(BuyPremiumSecondStepActivity.this, getString(R.string.localerrorcode7));
                        }else{

                            try {
                                Thread.sleep(1000);
                                PreferencesHelper.getInstance(getApplicationContext()).setPurchaseSend(false);
                            } catch (InterruptedException e2) {

                            }

                        }

                        tryNumberforSale = tryNumberforSale - 1;
                        writeValuestoServer(jsonparams);
                    }
                }

                @Override
                public void onError(Throwable error) {
                    saleTrace.stop();
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    if (tryNumberforSale==1){
                        PreferencesHelper.getInstance(getApplicationContext()).setPurchaseSend(false);
                        DialogUtil.mentalUpErrorMsg(BuyPremiumSecondStepActivity.this, getString(R.string.localerrorcode7));
                    }else{

                        try {
                            Thread.sleep(1000);
                            PreferencesHelper.getInstance(getApplicationContext()).setPurchaseSend(false);
                        } catch (InterruptedException e2) {

                        }

                    }
                    tryNumberforSale = tryNumberforSale - 1;
                    writeValuestoServer(jsonparams);
                }

                @Override
                public void onFinish() {
                    saleTrace.stop();
                    super.onFinish();
                    if (loadingProgress != null && loadingProgress.isShowing()) {
                        loadingProgress.dismiss();
                    }
                }
            });
        }





    }


    private void manageInAppBilling() {
        mIabHelper = new IabHelper(this, IabUtil.BASE_64_PUBLIC_KEY);
        mIabHelper.enableDebugLogging(true);
        mIabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.v(TAG, "Iab Setup Finished");
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mIabHelper == null)
            return;
        if (!mIabHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mIabHelper != null) {
            mIabHelper.dispose();
            mIabHelper = null;
        }
    }


    public void doAskItemsOnServer(final String username) {
        myTrace.start();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        firstBuyItemRootLayout.measure(0, 0);

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("username", username);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        fillPackpages(jsonParams);

    }

    private void navigateToWebView() {
        Intent intent = new Intent(this, WebViewActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page
            Intent i = new Intent(BuyPremiumSecondStepActivity.this, BuyPremiumFirstStepActivity.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void fillPackpages(final JSONObject jsonparams){

        if (tryNumberforPackpages != 0 ) {

            final SweetAlertDialog loadingProgress = DialogUtil.showProgressDialog(this, getString(R.string.alert_buyservice_message), R.color.colorPrimary);
            HttpClient.directPostWithJsonEntity(BuyPremiumSecondStepActivity.this, Constants.S_SALE, Utils.jsontoStringEntity(jsonparams), new JsonObjectResponseHandler() {

                @Override
                public void onSuccess(JSONObject response) {
                    myTrace.stop();
                    try {
                        String status = response.getString("status");
                        String error = response.getString("errorCode");
                        Constants.Coupon = response.getString("couponNo");
                        JSONArray items = response.getJSONArray("itemList");

                        if (status.equals("SUCCESS")) {
                            //param.putString("LoginResult", "Success");
                            //mFirebaseAnalytics.logEvent("LOGINPAGE_LOGINBUTTON_CLICK",param);
                            tryNumberforPackpages = 0;
                            JSONObject firstPackpage = items.getJSONObject(0);
                            JSONObject secondPackpage = items.getJSONObject(1);
                            JSONObject thirdPackpage = items.getJSONObject(2);


                            firstDuration = firstPackpage.getString("duration");
                            String firstCurrency = firstPackpage.getString("currency");
                            int firstisPopular = firstPackpage.getInt("isPopular");
                            int firstisRenewable = firstPackpage.getInt("isRenewable");
                            String firstpriceDisc = firstPackpage.getString("priceDisc");
                            String firstpriceOrg = firstPackpage.getString("priceOrg");
                            String firststoreId = firstPackpage.getString("storeId").toLowerCase();
                            purcheseIdforfirst = firststoreId;
                            String firstview = firstPackpage.getString("view");
                            int discountRatefirst = thirdPackpage.getInt("discount");

                            String discountRate = getString(R.string.discountRate).replace("XX", String.valueOf(discountRatefirst));
                            firstBuyItemCoupon.setText(discountRate +" "+ getString(R.string.buypageFirstCoupon));


                            if (firstDuration.equals("1")) {
                                firstBuyItemPackageName.setText(getString(R.string.buypageMonthly));
                            } else if (firstDuration.equals("12")) {
                                firstBuyItemPackageName.setText(getString(R.string.buypageYearly));
                            } else {
                                firstBuyItemPackageName.setText(firstDuration + " " + getString(R.string.buypageXMonthly));
                            }

                            firstbuyItemFakePrice.setText(getString(R.string.buypagefirstFakelPricefromServiceContText) +" "+ firstpriceOrg + " "+ firstCurrency );
                            firstbuyItemRealPrice.setText(getString(R.string.buypagefirstFakelPricefromServiceContText) + " " + firstpriceDisc + " " + firstCurrency );


                            if (firstisPopular == 0) {

                                firstBuyItemPackageName.measure(0, 0);//must call measure!
                                firstBuyItemCoupon.setVisibility(View.GONE);


                            } else {

                                firstBuyItemRootLayout.setBackgroundResource(R.drawable.paket_indirimli);
                            }

                            if (firstpriceDisc.equals(firstpriceOrg)) {
                                firstbuyItemFakePrice.setVisibility(View.INVISIBLE);
                            }

                            secondDuration = secondPackpage.getString("duration");
                            String secondcurrency = secondPackpage.getString("currency");
                            int secondisPopular = secondPackpage.getInt("isPopular");
                            int secondisRenewable = secondPackpage.getInt("isRenewable");
                            String secondpriceDisc = secondPackpage.getString("priceDisc");
                            String secondpriceOrg = secondPackpage.getString("priceOrg");
                            String secondstoreId = secondPackpage.getString("storeId").toLowerCase();
                            purcheseIdforsecond = secondstoreId;
                            String secondview = secondPackpage.getString("view");
                            int discountRates2 = thirdPackpage.getInt("discount");


                            secondbuyItemFakePrice.setText(getString(R.string.buypagefirstFakelPricefromServiceContText) + " "+ firstpriceOrg + " " + firstCurrency );
                            secondbuyItemRealPrice.setText(getString(R.string.buypagefirstFakelPricefromServiceContText) + " " + secondpriceDisc + " " + firstCurrency );

                            String discountRatesecond=  getString(R.string.discountRate).replace("XX", String.valueOf(discountRates2));
                            secondBuyItemCoupon.setText(discountRatesecond +" "+ getString(R.string.buypageFirstCoupon));

                            if (secondDuration.equals("1")) {
                                secondBuyItemPackageName.setText(getString(R.string.buypageMonthly));
                            } else if (secondDuration.equals("12")) {
                                secondBuyItemPackageName.setText(getString(R.string.buypageYearly));
                            } else {
                                secondBuyItemPackageName.setText(secondDuration + " " + getString(R.string.buypageXMonthly));
                            }


                            if (secondisPopular == 0) {

                                secondBuyItemPackageName.measure(0, 0);//must call measure!
                                secondBuyItemCoupon.setVisibility(View.GONE);



                            } else {

                                secondBuyItemRootLayout.setBackgroundResource(R.drawable.paket_indirimli);
                            }

                            if (secondpriceDisc.equals(secondpriceOrg)) {
                                secondbuyItemFakePrice.setVisibility(View.INVISIBLE);
                            }

                            thirdDuration = thirdPackpage.getString("duration");
                            String thirdcurrency = thirdPackpage.getString("currency");
                            int thirdisPopular = thirdPackpage.getInt("isPopular");
                            int thirdisRenewable = thirdPackpage.getInt("isRenewable");
                            String thirdpriceDisc = thirdPackpage.getString("priceDisc");
                            String thirdpriceOrg = thirdPackpage.getString("priceOrg");
                            String thirdstoreId = thirdPackpage.getString("storeId");
                            String thirdview = thirdPackpage.getString("view");
                            int discountRate3 = thirdPackpage.getInt("discount");



                            String discountRatethird =  getString(R.string.discountRate).replace("XX", String.valueOf(discountRate3));
                            thirdBuyItemCoupon.setText(discountRatethird +" "+ getString(R.string.buypageFirstCoupon));



                            purcheseIdforthird = thirdstoreId;

                            thirdbuyItemFakePrice.setText(getString(R.string.buypagefirstFakelPricefromServiceContText) + " " + firstpriceOrg + " " + firstCurrency );
                            thirdbuyItemRealPrice.setText(getString(R.string.buypagefirstFakelPricefromServiceContText)  + " "  + thirdpriceDisc + " " + firstCurrency );

                            if (thirdDuration.equals("1")) {
                                thirdBuyItemPackageName.setText(getString(R.string.buypageMonthly));
                            } else if (thirdDuration.equals("12")) {
                                thirdBuyItemPackageName.setText(getString(R.string.buypageYearly));
                            } else {
                                thirdBuyItemPackageName.setText(thirdDuration + " " + getString(R.string.buypageXMonthly));
                            }



                            if (thirdisPopular == 0) {

                                thirdBuyItemPackageName.measure(0, 0);//must call measure!
                                int textHeight = thirdBuyItemPackageName.getMeasuredHeight();  //get height
                                //thirdBuyItemPackageName.setY((int)(buyItemHeight - textHeight) / 2);
                                thirdBuyItemCoupon.setVisibility(View.GONE);


                            } else {

                                thirdBuyItemRootLayout.setBackgroundResource(R.drawable.paket_indirimli);

                                //thirdBuyItemCoupon.setVisibility(View.GONE);
                            }

                            if (thirdpriceDisc.equals(thirdpriceOrg)) {
                                thirdbuyItemFakePrice.setVisibility(View.INVISIBLE);
                            }
                            tryNumberforPackpages = 0;

                        } else {

                            if (tryNumberforPackpages == 1){
                                navigateToWebView();
                            }else{

                                try {
                                    Thread.sleep(250);
                                } catch (InterruptedException e2) {

                                }

                            }
                            tryNumberforPackpages = tryNumberforPackpages - 1;
                            fillPackpages(jsonparams);

                        }

                    } catch (JSONException e) {

                        if (tryNumberforPackpages == 1){
                            navigateToWebView();
                        }else{

                            try {
                                Thread.sleep(250);
                            } catch (InterruptedException e2) {

                            }

                        }
                        tryNumberforPackpages = tryNumberforPackpages - 1;
                        fillPackpages(jsonparams);

                    }
                }

                @Override
                public void onError(Throwable error) {
                    myTrace.stop();

                    if (tryNumberforPackpages == 1){
                        navigateToWebView();
                    }else{

                        try {
                            Thread.sleep(250);
                        } catch (InterruptedException e2) {

                        }

                    }
                    tryNumberforPackpages = tryNumberforPackpages - 1;
                    fillPackpages(jsonparams);

                }


                @Override
                public void onFinish() {
                    myTrace.stop();
                    super.onFinish();
                    if (loadingProgress != null && loadingProgress.isShowing())
                        loadingProgress.dismiss();
                }
            });
        }
    }

}
