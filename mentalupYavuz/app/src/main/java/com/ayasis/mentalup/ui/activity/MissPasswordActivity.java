package com.ayasis.mentalup.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;

public class MissPasswordActivity extends BaseActivity {



    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_miss_password);
        webView = (WebView) findViewById(R.id.misspasswordActivity);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equals("https://www.mentalup.net/mobile/giris-yap")) {
                    Intent i = new Intent(MissPasswordActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                    return false;
                }else {
                    return super.shouldOverrideUrlLoading(view, url);
                }
            }

        });



        if (PreferencesHelper.getInstance(getApplicationContext()).getUserLang().equals("tr")) {

            loadURL("https://www.mentalup.net/mobile/sifremi-unuttum");
        }else{
            if (PreferencesHelper.getInstance(getApplicationContext()).getUserLang().contains("de")){
                loadURL("https://www.mentalup.net/mobile/passwort-vergessen");
            }
            if (PreferencesHelper.getInstance(getApplicationContext()).getUserLang().contains("az")){
                loadURL("https://www.mentalup.net/mobile/sifremi-unuttum-az");
            }
            else{

                loadURL("https://www.mentalup.net/mobile/forgot-password");
            }


        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page
            Intent i = new Intent(MissPasswordActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void loadURL(String url){

        webView.loadUrl(url);

    }
}
