package com.ayasis.mentalup.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.unity3d.player.UnityPlayer;

import java.util.Objects;

import static com.ayasis.mental.UnityPlayerActivity.unityMessageGameEnd;
import static com.ayasis.mental.UnityPlayerActivity.fromTrainingUnity;
import static com.ayasis.mental.UnityPlayerActivity.gameIdUnity;
import static com.ayasis.mental.UnityPlayerActivity.gameLevelUnity;
import static com.ayasis.mentalup.util.PreferencesHelper.*;
import static com.ayasis.mentalup.util.unityCommicationClass.unityMes;
import static com.ayasis.mentalup.util.unityCommicationClass.userNameUnity;
import static com.ayasis.mentalup.util.unityCommicationClass.userPasswordUnity;

public class UnityGamesWebview extends  AppCompatActivity{
    public static int  i=0;
    public static String state="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Context context = null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yavuz_webview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final WebView webView;
        webView = (WebView) findViewById(R.id.webcik);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }


            //Url catch unity için durumu set ediyor geri dondu oyunu bitirdi
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (Objects.equals(url, "https://www.mentalup.net/Application/MentalUP/handler/game/gamesummary")) {
                    unityMessageGameEnd="1";
                    webView.clearCache(true);
                    webView.clearHistory();
                    finish();

                }
                else if (Objects.equals(url, "https://www.mentalup.net/Application/MentalUP/handler/game/unityback")) {
                    //PreferencesHelper.getInstance(getApplicationContext()).setUnitystate("2");
                    unityMessageGameEnd="2";
                    webView.clearCache(true);
                    webView.clearHistory();
                    finish();

                }
                else{
                    unityMessageGameEnd="2";
                }
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {


                super.onPageFinished(view, url);

                //
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Log.e("Error", description);
            }


        });
        userNameUnity = PreferencesHelper.getInstance(context).getUsername();
        userPasswordUnity = PreferencesHelper.getInstance(context).getPassword();

        //Oyun url i
        String url = "https://www.mentalup.net/Application/MentalUP/game/game"+gameIdUnity+"?username="+userNameUnity+"&password="+ userPasswordUnity+"&gameid="+gameIdUnity+"&level="+gameLevelUnity+"&fromdailytraining="+fromTrainingUnity;

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            unityMessageGameEnd="2";
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
