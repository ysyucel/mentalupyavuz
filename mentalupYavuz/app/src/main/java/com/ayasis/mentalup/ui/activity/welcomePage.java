package com.ayasis.mentalup.ui.activity;


import android.content.Intent;
import android.content.pm.ActivityInfo;


import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;

import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;

import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ayasis.mentalup.R;

import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.io.File;


import static com.ayasis.mentalup.R.layout.activity_welcome_page;


public class welcomePage extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    private FirebaseAnalytics mFirebaseAnalytics;
    Button welcomeRegisterButton , welcomeLoginButton, googleSingIn ;
    RelativeLayout welcomeButtonsLayout, welcomeLogoLayout;
    TextView welcomeText, welcomeFunText;
    ImageView welcomeLogo;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_welcome_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mAuth = FirebaseAuth.getInstance();

        Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");


        //Google Sing In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(welcomePage.this.getResources().getString(R.string.default_web_client_idMY))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //Google SingInEnd

        //Firebase Event: WelcomePage Visited
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString("noparam"," " );
        mFirebaseAnalytics.logEvent("WELCOMEPAGE_VISITED",bundle);

        //Fabric Event - Welcome Page visit
        Answers.getInstance().logCustom(new CustomEvent("Visit Welcome Page"));

        welcomeRegisterButton = (Button) findViewById(R.id.welcomeRegisterButton);
        welcomeRegisterButton.setTypeface(opensansSemibold);
        welcomeLoginButton = (Button) findViewById(R.id.welcomeLoginButton);
        welcomeLoginButton.setTypeface(opensansSemibold);

        googleSingIn = (Button) findViewById(R.id.button2);


        welcomeLogoLayout =(RelativeLayout) findViewById(R.id.welcomeLogoLayout);

        welcomeLogo = (ImageView) findViewById(R.id.welcomeLogo);
        if (!PreferencesHelper.getInstance(getApplicationContext()).getUserLang().equals("tr")) {
            //TR degilse resmi degistirelim
            welcomeLogo.setImageResource(R.drawable.mulogoeng);

        }

        welcomeText = (TextView) findViewById(R.id.welcomeText);
        welcomeText.setTypeface(opensansSemibold);
        welcomeFunText = (TextView) findViewById(R.id.welcomeFunText);
        welcomeFunText.setTypeface(opensansRegular);


        welcomeButtonsLayout =(RelativeLayout) findViewById(R.id.welcomeButtonsLayout);

        welcomeRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(welcomePage.this, RegisterActivity.class);
                startActivity(i);
                finish();
                //Firebase Event: WelcomePage Register Action
                Bundle bundle = new Bundle();
                bundle.putString("noparam"," " );
                mFirebaseAnalytics.logEvent("WELCOMEPAGE_REGISTER_CLICK",bundle);

                //Fabric Event - Welcome Page Register Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click Register Button Via Welcome Page"));

            }
        });

        welcomeLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(welcomePage.this, LoginActivity.class);
                startActivity(i);
                finish();


                //Firebase Event: WelcomePage Login Action
                Bundle bundle = new Bundle();
                bundle.putString("noparam"," " );
                mFirebaseAnalytics.logEvent("WELCOMEPAGE_LOGIN_CLICK",bundle);

                //Fabric Event - Welcome Page Login Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click Login Button Via Welcome Page"));
            }
        });

        googleSingIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();

            }
        });



        manageLayoutMargins();


    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();


    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {


        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = mAuth.getCurrentUser();

                            Toast.makeText(welcomePage.this, " Done ",
                                    Toast.LENGTH_SHORT).show();

                        } else {
                            // If sign in fails, display a message to the user.

                            Toast.makeText(welcomePage.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }


    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                welcomeFunText.setText("olmadi");
            }
        }
    }


    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            welcomeFunText.setText(acct.getEmail());

            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));

        } else {

            welcomeFunText.setText("pama");
            // Signed out, show unauthenticated UI.

        }
    }





    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();
        if (isTablet == false) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWith = screenWidth / 12;



            double logoWidth = (unitWith * 6);
            welcomeLogoLayout.getLayoutParams().width =(int)logoWidth;
            welcomeLogoLayout.getLayoutParams().height = (int)(255 * screenHeigt) /1336;
            double firstTopGap = (screenHeigt * 213) / 1336;
            UiUtil.setMarginToView(welcomeLogoLayout, (int)(unitWith * 3), (int)firstTopGap, (int)(unitWith * 3 ), 0);


            double secondGap = (screenHeigt * 100) / 1336 ;
            UiUtil.setMarginToView(welcomeText, (int)unitWith, (int)secondGap, (int)unitWith, 0);

            double thirdGap = (screenHeigt * 80) / 1336;
            UiUtil.setMarginToView(welcomeFunText, (int)(unitWith), (int)thirdGap, (int)(unitWith), 0);

            double buttonWidth= (unitWith * 4);
            double buttonHeight = (buttonWidth/2.5);
            welcomeLoginButton.getLayoutParams().width = (int)buttonWidth ;
            welcomeLoginButton.getLayoutParams().height = (int)buttonHeight;
            welcomeRegisterButton.getLayoutParams().width=(int)buttonWidth ;
            welcomeRegisterButton.getLayoutParams().height=(int)buttonHeight;

            double bottomGap = (screenHeigt*124) / 1336;
            UiUtil.setMarginToView(welcomeButtonsLayout, (int)unitWith, 0, (int)unitWith, (int)bottomGap);


        } else {

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWith = screenWidth / 12;



            double logoWidth = (unitWith * 6);
            welcomeLogoLayout.getLayoutParams().width =(int)logoWidth;
            welcomeLogoLayout.getLayoutParams().height = (int)(255 * screenHeigt) /1024;
            double firstTopGap = (screenHeigt * 184) / 2048;
            UiUtil.setMarginToView(welcomeLogoLayout, (int)(unitWith * 3), (int)firstTopGap, (int)(unitWith * 3 ), 0);


            double secondGap = (screenHeigt * 94) / 2048 ;
            UiUtil.setMarginToView(welcomeText, (int)unitWith, (int)secondGap, (int)unitWith, 0);

            double thirdGap = (screenHeigt * 162) / 2048;
            UiUtil.setMarginToView(welcomeFunText, (int)(unitWith), (int)thirdGap, (int)(unitWith), 0);

            double buttonWidth= (unitWith * 4);
            double buttonHeight = (buttonWidth/2.5);
            welcomeLoginButton.getLayoutParams().width = (int)buttonWidth ;
            welcomeLoginButton.getLayoutParams().height=(int)buttonHeight;
            welcomeRegisterButton.getLayoutParams().width=(int)buttonWidth ;
            welcomeRegisterButton.getLayoutParams().height=(int)buttonHeight;

            double bottomGap = (screenHeigt*166) / 2048;
            UiUtil.setMarginToView(welcomeButtonsLayout, (int)unitWith, 0, (int)unitWith, (int)bottomGap);

        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.

    }
}
