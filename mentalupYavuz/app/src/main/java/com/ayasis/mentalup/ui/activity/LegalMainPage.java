package com.ayasis.mentalup.ui.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;

public class LegalMainPage extends BaseActivity {
    Button legalTOF , legalPP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal_main_page);

        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        legalTOF = (Button)findViewById(R.id.TOF);
        legalTOF.setTypeface(opensansSemibold);
        legalTOF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LegalMainPage.this, userAgreement.class);
                startActivity(i);
                finish();
            }
        });

        legalPP = (Button)findViewById(R.id.PP);
        legalPP.setTypeface(opensansSemibold);
        legalPP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LegalMainPage.this, PrivacyPolicy.class);
                startActivity(i);
                finish();
            }
        });
        manageLayoutMargins();
    }

    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();

        if (isTablet == false) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            UiUtil.setMarginToView(legalTOF, (int)unitWidth, (int)(unitWidth/2) , (int)unitWidth,(int)(2 * unitWidth) );
            UiUtil.setMarginToView(legalPP, (int)unitWidth, (int)(unitWidth/2) , (int)unitWidth, (int)(unitWidth));

            //userAgreementtitleTV.getLayoutParams().height =(int)(unitWidth*1.9);
            legalTOF.getLayoutParams().height = (int)(unitWidth*1.9);
            legalPP.getLayoutParams().height = (int)(unitWidth*1.9);



        } else {

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;

            UiUtil.setMarginToView(legalTOF, (int)unitWidth*2, (int)(unitWidth) , (int)unitWidth*2,(int)(2 * unitWidth) );
            UiUtil.setMarginToView(legalPP, (int)unitWidth*2, (int)(unitWidth) , (int)unitWidth*2, (int)(unitWidth));

            legalPP.getLayoutParams().height =(int)(unitWidth*1.392);
            legalTOF.getLayoutParams().height = (int)(unitWidth*1.392);

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //Ask WebView- If There are any Previously Navigated Page
            Intent i = new Intent(LegalMainPage.this, EntryPage.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
