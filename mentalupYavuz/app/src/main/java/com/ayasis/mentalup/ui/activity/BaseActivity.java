package com.ayasis.mentalup.ui.activity;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ayasis.mentalup.util.Utils;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;


/**
 * Created by AhmtBrK on 18/03/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.gc();
        Utils.setLanguage(getApplicationContext());


    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        System.gc();


    }
    @Override
    protected void onStop(){
        super.onStop();
        System.gc();

    }



}
