package com.ayasis.mentalup.ui.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ayasis.mentalup.R;
import com.ayasis.mentalup.util.PreferencesHelper;

import static com.ayasis.mental.UnityPlayerActivity.unityLang;
import static com.ayasis.mental.UnityPlayerActivity.unityPassword;

public class passSetterUnity extends AppCompatActivity {

    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lang_setter_unity);
        PreferencesHelper.getInstance(context).setPassword(unityPassword);
        finish();

    }
}
