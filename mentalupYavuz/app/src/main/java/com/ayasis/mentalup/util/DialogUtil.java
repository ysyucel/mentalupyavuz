package com.ayasis.mentalup.util;

import android.app.Activity;
import android.content.Context;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;

import android.widget.Button;
import android.widget.TextView;

import com.ayasis.mentalup.R;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by AhmtBrK on 16/05/16.
 */
public class DialogUtil {

    public static void showSweetAlertDialogWithFinish(final Context context, String title, String message, String btnText, int type) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, type)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(btnText)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        ((Activity) context).finish();
                    }
                });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showSweetAlertDialog(final Context context, String title, String message, String btnText, int type) {

        SweetAlertDialog dialog = new SweetAlertDialog(context, type)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(btnText);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showSweetAlertDialogWithListener(final Context context, String title, String message, String btnText, int type, SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, type)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(btnText)
                .setConfirmClickListener(onSweetClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showYesNoSweetAlertDialog(final Context context, String title, String message, String confirmButtonText, String cancelButtonText, SweetAlertDialog.OnSweetClickListener confirmClickListener, SweetAlertDialog.OnSweetClickListener cancelClickListener, int type) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, type)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText(confirmButtonText)
                .setCancelText(cancelButtonText)
                .setConfirmClickListener(confirmClickListener)
                .setCancelClickListener(cancelClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static SweetAlertDialog showProgressDialog(Context context,String text, int colorResId) {
        SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(context, colorResId));
        pDialog.setTitleText(text);
        pDialog.setCancelable(false);
        pDialog.show();

        return pDialog;
    }
    public static void showOurError(Context context, String text){

        new SweetAlertDialog(context)
                .setTitleText(text)
                .show();


    }



    public static void myDialogPage(final Context context){

        Typeface opensansRegular = Typeface.createFromAsset(context.getAssets(),"fonts/OpenSans-Regular.ttf" );
        Typeface opensansSemibold = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Semibold.ttf");

        AlertDialog.Builder builder = new AlertDialog.Builder(context);




        TextView myErrorMsg = new TextView(context);
        myErrorMsg.setText(R.string.errorcode2);
        myErrorMsg.setTypeface(opensansRegular);
        myErrorMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myErrorMsg.setTextSize(14);

        Button myOkbutton = new Button(context);


        /*builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                dialog.dismiss();
            }
        });*/
        AlertDialog dialog = builder.create();
        dialog.show();

    }




    public static void mentalUpErrorMsg(Context context,String description ){

        MentalUpDialog.with(context)
                .setDescription(description)
                .build();
    }



}
