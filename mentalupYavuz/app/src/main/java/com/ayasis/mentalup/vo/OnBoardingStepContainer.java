package com.ayasis.mentalup.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by AhmtBrK on 18/03/2017.
 */

public class OnBoardingStepContainer {
    private String nameSurname;
    private int gender;
    private String birthDay;
    private int readSkill;
    private int mathSkill;
    private int cat1;
    private int cat2;
    private int cat3;
    private int cat4;
    private int cat5;
    private String email;
    private String phone;

    private static SimpleDateFormat sdfOne = new SimpleDateFormat("dd / mm / yyyy");
    private static SimpleDateFormat sdfTwo = new SimpleDateFormat("yyyy-mm-dd");

    public OnBoardingStepContainer() {
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        try {
            Date birthDayDate = sdfOne.parse(birthDay);
            this.birthDay = sdfTwo.format(birthDayDate);
        } catch (ParseException e) {
            e.printStackTrace();
            this.birthDay = birthDay;
        }
    }

    public int getReadSkill() {
        return readSkill;
    }

    public void setReadSkill(int readSkill) {
        this.readSkill = readSkill;
    }

    public int getMathSkill() {
        return mathSkill;
    }

    public void setMathSkill(int mathSkill) {
        this.mathSkill = mathSkill;
    }

    public int getCat1() {
        return cat1;
    }

    public void setCat1(int cat1) {
        this.cat1 = cat1;
    }

    public int getCat2() {
        return cat2;
    }

    public void setCat2(int cat2) {
        this.cat2 = cat2;
    }

    public int getCat3() {
        return cat3;
    }

    public void setCat3(int cat3) {
        this.cat3 = cat3;
    }

    public int getCat4() {
        return cat4;
    }

    public void setCat4(int cat4) {
        this.cat4 = cat4;
    }

    public int getCat5() {
        return cat5;
    }

    public void setCat5(int cat5) {
        this.cat5 = cat5;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public static SimpleDateFormat getSdfOne() {
        return sdfOne;
    }

    public static void setSdfOne(SimpleDateFormat sdfOne) {
        OnBoardingStepContainer.sdfOne = sdfOne;
    }

    public static SimpleDateFormat getSdfTwo() {
        return sdfTwo;
    }

    public static void setSdfTwo(SimpleDateFormat sdfTwo) {
        OnBoardingStepContainer.sdfTwo = sdfTwo;
    }
}
