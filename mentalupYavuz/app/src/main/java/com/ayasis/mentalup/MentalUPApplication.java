package com.ayasis.mentalup;

import android.app.Application;
import com.ayasis.mentalup.util.Foreground;

public class MentalUPApplication extends Application {
    public void onCreate(){
        super.onCreate();
        Foreground.init(this);
    }
}
