package com.ayasis.mentalup.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.ayasis.mentalup.R;

/**
 * Created by AhmtBrK on 23/03/2017.
 */

public class MentalUpDialog {

    private Context mContext;
    private int mIcon = 0;
    private boolean mCancelable = true;



    private String mTitle;
    private String mTitleTextColor;
    private int mTitleTextSize = 14;
    private String mTitleFont;

    private String mDescription;
    private MentalUpDialog(Context context) {
        mContext = context;
    }

    public static MentalUpDialog with(Context context) {
        if (!(context instanceof Activity)) {
            throw new IllegalArgumentException("Context must be instance of Activity");
        }
        MentalUpDialog mentalUpDialog = new MentalUpDialog(context);
        return mentalUpDialog;
    }

    public MentalUpDialog setDescription(String description) {
        mDescription = description;
        return this;
    }


    public void build() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_layout);

        TextView descriptionTV = (TextView) dialog.findViewById(R.id.descriptionTV);

        /* Description Start */
        if (TextUtils.isEmpty(mDescription)) {
            descriptionTV.setVisibility(View.GONE);
        } else {
            descriptionTV.setVisibility(View.VISIBLE);
            descriptionTV.setText(mDescription);
        }
        /* Description End */

        /* Button Start */
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButton);
        dialogButton.setTransformationMethod(null);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        /* Button End */

        dialog.setCancelable(mCancelable);
        dialog.show();
    }
}