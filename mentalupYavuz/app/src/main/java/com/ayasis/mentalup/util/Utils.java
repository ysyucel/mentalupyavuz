package com.ayasis.mentalup.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;

import com.ayasis.mentalup.http.HttpClient;
import com.ayasis.mentalup.http.JsonObjectResponseHandler;
import com.ayasis.mentalup.ui.activity.SplashActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

import static android.content.Context.TELEPHONY_SERVICE;
import static com.loopj.android.http.AsyncHttpClient.log;


/**
 * Created by AhmtBrK on 02/12/15.
 */
public class Utils {

    public static String getShortLanguageCode(Context context) {
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = context.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = context.getResources().getConfiguration().locale;
        }
        String localeString = locale.getLanguage();
        return localeString;
    }

    public static void showAlert(final Context context, String title, String message, String btnText) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(btnText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity) context).finish();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void showAlertWithoutFinish(final Context context, String title, String message, String btnText) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(btnText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static boolean isValidEmail(String emailAddress) {
        EmailValidator validator = new EmailValidator();
        return validator.validate(emailAddress);
    }

    public static void showAlertWithListener(final Context context, String title, String message, String btnText, DialogInterface.OnClickListener clickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(btnText, clickListener);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void showYesNoAlertWithListener(Context context, String title, String message, String btn1Text, String btn2Text, DialogInterface.OnClickListener btn1ClickListener, DialogInterface.OnClickListener btn2ClickListener) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(btn1Text, btn1ClickListener)
                .setNegativeButton(btn2Text, btn2ClickListener);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void openFacebookIntent(Context context, String userId, String userName) {
        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + userId)));
        } catch (Exception e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/" + userName)));
        }
    }

    public static void callPhone(Context context, String phoneNumber) {
        Intent callIntent = new Intent(Intent.ACTION_VIEW);
        callIntent.setData(Uri.parse("tel:" + phoneNumber));
        context.startActivity(callIntent);
    }

    public static void shareLink(Context context, String title, String link) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, link);
        context.startActivity(Intent.createChooser(share, title));
    }

    public static void openTwitterIntent(Context context, String userId, String userName) {
        Intent intent = null;
        try {
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?user_id=" + userId));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + userName));
        }
        context.startActivity(intent);
    }

    public static void openGooglePlayIntent(Context context) {
        final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static void openGooglePlayIntent(Context context, String packageName) {
        final String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    public static void sendEmail(Context context, String mailAddress) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + mailAddress));
        context.startActivity(emailIntent);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static void openMapIntent(Context context, String label, double lat, double lon) {
        String uriBegin = "geo:" + lat + "," + lon;
        String query = lat + "," + lon + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        context.startActivity(intent);
    }

    public static void openWebBrowserIntent(Context context, String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void setAlpha(View view, float alpha) {
        if (Build.VERSION.SDK_INT < 11) {
            final AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
            animation.setDuration(0);
            animation.setFillAfter(true);
            view.startAnimation(animation);
        } else {
            view.setAlpha(alpha);
        }
    }

    public static Drawable setTint(Context context, Drawable d, int color) {
        Drawable wrappedDrawable = DrawableCompat.wrap(d);
        DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(context, color));
        return wrappedDrawable;
    }

    public static View getRowViewByPositionFromListView(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    public static boolean isValidURL(String url) {
        if (!url.startsWith("http//"))
            url = "http://" + url;
        URL u = null;
        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            return false;
        }

        try {
            u.toURI();
        } catch (URISyntaxException e) {
            return false;
        }
        return true;
    }

    public static int getActionBarHeight(Context context) {
        final TypedArray ta = context.getTheme().obtainStyledAttributes(
                new int[]{android.R.attr.actionBarSize});
        int actionBarHeight = (int) ta.getDimension(0, 0);
        return actionBarHeight;
    }

    public static void toggleKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static int getSoftButtonsBarHeight(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager windowManager = ((Activity) context).getWindowManager();
            windowManager.getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            windowManager.getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }

    public static void fixSoftButtonsHeightProblem(Context context, View view) {
        int softButtonsHeight = Utils.getSoftButtonsBarHeight(context);
        view.setPadding(0, 0, 0, softButtonsHeight);
    }

    public static String buildPicassoUrl(String url) {
        if (TextUtils.isEmpty(url))
            url += "http://";

        return url;
    }

    public static void shareImage(Context context, Bitmap bitmap, String cacheFolderName) {
        UUID imageName = UUID.randomUUID();
        OutputStream fOut = null;
        try {
            File root = new File(Environment.getExternalStorageDirectory()
                    + File.separator + cacheFolderName + File.separator);
            root.mkdirs();
            File sdImageMainDirectory = new File(root, imageName.toString() + ".jpg");
            fOut = new FileOutputStream(sdImageMainDirectory);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 95, fOut);

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(sdImageMainDirectory));
            context.startActivity(Intent.createChooser(share, "Fotoğrafı Paylaş..."));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fOut != null) {
                try {
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public static void removeDir(String dirName) {
        File cacheDir = new File(Environment.getExternalStorageDirectory()
                + File.separator + dirName + File.separator);
        deleteCacheDir(cacheDir);
    }

    private static boolean deleteCacheDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteCacheDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public static boolean checkSpecialCharacter(String str) {
        String[] trCharacters = new String[]{"ç", "ı", "ü", "ğ", "ö", "ş", " "};
        boolean valid = true;
        for (int i = 0; i < str.length(); i++) {
            String character = String.valueOf(str.charAt(i));
            boolean result = Arrays.asList(trCharacters).contains(character.toLowerCase());
            if (result) {
                valid = false;
                return valid;
            }
        }
        return valid;
    }

    public static String replaceTurkishCharacters(String text) {
        String[] olds = {"Ğ", "ğ", "Ü", "ü", "Ş", "ş", "İ", "ı", "Ö", "ö", "Ç", "ç"};
        String[] news = {"G", "g", "U", "u", "S", "s", "I", "i", "O", "o", "C", "c"};
        for (int i = 0; i < olds.length; i++) {
            text = text.replace(olds[i], news[i]);
        }
        text = text.toUpperCase();
        return text;
    }

    public static boolean checkPermission(String strPermission, Context context) {
        int result = ContextCompat.checkSelfPermission(context, strPermission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public static void requestPermission(String strPermission, int perCode, Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{strPermission}, perCode);
    }

    public static void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    public static void openContactList(Context context, int reqCode) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        ((Activity) context).startActivityForResult(intent, reqCode);
    }

    public static void openAppSettingsIntent(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static String cleanSpecialCharValue(String value) {
        if ((value == null)){
            return "";
        }else{
            value = value.replace("'\\'", "");
            value = value.replace("<", "");
            value = value.replace(">","");
            value = value.replace("!","");
            value = value.replace("=","");
            value = value.replace("%", "");
            value = value.replace(";", "");
            value = value.replace("(","");
            value = value.replace(")","");
            value = value.replace("$","");
            value = value.replace("+","");
            value = value.replace("^","");
            return value;
        }


    }

    public static StringEntity jsontoStringEntity(JSONObject json){

        StringEntity entity = null;
        entity = new StringEntity(json.toString(), HTTP.UTF_8);
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json; charset=UTF-8"));

        return entity;
    }

    public static String getLanguage (){
        //final String[] SUPPORTED_LANGUAGES = {"tr", "en", "de"};
        //final String defaultLanguage = "en";
        String lang = Locale.getDefault().getLanguage().toLowerCase();
        if (isLangSupported(lang)){

            return lang;
        }
        else {

            return "en";
        }
    }

    public static boolean isLangSupported(String lang){

        final String[] SUPPORTED_LANGUAGES = {"tr", "en", "de", "de-ch", "de-lu", "de-li", "de-at", "az"};


        if (!Arrays.asList(SUPPORTED_LANGUAGES).contains(lang))
        {
            return false;
        }else{

            return true;
        }

    }

    public static void setLanguage (Context context){

        String lang = PreferencesHelper.getInstance(context).getUserLang();
        if (!isLangSupported(lang)){

            lang ="en";
        }

        Locale locale = new Locale(lang);
        Configuration config = new Configuration();
        config.setLocale(locale);
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    private static String getCountrybyLocale(Context context) {
        try {
            return context.getResources().getConfiguration().locale.getCountry().trim().toUpperCase();
        }
        catch (Exception e) {
            return "";
        }
    }


    public static String getCountrybySim(Context context) {
        try {
            TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
            return mTelephonyManager.getNetworkCountryIso().trim().toUpperCase();
        }
        catch (Exception e) {
           return " ";
        }

    }




    public static void restartAPP(Context context) {

        Intent i = new Intent(context, SplashActivity.class);
        context.startActivity(i);
    }





    public static void asekronNotificationToken(Context context, String token, String username){

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("username", username);
            jsonParams.put("token", token);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpClient.directPostWithJsonEntity(context, Constants.S_NOTIFICATIONTOKEN, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.equals("SUCCESS")) {

                        log.d("writed", "done");

                    } else {

                    }
                }catch (JSONException e){

                }
            }
            @Override
            public void onError(Throwable error) {


                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException e2) {

                    }


            }

            @Override
            public void onFinish() {
                super.onFinish();

            }
        });




    }


    public static void asekronPurchaseSend(final Context context){

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("username", PreferencesHelper.getInstance(context).getSaleUserName());
            jsonParams.put("token", PreferencesHelper.getInstance(context).getOrderToken());
            jsonParams.put("orderid", PreferencesHelper.getInstance(context).getOrderId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpClient.directPostWithJsonEntity(context, Constants.S_MISSING_SALE, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.equals("SUCCESS")) {

                        PreferencesHelper.getInstance(context).setPurchaseSend(true);
                        PreferencesHelper.getInstance(context).setOrderToken("");
                        PreferencesHelper.getInstance(context).setOrderId("");
                        PreferencesHelper.getInstance(context).setSaleUserName("");

                    } else {
                        PreferencesHelper.getInstance(context).setPurchaseSend(false);
                    }
                }catch (JSONException e){
                    PreferencesHelper.getInstance(context).setPurchaseSend(false);
                }
            }
            @Override
            public void onError(Throwable error) {
                PreferencesHelper.getInstance(context).setPurchaseSend(false);

                try {
                    Thread.sleep(250);
                } catch (InterruptedException e2) {

                }


            }

            @Override
            public void onFinish() {
                super.onFinish();

            }
        });




    }


    public static void asekronLocaleSend(final Context context, String Token, String OrderID, String PackpageName){

        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("username", PreferencesHelper.getInstance(context).getUsername());
            jsonParams.put("token",Token);
            jsonParams.put("orderid", OrderID);
            jsonParams.put("storeid", PackpageName);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpClient.directPostWithJsonEntity(context, Constants.S_MISSING_SALE, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.equals("SUCCESS")) {



                    } else {
                        //PreferencesHelper.getInstance(context).setPurchaseSend(false);
                    }
                }catch (JSONException e){
                    //PreferencesHelper.getInstance(context).setPurchaseSend(false);
                }
            }
            @Override
            public void onError(Throwable error) {
                //PreferencesHelper.getInstance(context).setPurchaseSend(false);

                try {
                    Thread.sleep(250);
                } catch (InterruptedException e2) {

                }


            }

            @Override
            public void onFinish() {
                super.onFinish();

            }
        });




    }



}
