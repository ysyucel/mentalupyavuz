package com.ayasis.mentalup.intrfc;

import com.ayasis.mentalup.vo.OnBoardingStepContainer;

/**
 * Created by AhmtBrK on 18/03/2017.
 */

public interface IOnBoardingFirstActivity {

    OnBoardingStepContainer getOnBoardingStepContainer();
    void updateProfileInfo();
}
