package com.ayasis.mentalup.ui.activity;

import android.content.Intent;

import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.ayasis.mentalup.R;

import com.ayasis.mentalup.http.HttpClient;
import com.ayasis.mentalup.http.JsonObjectResponseHandler;
import com.ayasis.mentalup.util.Constants;
import com.ayasis.mentalup.util.DialogUtil;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.ayasis.mentalup.util.UiUtil;
import com.ayasis.mentalup.util.Utils;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;

import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.appindexing.builders.Actions;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;


import org.json.JSONException;
import org.json.JSONObject;


import cn.pedant.SweetAlert.SweetAlertDialog;


public class EntryPage extends BaseActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    LoginButton loginButton;
    ImageView entryPageLogo;
    SignInButton signInButton;
    Button singWithEmail, googleAlt;
    TextView userAgreementETV;


    CallbackManager callbackManager;
    ProfileTracker profileTracker;
    int tryCount = Constants.tryNumber;

    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    private FirebaseAuth mAuth;
    private FirebaseAnalytics mFirebaseAnalytics;

    Trace myTrace = FirebasePerformance.getInstance().newTrace("EntryPageConnect");




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sing);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        FacebookSdk.sdkInitialize(getApplicationContext());
        Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");


        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        userAgreementETV = (TextView) findViewById(R.id.userAgreementETV);
        userAgreementETV.setTypeface(opensansRegular);
        userAgreementETV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Sözleşmeyi inceleyen kullanıcılar için tetiklenir.
                mFirebaseAnalytics.logEvent("ENTRYSCRREN_CLICKEDAGGREEMENT", null);

                //Fabric Event - Welcome Page User Agreement Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click User Aggrement Button Via Welcome Page"));

                Intent i = new Intent(EntryPage.this, LegalMainPage.class);
                startActivity(i);
                finish();



            }
        });


        if (Constants.navigationFromSplash == 1 ){


            //Auto login yapmamış tüm kullanıcılar için tetiklenir.
            mFirebaseAnalytics.logEvent("ENTRYSCRREN_VISITED", null);

            //Fabric Event - Welcome Page visit
            Answers.getInstance().logCustom(new CustomEvent("Visit Welcome Page"));
            Constants.navigationFromSplash =0;


        }




        entryPageLogo = (ImageView) findViewById(R.id.entryPageLogo);

        if (!PreferencesHelper.getInstance(getApplicationContext()).getUserLang().equals("tr")) {
            //TR degilse resmi degistirelim
            entryPageLogo.setImageResource(R.drawable.en_whitelogo);

        }


        singWithEmail = (Button) findViewById(R.id.singWithEmail);
        findViewById(R.id.singWithEmail).setOnClickListener(this);
        singWithEmail.setTypeface(opensansSemibold);

        googleAlt = (Button) findViewById(R.id.googleAlt);
        findViewById(R.id.googleAlt).setOnClickListener(this);
        googleAlt.setTypeface(opensansSemibold);

        mAuth = FirebaseAuth.getInstance();

        //Google Sing In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(EntryPage.this.getResources().getString(R.string.default_web_client_idMY))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();



        loginButton = (LoginButton) findViewById(R.id.connectWithFbButton);
        loginButton.setTypeface(opensansSemibold);

        //Logout Kismina Bak
        if (true){

            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                    .Callback() {
                @Override
                public void onCompleted(GraphResponse graphResponse) {


                    LoginManager.getInstance().logOut();
                    mGoogleApiClient.disconnect();
                    //Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                    FirebaseUser user = mAuth.getCurrentUser();
                    if (user == null){
                        FirebaseAuth.getInstance().signOut();
                    }

                }
            }).executeAsync();
        }



        loginButton.setReadPermissions("email");
        callbackManager = CallbackManager.Factory.create();


        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {



            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        tryCount = Constants.tryNumber;
                        // Get facebook data from login
                        getFacebookData(object);
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
                request.setParameters(parameters);
                request.executeAsync();

            }



            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
                // display error
               //TODO Buraya Hata Mesaji Ekleyelim
            }



        });

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged (Profile oldProfile, Profile currentProfile) {
                if (currentProfile != null) {
                    displayProfileInfo(currentProfile);
                }

            }
        };


        manageLayoutMargins();

    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]

                        // [END_EXCLUDE]
                    }
                });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Forward result to the callback manager for Login Button
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase

                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately

            }
        }

    }

    /*protected void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
        // Find the TextView that is inside of the SignInButton and set its text
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText(getString(R.string.googleLoginText));
                tv.setTextSize(16);
                Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");
                Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
                tv.setTypeface(opensansSemibold);

                return;
            }
        }
    }*/


    private void displayProfileInfo(Profile profile) {
        // get Profile ID


        String profileId = profile.getId();
        String name = profile.getName();
        String surname = profile.getLastName();


        //connectOnServerWithFacebook(profileId);

        // display the Profile name
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {


        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = mAuth.getCurrentUser();
                            connectOnServerWithGoogle(user.getUid(), user.getEmail());

                        } else {
                            // If sign in fails, display a message to the user.

                            //TODO buraya dialog ekleyelim

                        }

                        // ...
                    }
                });
    }


    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void getFacebookData(JSONObject object) {

        try {

            String id = object.getString("id");
            String email = null;
            if (object.has("email"))
                email = object.getString("email");

            connectOnServerWithFacebook(id, email);

        }
        catch(JSONException e) {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.singWithEmail:

                //Email ile bağlanmaya çalışan kullanıcılar için tetiklenir
                mFirebaseAnalytics.logEvent("ENTRYSCRREN_CLICKEDEMAIL", null);

                //Fabric Event - Welcome Page Connect With Email Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click ConnectWithEmail Button Via Welcome Page"));

                if (PreferencesHelper.getInstance(getApplicationContext()).getUsername().equals("") ||
                        PreferencesHelper.getInstance(getApplicationContext()).getUsername() ==null ){


                    Intent i = new Intent(EntryPage.this, RegisterActivity.class);
                    startActivity(i);
                    finish();

                }else{

                    Intent i = new Intent(EntryPage.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.googleAlt:
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                //Fabric Event - Welcome Page Connect With Google Button Click
                Answers.getInstance().logCustom(new CustomEvent("Click ConnectWithGoogle Button Via Welcome Page"));

                //Google ile bağlanmaya çalışan kullanıcılar için tetiklenir
                mFirebaseAnalytics.logEvent("ENTRYSCRREN_CLICKEDGOOGLE", null);

                signIn();
                tryCount = Constants.tryNumber;
                break;
            // ...
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {


    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        FirebaseUserActions.getInstance().start(getIndexApiAction());
    }

    public void connectOnServerWithFacebook(final String fuid, final String email) {
        myTrace.start();


        if (tryCount != 0) {
            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("fuid",fuid );
                jsonParams.put("connectiontype", "fuid");
                jsonParams.put("email", email);
                jsonParams.put("guid",null);
                jsonParams.put("language",PreferencesHelper.getInstance(getApplicationContext()).getUserLang());


            } catch (JSONException e) {
                e.printStackTrace();
            }

            final SweetAlertDialog loadingProgress = DialogUtil.showProgressDialog(EntryPage.this, getString(R.string.newEntryAlert), R.color.colorPrimary);

            HttpClient.directPostWithJsonEntity(EntryPage.this, Constants.S_CONNECT, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {



                @Override
                public void onSuccess(JSONObject response) {
                    myTrace.stop();
                    try {
                        String status = response.getString("status");
                        if (status.equals("SUCCESS")) {
                            tryCount = 0;
                            PreferencesHelper.getInstance(getApplicationContext()).setUsername(response.getString("userid"));
                            PreferencesHelper.getInstance(getApplicationContext()).setPassword(response.getString("password"));
                            PreferencesHelper.getInstance(getApplicationContext()).setEmail(email);
                            PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(true);
                            PreferencesHelper.getInstance(getApplicationContext()).setUserLang(response.getString("language"));

                            String notificationToken = FirebaseInstanceId.getInstance().getToken();
                            Utils.asekronNotificationToken(getApplicationContext(), notificationToken, response.getString("userid"));


                            if (response.getString("type").equals("register")){

                                //facebook yolu ile kayıt olan kullanıcılar için tetiklenir.
                                Bundle param = new Bundle();
                                param.putString(FirebaseAnalytics.Param.SIGN_UP_METHOD, "facebook");
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, param);

                                //Fabric Event - Welcome Page sayfası Başarılı Facebook Kayıt Sonrası
                                Answers.getInstance().logSignUp(new SignUpEvent()
                                        .putMethod("RegisterWithFacebook")
                                        .putSuccess(true));

                                Intent i = new Intent(EntryPage.this, OnBoardingFirstActivity.class);
                                startActivity(i);
                                finish();
                            }else{

                                //facebook yolu ile giriş yapan kullanıcılar için tetiklenir.
                                Bundle param = new Bundle();
                                param.putString("LOGIN_METHOD", "facebook");
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, param);

                                //Fabric Event - Welcome Page sayfası başarılı facebook login sonrası
                                Answers.getInstance().logLogin(new LoginEvent()
                                        .putMethod("LoginWithFacebook")
                                        .putSuccess(true));

                                Intent i = new Intent(EntryPage.this, NativeCallUnity.class);
                                startActivity(i);
                                finish();
                            }


                        } else {
                            String error = response.getString("errorCode");
                            if (error.endsWith("3")||error.endsWith("4")||error.endsWith("8")||error.endsWith("9")||error.endsWith("10")){
                                if (tryCount==1) {

                                    Intent i = new Intent(EntryPage.this, EntryPage.class);
                                    startActivity(i);
                                    finish();
                                }
                                else {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e2) {

                                    }
                                    tryCount = tryCount -1;
                                    connectOnServerWithFacebook(fuid,email);
                                }

                            }else{

                                PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);

                            }


                        }
                    } catch (JSONException e) {

                        if (tryCount==1) {

                        }
                        else {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e2) {

                            }
                            tryCount = tryCount -1;
                            connectOnServerWithFacebook(fuid,email);
                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    myTrace.stop();
                    if (tryCount==1) {


                    }
                    else {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e2) {

                        }
                        tryCount = tryCount - 1;
                        connectOnServerWithFacebook(fuid,email);
                    }
                }

                @Override
                public void onFinish() {
                    myTrace.stop();
                    super.onFinish();
                    if (loadingProgress != null && loadingProgress.isShowing()) {
                        loadingProgress.dismiss();
                    }
                }
            });
        }
    }


    public void connectOnServerWithGoogle(final String guid, final String email) {
        myTrace.start();


        if (tryCount != 0) {
            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("fuid",null );
                jsonParams.put("connectiontype", "guid");
                jsonParams.put("email", email);
                jsonParams.put("guid",guid);
                jsonParams.put("language",PreferencesHelper.getInstance(getApplicationContext()).getUserLang());


            } catch (JSONException e) {
                e.printStackTrace();
            }

            final SweetAlertDialog loadingProgress = DialogUtil.showProgressDialog(EntryPage.this, getString(R.string.newEntryAlert), R.color.colorPrimary);

            HttpClient.directPostWithJsonEntity(EntryPage.this, Constants.S_CONNECT, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {

                @Override
                public void onSuccess(JSONObject response) {
                    myTrace.stop();
                    try {
                        String status = response.getString("status");
                        if (status.equals("SUCCESS")) {
                            tryCount = 0;


                            PreferencesHelper.getInstance(getApplicationContext()).setUsername(response.getString("userid"));
                            PreferencesHelper.getInstance(getApplicationContext()).setPassword(response.getString("password"));
                            PreferencesHelper.getInstance(getApplicationContext()).setEmail(email);
                            PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(true);

                            PreferencesHelper.getInstance(getApplicationContext()).setUserLang(response.getString("language"));

                            //TODO:Start Asynchronous Service for Send Register Token
                            String notificationToken = FirebaseInstanceId.getInstance().getToken();
                            Utils.asekronNotificationToken(getApplicationContext(), notificationToken, response.getString("userid"));


                            if (response.getString("type").equals("register")){

                                //google yolu ile kayıt olan kullanıcılar için tetiklenir.
                                Bundle param = new Bundle();
                                param.putString(FirebaseAnalytics.Param.SIGN_UP_METHOD, "google");
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, param);

                                //Fabric Event - Welcome Page sayfası Başarılı Google Kayıt Sonrası
                                Answers.getInstance().logSignUp(new SignUpEvent()
                                        .putMethod("RegisterWithGoogle")
                                        .putSuccess(true));

                                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                Intent i = new Intent(EntryPage.this, OnBoardingFirstActivity.class);
                                startActivity(i);
                                finish();
                            }else{

                                //google yolu ile giriş yapan kullanıcılar için tetiklenir.
                                Bundle param = new Bundle();
                                param.putString("LOGIN_METHOD", "google");
                                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, param);

                                //Fabric Event - Welcome Page sayfası başarılı google login sonrası
                                Answers.getInstance().logLogin(new LoginEvent()
                                        .putMethod("LoginWithGoogle")
                                        .putSuccess(true));
                                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                Intent i = new Intent(EntryPage.this, NativeCallUnity.class);
                                startActivity(i);
                                finish();
                            }


                        } else {
                            String error = response.getString("errorCode");
                            if (error.endsWith("3")||error.endsWith("4")||error.endsWith("8")||error.endsWith("9")||error.endsWith("10")){
                                if (tryCount==1) {

                                   recreate();
                                }
                                else {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e2) {

                                    }
                                    tryCount = tryCount -1;
                                    connectOnServerWithGoogle(guid,email);
                                }

                            }else{

                                PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);

                            }


                        }
                    } catch (JSONException e) {

                        if (tryCount==1) {


                        }
                        else {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e2) {

                            }
                            tryCount = tryCount -1;
                            connectOnServerWithGoogle(guid,email);
                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    myTrace.stop();
                    if (tryCount==1) {


                    }
                    else {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e2) {

                        }
                        tryCount = tryCount - 1;
                        connectOnServerWithGoogle(guid,email);
                    }
                }

                @Override
                public void onFinish() {
                    myTrace.stop();
                    super.onFinish();
                    if (loadingProgress != null && loadingProgress.isShowing()) {
                        loadingProgress.dismiss();
                    }
                }
            });
        }
    }

    private void manageLayoutMargins() {
        boolean isTablet = PreferencesHelper.getInstance(getApplicationContext()).getDeviceModel();
        if (isTablet == false) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;


            double logoWidth = (unitWidth * 3.5);
            double logoHeight = (unitWidth * 2.72);
            entryPageLogo.getLayoutParams().width =(int)logoWidth;
            entryPageLogo.getLayoutParams().height=(int)logoHeight;
            double firstTopGap = unitWidth/3;
            UiUtil.setMarginToView(entryPageLogo, (int)unitWidth, (int)firstTopGap, (int)unitWidth, 0);

            double secondTopGap =  screenHeigt / 1336.0 * 500;
            UiUtil.setMarginToView(googleAlt, (int)unitWidth, (int)secondTopGap, (int)unitWidth, 0);

            double thirdTopGap = screenHeigt / 1336.0 * 34;
            UiUtil.setMarginToView(loginButton,(int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);

            UiUtil.setMarginToView(singWithEmail,(int)unitWidth, (int)thirdTopGap, (int)unitWidth, 0);

            double fourthGap = screenHeigt / 1336.0 * 50;

            UiUtil.setMarginToView(userAgreementETV,(int)unitWidth,(int)fourthGap , (int)unitWidth, 0);


        } else {


            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            double screenWidth = size.x;
            double screenHeigt = size.y;
            double unitWidth = screenWidth / 12;


            double logoWidth = (unitWidth * 3);
            double logoHeight = (unitWidth * 2.33);
            entryPageLogo.getLayoutParams().width =(int)logoWidth;
            entryPageLogo.getLayoutParams().height=(int)logoHeight;
            double firstTopGap = unitWidth/3;
            UiUtil.setMarginToView(entryPageLogo, (int)unitWidth, (int)firstTopGap, (int)unitWidth, 0);

            double secondTopGap =  screenHeigt / 1024 * 380;
            UiUtil.setMarginToView(googleAlt, (int)unitWidth*2, (int)secondTopGap, (int)unitWidth*2, 0);

            double thirdTopGap = screenHeigt / 1024 * 34;
            UiUtil.setMarginToView(loginButton,(int)unitWidth*2, (int)thirdTopGap, (int)unitWidth*2, 0);

            UiUtil.setMarginToView(singWithEmail,(int)unitWidth*2, (int)thirdTopGap, (int)unitWidth*2, 0);

            double fourthGap = screenHeigt / 1024 * 50;

            UiUtil.setMarginToView(userAgreementETV,(int)unitWidth*2,(int)fourthGap , (int)unitWidth*2, 0);



        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        return Actions.newView("EntryPage", "https://www.mentalup.net");
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
           super.finish();
            //finish();
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onStop() {

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        FirebaseUserActions.getInstance().end(getIndexApiAction());
        super.onStop();
    }
}
