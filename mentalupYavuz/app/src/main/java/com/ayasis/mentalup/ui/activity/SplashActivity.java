package com.ayasis.mentalup.ui.activity;

/**
 * Created by sercesh on 07/02/2017.
 */

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;

import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;


import com.ayasis.mentalup.util.NetworkStateReceiver;
import com.ayasis.mentalup.R;
import com.ayasis.mentalup.http.HttpClient;
import com.ayasis.mentalup.http.JsonObjectResponseHandler;
import com.ayasis.mentalup.util.Constants;
import com.ayasis.mentalup.util.UiUtil;
import com.ayasis.mentalup.util.Utils;
import com.crashlytics.android.Crashlytics;
import com.ayasis.mentalup.util.PreferencesHelper;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.amplitude.api.Amplitude;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import org.json.JSONException;
import org.json.JSONObject;
//import com.optimizely.Optimizely;


import io.fabric.sdk.android.Fabric;


public class SplashActivity extends BaseActivity {
    private FirebaseAnalytics mFirebaseAnalytics;
    int tryCount = Constants.tryNumber;
    private static int SPLASH_TIME_OUT = 3000;
    String notificationKey = "";
    String notificationValue = "";
    Boolean hasNotification = false;
    TextView mentalupMessage, mentalupMessageHeader;

    Trace myTrace = FirebasePerformance.getInstance().newTrace("Splash");





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Amplitude initialize
        Amplitude.getInstance().initialize(this, "a320d6646b0541845df3c2026dc80131").enableForegroundTracking(getApplication());
        HttpClient.trustAllHosts();

        /*ServiceConnection mServiceConn = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
            }

            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
                mService = IInAppBillingService.Stub.asInterface(service);
            }
        };

        Intent serviceIntent =
                new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        try {
            Bundle ownedItems = mService.getPurchases(3, getApplicationContext().getPackageName(),"subs", "");
        } catch (RemoteException e) {
            e.printStackTrace();
        }


        */




        //Appsee.start("823fc7dc64a148e1a5da240855b0c5e8");
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        /*Typeface opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface opensansSemibold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold.ttf");

        mentalupMessage = (TextView) findViewById(R.id.mentalupMessage);

        mentalupMessage.setTypeface(opensansSemibold);

        mentalupMessageHeader = (TextView) findViewById(R.id.mentalupMessageHeader);

        mentalupMessageHeader.setTypeface(opensansRegular);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        double screenWidth = size.x;
        double screenHeigt = size.y;
        double unitWidth = screenWidth / 12;


        UiUtil.setMarginToView(mentalupMessageHeader, (int)unitWidth, (int)100, (int)unitWidth, 0);
        UiUtil.setMarginToView(mentalupMessage, (int)unitWidth, (int)75, (int)unitWidth, (int)screenHeigt/10);

        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(3);

        if (randomInt == 0) {

            mentalupMessage.setText(getString(R.string.splashMessage1));

        }
        if (randomInt == 1) {

            mentalupMessage.setText(getString(R.string.splashMessage2));

        }
        if (randomInt == 2) {

            mentalupMessage.setText(getString(R.string.splashMessage3));

        }
        if (randomInt == 3) {

            mentalupMessage.setText(getString(R.string.splashMessage4));

        }

        */

        //Uygulama Acildiginda tetiklenen event
        Amplitude.getInstance().logEvent("APP_OPEN");


        //Optimizely.enablePreview();
        //Optimizely.enableEditor();
        //Optimizely.startOptimizelyWithAPIToken(getString(R.string.com_optimizely_api_key), getApplication());



        if (!PreferencesHelper.getInstance(getApplicationContext()).getPurchaseSend()){

            Utils.asekronPurchaseSend(getApplicationContext());

        }

        //Notification
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().get(key).toString();
                if (key.equals("1") || key.equals("2") || key.equals("3") || key.equals("4") || key.equals("5")){
                    hasNotification = true;
                    notificationKey = key;
                    notificationValue = value.toString();
                }
            }
        }


        if (PreferencesHelper.getInstance(getApplicationContext()).getUserLang().equals("")) {
            PreferencesHelper.getInstance(getApplicationContext()).setUserLang(Utils.getLanguage());
        }


        if (!NetworkStateReceiver.isConnected(this)) {
            Intent i = new Intent(this, NoConnectionActivity.class);
            this.startActivity(i);
            finish();
        } else {
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            getApplicationContext().registerReceiver(new NetworkStateReceiver(), filter);

            //Firebase Event: Uygulamanin ilk acildigininda olusan event
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


            PreferencesHelper.getInstance(getApplicationContext()).setDeviceModel(UiUtil.isTablet(getWindowManager()));

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing SplashActivity screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */
                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity

                    boolean pageNavigation = PreferencesHelper.getInstance(getApplicationContext()).getUsercomeback();

                    String username = PreferencesHelper.getInstance(getApplicationContext()).getUsername();
                    String password = PreferencesHelper.getInstance(getApplicationContext()).getPassword();

                    if (pageNavigation) {
                        doLoginOnServer(username, password);
                    } else {


                        if (username.equals("") || username.equals(null)){
                            Bundle param = new Bundle();
                            param.putString("USER_TYPE", "NEW_USER");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, param);
                        }else
                        {
                            Bundle param = new Bundle();
                            param.putString("USER_TYPE", "LOGGED_OUT_USER");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, param);

                        }
                        //Uygulamayı ilk defa indiren kullanıcılar tetiklenir
                        Intent i = new Intent(SplashActivity.this, EntryPage.class);
                        Constants.navigationFromSplash = 1;
                        startActivity(i);
                        finish();
                    }

                }
            }, SPLASH_TIME_OUT);
        }
    }

    public void doLoginOnServer(final String username, final String password) {
        myTrace.start();

        if (tryCount != 0) {
            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("username", username);
                jsonParams.put("password", password);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            HttpClient.directPostWithJsonEntity(SplashActivity.this, Constants.S_LOGIN, Utils.jsontoStringEntity(jsonParams), new JsonObjectResponseHandler() {

                @Override
                public void onSuccess(JSONObject response) {
                    myTrace.stop();
                    try {
                        String status = response.getString("status");
                        if (status.equals("SUCCESS")) {

                            tryCount = 0;

                            //Uygulamaya auto-login yapan kullanıcılar tetiklenir
                            Bundle param = new Bundle();
                            param.putString("USER_TYPE", "AUTO_LOGIN_USER");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, param);


                            PreferencesHelper.getInstance(getApplicationContext()).setUsername(response.getString("userid"));
                            PreferencesHelper.getInstance(getApplicationContext()).setEmail(response.getString("email"));
                            PreferencesHelper.getInstance(getApplicationContext()).setPassword(password);
                            PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(true);
                            PreferencesHelper.getInstance(getApplicationContext()).setUserLang(response.getString("language"));

                            //TODO:Start Asynchronous Service for Send Register Token
                            String notificationToken = FirebaseInstanceId.getInstance().getToken();
                            Utils.asekronNotificationToken(getApplicationContext(), notificationToken, response.getString("userid"));

                            if (hasNotification){

                                notificationNavigator(notificationKey,notificationValue);

                            }else{
                                Intent i = new Intent(SplashActivity.this, NativeCallUnity.class);
                                startActivity(i);
                                finish();
                            }



                        } else {
                            String error = response.getString("errorCode");
                            if (error.endsWith("3")||error.endsWith("4")||error.endsWith("8")||error.endsWith("9")||error.endsWith("10")){
                                if (tryCount==1) {
                                    //PreferencesHelper.getInstance(getApplicationContext()).setUsername(null);
                                    //PreferencesHelper.getInstance(getApplicationContext()).setEmail(null);
                                    //PreferencesHelper.getInstance(getApplicationContext()).setPassword(null);
                                    //PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);
                                    Intent i = new Intent(SplashActivity.this, EntryPage.class);
                                    Constants.navigationFromSplash = 1; 
                                    startActivity(i);
                                    finish();
                                }
                                else {
                                    try {
                                        Thread.sleep(250);
                                    } catch (InterruptedException e2) {

                                    }
                                    tryCount = tryCount -1;
                                    doLoginOnServer(username,password);
                                }

                            }else{
                                //PreferencesHelper.getInstance(getApplicationContext()).setUsername(null);
                                //PreferencesHelper.getInstance(getApplicationContext()).setEmail(null);
                                //PreferencesHelper.getInstance(getApplicationContext()).setPassword(null);

                                //Uygulamayı daha once giriş yapmış. Ama logout olmuş kullanıcılardan tekrar giriş yapanlar için tetiklenir
                                PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);
                                Intent i = new Intent(SplashActivity.this, EntryPage.class);
                                Constants.navigationFromSplash = 1;
                                startActivity(i);
                                finish();
                            }


                        }
                    } catch (JSONException e) {

                        if (tryCount==1) {
                            //PreferencesHelper.getInstance(getApplicationContext()).setUsername(null);
                            //PreferencesHelper.getInstance(getApplicationContext()).setEmail(null);
                            //PreferencesHelper.getInstance(getApplicationContext()).setPassword(null);
                            //PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);
                            Intent i = new Intent(SplashActivity.this, EntryPage.class);
                            Constants.navigationFromSplash = 1;
                            startActivity(i);
                            finish();
                        }
                        else {
                            try {
                                Thread.sleep(250);
                            } catch (InterruptedException e2) {

                            }
                            tryCount = tryCount -1;
                            doLoginOnServer(username,password);
                        }
                    }
                }

                @Override
                public void onError(Throwable error) {
                    myTrace.stop();
                    if (tryCount==1) {
                        //PreferencesHelper.getInstance(getApplicationContext()).setUsername(null);
                        //PreferencesHelper.getInstance(getApplicationContext()).setEmail(null);
                        //PreferencesHelper.getInstance(getApplicationContext()).setPassword(null);
                        //PreferencesHelper.getInstance(getApplicationContext()).setUsercomeback(false);
                        Intent i = new Intent(SplashActivity.this, EntryPage.class);
                        Constants.navigationFromSplash = 1;
                        startActivity(i);
                        finish();
                    }
                    else {
                        try {
                            Thread.sleep(250);
                        } catch (InterruptedException e2) {

                        }
                        tryCount = tryCount - 1;
                        doLoginOnServer(username,password);
                    }
                }
            });
        }
    }

    public void notificationNavigator(String noKey, String value){
        Intent i;
        Boolean unknownIntent = true;

        if (noKey.equals("1")){
            unknownIntent = false;
            //Answers.getInstance().logCustom(new CustomEvent("BuyPremiumSecondStep_Via_Notification"));
            i = new Intent(this, BuyPremiumSecondStepActivity.class);
            this.startActivity(i);
            finish();

        }
        if (noKey.equals("2")){
            //Answers.getInstance().logCustom(new CustomEvent("BuyPremiumFirsStep_Via_Notification"));
            unknownIntent = false;
            i = new Intent(this, BuyPremiumFirstStepActivity.class);
            this.startActivity(i);
            finish();

        }
        if (noKey.equals("3")){
            //Answers.getInstance().logCustom(new CustomEvent("IKnowThisPage_Via_Notification"));
            unknownIntent = false;
            i = new Intent(this, OnBoardingSecondActivity.class);
            this.startActivity(i);
            finish();

        }
        if (noKey.equals("4")){
            //Answers.getInstance().logCustom(new CustomEvent("UserAgrement_Via_Notification"));
            unknownIntent = false;
            i = new Intent(this, userAgreement.class);
            this.startActivity(i);
            finish();

        }
        if (noKey.equals("5")){
            unknownIntent = false;
            //Answers.getInstance().logCustom(new CustomEvent("SpesificWebPage_Via_Notification"));
            Constants.NotificationPageWebPage = value;
            Constants.savedNotification = true;
            i = new Intent(this, NativeCallUnity.class);
            this.startActivity(i);
            finish();

        }
        if (unknownIntent){
            //Answers.getInstance().logCustom(new CustomEvent("UnknownIntentWasCome"));

            i = new Intent(this, NativeCallUnity.class);
            this.startActivity(i);
            finish();
        }

    }



}


