# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/sercesh/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-ignorewarnings
-keep class cn.pedant.** { *; }
-keep class com.android.vending.billing.**
-keep class com.google.android.gms.ads.** { *; }
-keep class com.ayasis.mental.*{*;}
-keep class com.ayasis.mental.UnityPlayerActivity.* { *; }
-keep class com.ayasis.mentalup.util.unityCommicationClass.* { *; }
-keep class com.ayasis.mentalup.ui.activity.UnityGamesWebview.* { *; }
-keep class bitter.jnibridge.* { *; }
-keep class com.unity3d.player.* { *; }
-keep class org.fmod.* { *; }
    -dontwarn okio.**
