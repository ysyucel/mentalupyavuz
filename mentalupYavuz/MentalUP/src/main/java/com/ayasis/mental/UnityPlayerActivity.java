package com.ayasis.mental;

import com.unity3d.player.*;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.ComponentName;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;

public class UnityPlayerActivity extends Activity
{   public static String gameIdUnity,gameLevelUnity,fromTrainingUnity,facebookMessageUnity,unityMessageGameEnd,unityLang,unityPassword;
    public static String userNameForUnity = "";
    public static String userLanguageForUnity = "";
    public static String isPremiumUnity="";
    static Button unityCallWebviewBt,unityCallBuyPremiumFirstStepActivityBt,quitActivityUnityBt,unityCallFacebookShareBt,langSetterUnityBt,passSetterUnityBt;

    protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code

    FrameLayout fl_forUnity;

    // Setup activity layout
    @Override protected void onCreate (Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        getWindow().setFormat(PixelFormat.RGBX_8888); // <--- This makes xperia play happy

        mUnityPlayer = new UnityPlayer(this);
        if (mUnityPlayer.getSettings ().getBoolean ("hide_status_bar", true))
        {
            setTheme(android.R.style.Theme_NoTitleBar_Fullscreen);
            getWindow ().setFlags (WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.unitylayout);
        this.fl_forUnity = (FrameLayout)findViewById(R.id.fl_forUnity);

        this.fl_forUnity.addView(mUnityPlayer.getView(),
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        mUnityPlayer.requestFocus();

        unityCallWebviewBt = (Button)findViewById(R.id.unityCallWebviewButton);
        this.unityCallWebviewBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.v("anan2", "saleOpen: ");
                Intent res = new Intent();
                String mPackage = "com.ayasis.mentalup";
                String mClass = ".ui.activity.UnityGamesWebview";
                res.setComponent(new ComponentName(mPackage,mPackage+mClass));
                //res.putExtra("STRING_I_NEED",strName);
                startActivity(res);


            }
        });

        unityCallBuyPremiumFirstStepActivityBt = (Button)findViewById(R.id.unityCallBuyPremiumFirstStepActivityButton);
        this.unityCallBuyPremiumFirstStepActivityBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent res = new Intent();
                String mPackage = "com.ayasis.mentalup";
                String mClass = ".ui.activity.BuyPremiumFirstStepActivity";
                res.setComponent(new ComponentName(mPackage,mPackage+mClass));
                //res.putExtra("STRING_I_NEED",strName);
                startActivity(res);

            }
        });

        unityCallFacebookShareBt = (Button)findViewById(R.id.unityCallFacebookShareButton);
        this.unityCallFacebookShareBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent res = new Intent();
                String mPackage = "com.ayasis.mentalup";
                String mClass = ".ui.activity.unityFacebookShare";
                res.setComponent(new ComponentName(mPackage,mPackage+mClass));
                //res.putExtra("STRING_I_NEED",strName);
                startActivity(res);

            }
        });

        quitActivityUnityBt = (Button)findViewById(R.id.quitActivityUnityButton);
        this.quitActivityUnityBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent res = new Intent();
                String mPackage = "com.ayasis.mentalup";
                String mClass = ".ui.activity.quitActivityUnity";
                res.setComponent(new ComponentName(mPackage,mPackage+mClass));
                //finish();
                startActivity(res);

            }
        });

        langSetterUnityBt = (Button)findViewById(R.id.langSetterUnityButton);
        this.langSetterUnityBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent res = new Intent();
                String mPackage = "com.ayasis.mentalup";
                String mClass = ".ui.activity.langSetterUnity";
                res.setComponent(new ComponentName(mPackage,mPackage+mClass));
                //res.putExtra("STRING_I_NEED",strName);
                startActivity(res);

            }
        });

        passSetterUnityBt = (Button)findViewById(R.id.passSetterUnityButton);
        this.passSetterUnityBt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent res = new Intent();
                String mPackage = "com.ayasis.mentalup";
                String mClass = ".ui.activity.passSetterUnity";
                res.setComponent(new ComponentName(mPackage,mPackage+mClass));
                //res.putExtra("STRING_I_NEED",strName);
                startActivity(res);

            }
        });
    }

    //unity satış ekranını açıyor
    public void saleOpen(){
        unityCallBuyPremiumFirstStepActivityBt.callOnClick();
    }

    //unity nativedeki webview sayfası açıyo değer olarak games id yi games leveli ve trainingden mi yoksa günlük egzersizden mi geldiğini yolluyor
    public void gamesOpen(String x){
        String[] parts = x.split(",");
        gameIdUnity=parts[0];
        gameLevelUnity=parts[1];
        fromTrainingUnity=parts[2];
        unityCallWebviewBt.callOnClick();
    }

    //unity nativedeki facebook sayfasına intent çıkyıo parametre olarak puanı yolluyor
    public void faceShare(String x){
        facebookMessageUnity=x;
        unityCallFacebookShareBt.callOnClick();
    }

    //Logout yapan kullanıcı çağırıyo
    public void gamesQuit(){
        quitActivityUnityBt.callOnClick();
    }

    //unity içerisinde language değiştirilirse native uygulamanın language'i güncelleniyor
    public void unityLangChange(String lang){
        unityLang=lang;
        langSetterUnityBt.callOnClick();
    }

    //unity içerisinde password değiştirilirse native uygulamanın password'u güncelleniyor
    public void unityPasswordsChange(String pass){
        unityPassword=pass;
        passSetterUnityBt.callOnClick();
    }

    //Native uygulamadan username unity'e yollanıyor
    public void unityUsernameReturnerNative(){
        UnityPlayer.UnitySendMessage("GameManager", "takeUserName",userNameForUnity);
    }

    //Native uygulamadan kullanıcının language'i unity'e yollanıyor
    public void unityUserLanguageReturnerNative(){
        UnityPlayer.UnitySendMessage("GameManager", "takeUserLanguage",userLanguageForUnity);
    }

    //native uygulamadaki games webview'unun sonuçlarına göre unity'e napması gerektiği haber veriliyor
    //secenek 1 kullanıcı oyunu başarıyla bitirdi game summary açılcaksa
    //secenek 2 kullanıcı oyunu bitirmeden kapamaya bastı yada back buttonuna bastıysa unity gamesten geldiyse game'e daily den geldiyse indexe donuyor
    public void unityGameEndReturn(){

        if(unityMessageGameEnd.equals("1")) {
            UnityPlayer.UnitySendMessage("AndroidHelper", "delayOpenCanvasGameSummary","");
        }
        else if(unityMessageGameEnd.equals("2")) {
            UnityPlayer.UnitySendMessage("AndroidHelper", "delayOpenCanvasComing","");
        }
        else {
            UnityPlayer.UnitySendMessage("AndroidHelper", "delayOpenCanvasComing","");
        }
    }
    public void unityPremiumReturn(){

        if(isPremiumUnity.equals("false")) {
            return;
        }
        else if(unityMessageGameEnd.equals("true")) {
            UnityPlayer.UnitySendMessage("AndroidHelper", "userBuyReset","");
        }
        else {
            UnityPlayer.UnitySendMessage("AndroidHelper", "userBuyReset","");
        }
    }
    @Override protected void onNewIntent(Intent intent)
    {
        // To support deep linking, we need to make sure that the client can get access to
        // the last sent intent. The clients access this through a JNI api that allows them
        // to get the intent set on launch. To update that after launch we have to manually
        // replace the intent with the one caught here.
        setIntent(intent);
    }

    // Quit Unity
    @Override protected void onDestroy ()
    {
        mUnityPlayer.quit();
        super.onDestroy();
    }

    // Pause Unity
    @Override protected void onPause()
    {
        super.onPause();
        mUnityPlayer.pause();
    }

    // Resume Unity
    @Override protected void onResume()
    {
        super.onResume();
        mUnityPlayer.resume();
    }

    // Low Memory Unity
    @Override public void onLowMemory()
    {
        super.onLowMemory();
        mUnityPlayer.lowMemory();
    }

    // Trim Memory Unity
    @Override public void onTrimMemory(int level)
    {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_RUNNING_CRITICAL)
        {
            mUnityPlayer.lowMemory();
        }
    }

    // This ensures the layout will be correct.
    @Override public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
    /*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }
}